﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace MarketMaker
{
    public class ThreadSetting
    {
        public Thread thread, timeThread;
        public bool running = false;
        public string pair = "", direction = "";
        public string destination = "100", applyTime = "0", hoga_unit = "0.00", min_order = "0.00", premium = "0.00", rate = "0.00";
        public int timeElapsed = 0, timePass = 0, applyTimeCounter = 0, timeInterval = 0, lastTimeInterval = 0;
        public string apiCallTime = "0.1";
        public int block = 0, nblock = 0, precisionNumber = 8;
        double step = 0;
        public double firstBuyValue;    // 1가 매수
        public double firstSellValue;   // 1가 매도 
        public double curSellValFirst = 0, curBuyValFirst = 0;
        public double curPriceVal = 0;
        public double blockOrderSum = 0;
        public List<string> pendingOrders;
        public HttpClient httpClient;
        public double destinationPrice = 0;
        public bool success = false;
        /* new attribute */
        public bool lastStep = false;
        public int status = 0;
        //This is pumping up function
        public async void prevSetting(double firstPrice, bool firstPriceSet)
        {
            int i = 0;
            //get current Price
            RestClient client = new RestClient();
            var values = client.getMyOrderBook(this.pair);

            if (values == null)
            {
                status = 1;
                return;
            }

            // 매도1가 

            if (values.order_book.sell.Count == 0)
                curSellValFirst = firstPrice;
            else
            {
                if (!firstPriceSet)
                    curSellValFirst = double.Parse(values.order_book.sell[0].rate);
                else
                    curSellValFirst = firstPrice;
            }

            curPriceVal = curSellValFirst;
            ////////////////////////
            if (double.Parse(this.hoga_unit) == 0)
            {
                double temp_hoga = (double)1 / Math.Pow(10, this.precisionNumber);
                this.hoga_unit = temp_hoga.ToString();
            }
            //매수 1가
            int temp_index;
            temp_index = i;

            step = double.Parse(this.hoga_unit) * 9;
            nblock = Convert.ToInt32((double.Parse(this.destination) - 100) * curSellValFirst / (step * 100));
            timeInterval = int.Parse(applyTime) * 60 * 1000 / nblock;
            applyTimeCounter = int.Parse(applyTime) * 60 * 1000;

            //nblock = int.Parse(applyTime) * 60 * 1000 / Program.timeInterval;
            this.lastStep = false;
            //get step for one block
            //step =  Math.Round( (double.Parse(this.destination) - 100) * curSellValFirst / (nblock * 100) , this.precisionNumber ) ;

            this.destinationPrice = Math.Round(curSellValFirst * double.Parse(this.destination) / 100, this.precisionNumber);

            //destination for the first step
            double firstDestination = curSellValFirst + step;

            //매도 1가 ~ 10가 
            Random r = new Random();
            httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string postdata = "";
            int k = 0;
            for (i = 0; i < 10; i++)
            {
                double req_price = Math.Round(step / 9 * i + curSellValFirst, this.precisionNumber);
                //quantity random
                double quantity = (double)r.Next(10, 100) / 100;
                this.blockOrderSum += quantity;
               
                postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"sell\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));
                requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                if (this.running)
                {
                    try
                    {
                        HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                        string json = await response.Content.ReadAsStringAsync();
                        try
                        {
                            var value = JsonConvert.DeserializeObject<OrderResult>(json);
                            k++;
                            if (value.result && k == 10)
                            {
                                this.lastStep = true;
                            }
                        }
                        catch (Newtonsoft.Json.JsonReaderException e)
                        {
                            status = 1;
                        }
                        catch (Newtonsoft.Json.JsonSerializationException e)
                        {
                            status = 1;
                        }
                    }
                    catch (System.Threading.Tasks.TaskCanceledException e)
                    {
                        status = 1;
                    }
                    catch (ArgumentNullException e)
                    {
                        status = 1;
                    }
                    catch (InvalidOperationException e)
                    {
                        status = 1;
                    }
                    catch (HttpRequestException e)
                    {
                        status = 1;
                    }
                }
            }
            pendingOrders = new List<string>();
        }

        public async void prevSettingDown(double firstPrice, bool firstPriceSet)
        {

            int i = 0;

            // get current Price
            RestClient client = new RestClient();
            var values = client.getMyOrderBook(this.pair);

            if (values == null)
            {
                status = 1;
                return;
            }

            //매수 1가 

            if (values.order_book.buy.Count == 0)
                curBuyValFirst = firstPrice;
            else
            {
                if (!firstPriceSet)
                    curBuyValFirst = double.Parse(values.order_book.buy[0].rate);
                else
                    curBuyValFirst = firstPrice;
            }

            curPriceVal = curBuyValFirst;
            ////////////////////////
            if (double.Parse(this.hoga_unit) == 0)
            {
                double temp_hoga = (double)Math.Pow(10, this.precisionNumber);
                this.hoga_unit = temp_hoga.ToString();
            }
            //매도 1가 얻기 
            int temp_index;
            temp_index = 0;
            //step for one block
            step = double.Parse(this.hoga_unit) * 9;
            nblock = Convert.ToInt32((100 - double.Parse(this.destination)) * curBuyValFirst / (step * 100));
            timeInterval = int.Parse(applyTime) * 60 * 1000 / nblock;
            applyTimeCounter = int.Parse(applyTime) * 60 * 1000;
            this.lastStep = false;
            this.destinationPrice = Math.Round(curBuyValFirst * double.Parse(this.destination) / 100, this.precisionNumber);
            //destination for the first step
            double firstDestination = curBuyValFirst - step;

            //매수 1가  부터 10가
            Random r = new Random();
            httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string postdata = "";
            int k = 0;
            for (i = 0; i < 10; i++)
            {
                double req_price = Math.Round(curBuyValFirst - step / 9 * i, this.precisionNumber);
                //randomize
                double quantity = (double)r.Next(10, 100) / 100;
                this.blockOrderSum += quantity;

                postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"buy\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";                
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));

                requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                if (this.running)
                {
                    try
                    {
                        HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                        string json = await response.Content.ReadAsStringAsync();
                        try
                        {
                            var value = JsonConvert.DeserializeObject<OrderResult>(json);
                            k++;
                            if (value.result && k == 10)
                            {
                                this.lastStep = true;
                            }
                        }
                        catch (Newtonsoft.Json.JsonReaderException e)
                        {
                            status = 1;
                        }
                        catch (Newtonsoft.Json.JsonSerializationException e)
                        {
                            status = 1;
                        }
                    }
                    catch (System.Threading.Tasks.TaskCanceledException e)
                    {
                        status = 1;
                    }
                    catch (ArgumentNullException e)
                    {
                        status = 1;
                    }
                    catch (InvalidOperationException e)
                    {
                        status = 1;
                    }
                    catch (HttpRequestException e)
                    {
                        status = 1;
                    }
                }
            }
            pendingOrders = new List<string>();
        }

        //상승 스레드 함수
        public async void pumping_logic_up()
        {
            try
            {
                while (running)
                {
                    if (this.lastStep)
                    {

                        if (block == nblock)
                        {
                            running = false; success = true;
                            Program.pumpingStatus[pair].up.running = false;
                            Program.pumpingStatus[pair].up.thread.Abort();
                            Program.pumpingStatus[pair].up.timeThread.Abort();

                            Program.pumpingStatus[pair].up.thread = null;
                            Program.pumpingStatus[pair].up.timeThread = null;
                            continue;
                        }
                        string postdata = "";
                        if (timeElapsed == 0)
                        {
                            int i;
                            //save current buy orders.....
                            if (pendingOrders != null)
                                pendingOrders.Clear();

                            RestClient client;
                            client = new RestClient();
                            var values1 = client.getMyOrder(this.pair);
                            //매도 11~20
                            if (block != nblock - 1)
                            {
                                for (i = 0; i < 10; i++) {
                                    double req_price;
                                    req_price = Math.Round(step / 9 * i + curSellValFirst + (step + double.Parse(hoga_unit)) * (block + 1), this.precisionNumber);
                                    //randomize
                                    Random r = new Random();
                                    double quantity = (double)r.Next(10, 100) / 100;
                                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"sell\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));
                                    requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                                    if (this.running)
                                    {

                                        try
                                        {
                                            HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                            string json = await response.Content.ReadAsStringAsync();
                                        }
                                        catch (System.Threading.Tasks.TaskCanceledException e)
                                        {
                                        }
                                        catch (ArgumentNullException e)
                                        {
                                        }
                                        catch (InvalidOperationException e)
                                        {
                                        }
                                        catch (HttpRequestException e)
                                        {
                                        }

                                    }
                                }
                            }
                            //make buy order according to sell1  price   firstSellPrice ...curSellValFirst + step * block
                            double curSellBlockFirst = this.curSellValFirst + (step + double.Parse(this.hoga_unit)) * block;
                            double sum = 0;
                            for (i = 0; i < 10; i++)
                            {
                                if (curSellBlockFirst - double.Parse(this.hoga_unit) * (i + 1) >= 0)
                                {
                                    double req_price = Math.Round(curSellBlockFirst - step / 9 * (i + 1), this.precisionNumber);
                                    //수량 랜덤
                                    Random r = new Random();
                                    double quantity = (double)r.Next(10, 100) / 100;
                                    sum += quantity;

                                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"buy\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));

                                    requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                                    if (this.running)
                                    {
                                        try
                                        {
                                            HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                            string json = await response.Content.ReadAsStringAsync();
                                        }
                                        catch (System.Threading.Tasks.TaskCanceledException e)
                                        {
                                        }
                                        catch (ArgumentNullException e)
                                        {
                                        }
                                        catch (InvalidOperationException e)
                                        {
                                        }
                                        catch (HttpRequestException e)
                                        {
                                        }
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }

                            if (curSellBlockFirst - double.Parse(this.hoga_unit) * (i + 1) >= 0)
                            {
                                double req_price = Math.Round(curSellBlockFirst - step / 9 * (i + 1), this.precisionNumber);
                                double quantity = sum * 2 + 10;

                                postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"buy\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));
                                requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                                if (this.running)
                                {
                                    try
                                    {
                                        HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                        string json = await response.Content.ReadAsStringAsync();
                                    }
                                    catch (System.Threading.Tasks.TaskCanceledException e)
                                    {
                                    }
                                    catch (ArgumentNullException e)
                                    {
                                    }
                                    catch (InvalidOperationException e)
                                    {
                                    }
                                    catch (HttpRequestException e)
                                    {
                                    }
                                }
                            }
                            else
                            {
                                double req_price = Math.Round(curSellBlockFirst - step / 9 * (i - 1 + 1), this.precisionNumber);
                                double quantity = sum * 2 + 10;

                                postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"buy\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));
                                requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                                if (this.running)
                                {
                                    try
                                    {
                                        HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                        string json = await response.Content.ReadAsStringAsync();
                                    }
                                    catch (System.Threading.Tasks.TaskCanceledException e)
                                    {
                                    }
                                    catch (ArgumentNullException e)
                                    {
                                    }
                                    catch (InvalidOperationException e)
                                    {
                                    }
                                    catch (HttpRequestException e)
                                    {
                                    }
                                }
                            }
                            //prev order cancel
                            for (i = 0; i < values1.open_orders.Count; i++)
                            {
                                postdata = "{\"id\" : " + values1.open_orders[i].id + " , \"api_key\" : \"" + Program.api_key + "\"}";
                                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"cancel_order?market=" + CommonFunc.getPairNameInCoinSky(pair));
                                requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                                if (running)
                                {
                                    try
                                    {
                                        HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                        string json = await response.Content.ReadAsStringAsync();
                                    }
                                    catch (System.Threading.Tasks.TaskCanceledException e)
                                    {
                                    }
                                    catch (ArgumentNullException e)
                                    {
                                    }
                                    catch (InvalidOperationException e)
                                    {
                                    }
                                    catch (HttpRequestException e)
                                    {
                                    }
                                }
                            }
                            timeElapsed += 100;
                        }
                        else
                        {
                            //random making order
                            double curSellBlockFirst = this.curSellValFirst + (step + double.Parse(hoga_unit)) * block;
                            if ((timePass + 3000 >= timeInterval && block != (nblock - 1)) || (timePass >= lastTimeInterval && block == (nblock - 1)))
                            {

                                double req_price;
                                if (block == nblock - 1)
                                    req_price = this.destinationPrice;
                                else
                                    req_price = Math.Round(curSellValFirst + (step + double.Parse(hoga_unit)) * block + step, this.precisionNumber);

                                curPriceVal = req_price;

                                RestClient client = new RestClient();
                                var resultValues = client.getMyOrderBook(pair);
                                this.blockOrderSum = 0;

                                if (resultValues.result && resultValues.order_book.sell.Count > 0)
                                {
                                    for (int i = 0; i < resultValues.order_book.sell.Count; i++)
                                    {
                                        if (double.Parse(resultValues.order_book.sell[i].rate) > req_price)
                                            break;
                                        this.blockOrderSum += double.Parse(resultValues.order_book.sell[i].volume);
                                    }

                                    Random rnd = new Random();
                                    double addition_quantity = (double)rnd.Next(10, 100) / 100;
                                    double quantity = this.blockOrderSum + addition_quantity;
    
                                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"buy\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";

                                    this.block += 1;
                                    timeElapsed = 0;
                                    timePass = 0;
                                    //time time time
                                    lastTimeInterval = applyTimeCounter;
                                }
                            }
                            else
                            {
                                Random r3 = new Random();
                                int result = r3.Next(0, 9);
                                result = result % 2;
                                // result : 1은 매수 진행 
                                // result : 2은 매도 진행 
                                Random r1 = new Random();
                                int unit = r1.Next(0, 9);
                                double req_price;

                                if (block == nblock - 1)
                                {
                                    //req_price = curSellBlockFirst + ((double)step / 10) * unit;
                                    req_price = Math.Round(step / 9 * unit + curSellBlockFirst, this.precisionNumber);
                                    if (req_price > this.destinationPrice)
                                        continue;
                                }
                                else
                                    req_price = Math.Round(step / 9 * unit + curSellBlockFirst, this.precisionNumber);

                                Random r2 = new Random();
                                double quantity = (double)r2.Next(10, 100) / 100;

                                this.blockOrderSum += quantity;

                                if (result == 1)
                                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"buy\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                                else
                                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"sell\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";       

                                timeElapsed += 100;
                            }

                            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));
                            requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                            if (running)
                            {
                                try
                                {
                                    HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                    string json = await response.Content.ReadAsStringAsync();
                                }
                                catch (System.Threading.Tasks.TaskCanceledException e)
                                {
                                }
                                catch (ArgumentNullException e)
                                {
                                }
                                catch (InvalidOperationException e)
                                {
                                }
                                catch (HttpRequestException e)
                                {
                                }
                            }
                        }

                        Thread.Sleep((int)(double.Parse(apiCallTime) * 1000));
                    }
                }
            }
            catch
            {
            }
        }
        //pumping logic down function
        public async void pumping_logic_down()
        {
            try
            {
                while (running)
                {
                    if (this.lastStep)
                    {

                        if (block == nblock)
                        {
                            running = false; success = true;
                            Program.pumpingStatus[pair].down.running = false;
                            Program.pumpingStatus[pair].down.thread.Abort();
                            Program.pumpingStatus[pair].down.timeThread.Abort();

                            Program.pumpingStatus[pair].down.thread = null;
                            Program.pumpingStatus[pair].down.timeThread = null;
                            continue;
                        }
                        string postdata = "";
                        if (timeElapsed == 0)
                        {
                            int i;
                            //save current sell order
                            if (pendingOrders != null)
                                pendingOrders.Clear();

                            RestClient client;
                            client = new RestClient();
                            var values1 = client.getMyOrder(this.pair);

                            //블록 시작시  매수 11~20까지
                            if (block != nblock - 1)
                            {
                                for (i = 0; i < 10; i++)
                                {
                                    double req_price;
                                    req_price = Math.Round(curBuyValFirst - step / 9 * i - (step + double.Parse(hoga_unit)) * (block + 1), this.precisionNumber);
                                    if (req_price <= 0)
                                    {
                                        ///////////////
                                        ///need Exception
                                        ////////////////
                                        break;
                                    }

                                    //randomize
                                    Random r = new Random();
                                    double quantity = (double)r.Next(10, 100) / 100;
                                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"buy\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));
                                    requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                                    if (this.running)
                                    {
                                        try
                                        {
                                            HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                            string json = await response.Content.ReadAsStringAsync();
                                        }
                                        catch (System.Threading.Tasks.TaskCanceledException e)
                                        {
                                        }
                                        catch (ArgumentNullException e)
                                        {
                                        }
                                        catch (InvalidOperationException e)
                                        {
                                        }
                                        catch (HttpRequestException e)
                                        {
                                        }
                                    }
                                }
                            }
                            //make order  ...curSellValFirst - step * block
                            double curBuyBlockFirst = this.curBuyValFirst - (step + double.Parse(this.hoga_unit)) * block;
                            double sum = 0;
                            for (i = 0; i < 10; i++)
                            {

                                if (curBuyBlockFirst + double.Parse(this.hoga_unit) * (i + 1) >= 0)
                                {
                                    double req_price = Math.Round(curBuyBlockFirst + step / 9 * (i + 1), this.precisionNumber);
                                    //randomize
                                    Random r = new Random();
                                    double quantity = (double)r.Next(10, 100) / 100;
                                    sum += quantity;

                                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"sell\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));

                                    requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");
                                    if (this.running)
                                    {
                                        try
                                        {
                                            HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                            string json = await response.Content.ReadAsStringAsync();
                                        }
                                        catch (System.Threading.Tasks.TaskCanceledException e)
                                        {
                                        }
                                        catch (ArgumentNullException e)
                                        {
                                        }
                                        catch (InvalidOperationException e)
                                        {
                                        }
                                        catch (HttpRequestException e)
                                        {
                                        }

                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }

                            {
                                double req_price = Math.Round(curBuyBlockFirst + step / 9 * (i + 1), this.precisionNumber);
                                double quantity = sum * 2 + 10;                           

                                postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"sell\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));
                                requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                                if (this.running)
                                {
                                    try
                                    {
                                        HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                        string json = await response.Content.ReadAsStringAsync();
                                    }
                                    catch (System.Threading.Tasks.TaskCanceledException e)
                                    {
                                    }
                                    catch (ArgumentNullException e)
                                    {
                                    }
                                    catch (InvalidOperationException e)
                                    {
                                    }
                                    catch (HttpRequestException e)
                                    {
                                    }
                                }
                            }
                            //이미전 매수 취소 한다. 
                            for (i = 0; i < values1.open_orders.Count; i ++)
                            {
                                postdata = "{\"id\" : " + values1.open_orders[i].id + " , \"api_key\" : \"" + Program.api_key + "\"}";
                                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"cancel_order?market=" + CommonFunc.getPairNameInCoinSky(pair));
                                requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                                if (running)
                                {
                                    try
                                    {
                                        HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                        string json = await response.Content.ReadAsStringAsync();
                                    }
                                    catch (System.Threading.Tasks.TaskCanceledException e)
                                    {
                                    }
                                    catch (ArgumentNullException e)
                                    {
                                    }
                                    catch (InvalidOperationException e)
                                    {
                                    }
                                    catch (HttpRequestException e)
                                    {
                                    }
                                }
                            }
                            timeElapsed += 100;
                        }
                        else
                        {
                            //random making order
                            double curBuyBlockFirst = this.curBuyValFirst - (step + double.Parse(hoga_unit)) * block;
                            if ((timePass + 3000 >= timeInterval && block != (nblock - 1)) || (timePass >= lastTimeInterval && block == (nblock - 1)))
                            {
                                double req_price;
                                if (block == nblock - 1)
                                    req_price = this.destinationPrice;
                                else
                                    req_price = Math.Round(curBuyValFirst - (step + double.Parse(hoga_unit)) * block - step, this.precisionNumber);

                                curPriceVal = req_price;

                                RestClient client = new RestClient();
                                var resultValues = client.getMyOrderBook(pair);
                                this.blockOrderSum = 0;

                                if (resultValues.result && resultValues.order_book.buy.Count > 0)
                                {
                                        for (int i = 0; i < resultValues.order_book.buy.Count; i++) { 
                                            if (double.Parse(resultValues.order_book.buy[i].rate) < req_price)
                                                break;
                                            this.blockOrderSum += double.Parse(resultValues.order_book.buy[i].volume);
                                        }

                                        Random rnd = new Random();
                                        double addition_quantity = (double)rnd.Next(10, 100) / 100;
                                        double quantity = this.blockOrderSum + addition_quantity;
                                        postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"sell\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                                        this.block += 1;
                                        timeElapsed = 0;
                                        timePass = 0;
                                        lastTimeInterval = applyTimeCounter;
                                }
                            }
                            else
                            {

                                Random r3 = new Random();
                                int result = r3.Next(0, 9);
                                result = result % 2;

                                Random r1 = new Random();
                                int unit = r1.Next(0, 9);
                                double req_price;

                                if (block == nblock - 1)
                                {
                                    req_price = Math.Round(curBuyBlockFirst - step / 9 * unit, this.precisionNumber);
                                    if (req_price < this.destinationPrice)
                                        continue;
                                }
                                else
                                    req_price = Math.Round(curBuyBlockFirst - step / 9 * unit, this.precisionNumber);

                                Random r2 = new Random();
                                double quantity = (double)r2.Next(10, 100) / 100;

                                this.blockOrderSum += quantity;

                                if (result == 1)
                                    postdata = "{\"side\" : \"S\" , \"type\" : \"L\" ,\"qty\" : " + quantity + ", \"price\" : " + req_price + "}";
                                else
                                    postdata = "{\"side\" : \"B\" , \"type\" : \"L\" ,\"qty\" : " + quantity + ", \"price\" : " + req_price + "}";

                                if (result == 1)
                                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"sell\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";
                                else
                                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"buy\" ,\"volume\" : " + quantity + ", \"rate\" : " + req_price + " , \"price\" : " + (req_price * quantity) + "}";

                                timeElapsed += 100;
                            }

                            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + CommonFunc.getPairNameInCoinSky(pair));
                            requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                            if (running)
                            {
                                try
                                {
                                    HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                                    string json = await response.Content.ReadAsStringAsync();
                                }
                                catch (System.Threading.Tasks.TaskCanceledException e)
                                {
                                }
                                catch (ArgumentNullException e)
                                {
                                }
                                catch (InvalidOperationException e)
                                {
                                }
                                catch (HttpRequestException e)
                                {
                                }
                            }
                        }

                        Thread.Sleep((int)(double.Parse(apiCallTime) * 1000));

                    }
                }
            }
            catch
            {
            }
        }
        public void follow_basecoin()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(3500);
                    if (running)
                    {
                        /*RestClient client = new RestClient();
                        var values1 = client.getMyOrder(pair);
                        client = new RestClient();
                        var values2 = client.getMyOrderBook(pair);

                        if (values2.success && values2.d.item.Count > 0)
                        {
                            int i = 0;
                            HttpClient httpClient = new HttpClient();
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            for (i = 0; i < values2.d.item.Count; i++)
                            {
                                double quantity = (double)values2.d.item[i].sum / values2.d.item[i].price;

                                if (quantity <= 0.01)
                                {
                                    Random rnd = new Random();
                                    quantity = rnd.Next(10, 20);
                                    CommonFunc.sendOrderPost(httpClient, quantity, values2.d.item[i].price, pair, values2.d.item[i].side, true);
                                }
                            }
                        }
                        CommonFunc.CheckSyncFromServer(pair, values1.d.item);
                        if (!running)
                        {
                            BITTREX_CLASS.connectBitTrexWebSocket("XRP/BTC");
                            running = true;
                        }
                        */
                    }
                }
            }
            catch
            {
            }
        }

        public void timeCounter()
        {
            while (running)
            {

                if (lastStep)
                {
                    Thread.Sleep(1000);
                    if (direction == "up" || direction == "down")
                    {
                        timePass += 1000;
                        if ((timePass <= timeInterval && block != (nblock - 1)) && applyTimeCounter >= 0)
                        {
                            applyTimeCounter -= 1000;
                        }
                        else if (block == (nblock - 1) && applyTimeCounter >= 1000)
                        {
                            applyTimeCounter -= 1000;
                        }
                    }
                }
                else if (direction == "baseCoin")
                {
                    Thread.Sleep(1000);
                    if (applyTimeCounter >= 1000)
                    {
                        applyTimeCounter -= 1000;

                        if (applyTimeCounter == 0)
                        {
                            running = false;
                            Program.pumpingStatus[pair].baseCoin.running = false;
                            Program.pumpingStatus[pair].baseCoin.thread.Abort();
                            Program.pumpingStatus[pair].baseCoin.timeThread.Abort();
                            Program.pumpingStatus[pair].baseCoin.thread = null;
                            Program.pumpingStatus[pair].baseCoin.timeThread = null;
                            continue;
                        }
                    }
                }
            }
        }

    }
}
