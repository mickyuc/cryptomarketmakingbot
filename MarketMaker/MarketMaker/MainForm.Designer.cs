﻿namespace MarketMaker
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.BTN_APIKEY = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.BTN_LOG = new System.Windows.Forms.Button();
            this.BTN_SETPROPERTY = new System.Windows.Forms.Button();
            this.BTN_PUMPING = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.FEE_SPIN = new System.Windows.Forms.NumericUpDown();
            this.MIN_SPIN = new System.Windows.Forms.NumericUpDown();
            this.UNIT_SPIN = new System.Windows.Forms.NumericUpDown();
            this.BTN_CANCEL = new System.Windows.Forms.Button();
            this.IDOK = new System.Windows.Forms.Button();
            this.COMB_BA_SITE = new System.Windows.Forms.ComboBox();
            this.COMB_CNY = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.COMB_SITE = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.RATE_SPIN = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.PREMIUM_SPIN = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.FEE = new System.Windows.Forms.Label();
            this.TIME = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.API_CALL_SPIN = new System.Windows.Forms.NumericUpDown();
            this.BTN_ALL_CANCEL = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pair = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Basic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Premium = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MinOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Method = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel7 = new System.Windows.Forms.Panel();
            this.GLOBAL_LABEL = new System.Windows.Forms.Label();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.panel8 = new System.Windows.Forms.Panel();
            this.initialize = new System.Windows.Forms.CheckBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FEE_SPIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MIN_SPIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UNIT_SPIN)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RATE_SPIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PREMIUM_SPIN)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.API_CALL_SPIN)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // BTN_APIKEY
            // 
            this.BTN_APIKEY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_APIKEY.Font = new System.Drawing.Font("Yu Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_APIKEY.Location = new System.Drawing.Point(8, 6);
            this.BTN_APIKEY.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.BTN_APIKEY.Name = "BTN_APIKEY";
            this.BTN_APIKEY.Size = new System.Drawing.Size(234, 34);
            this.BTN_APIKEY.TabIndex = 2;
            this.BTN_APIKEY.Text = "APIKEY 설정";
            this.BTN_APIKEY.UseVisualStyleBackColor = true;
            this.BTN_APIKEY.Click += new System.EventHandler(this.APIKEY_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 195);
            this.panel1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.BTN_LOG, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.BTN_APIKEY, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.BTN_SETPROPERTY, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.BTN_PUMPING, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(-1, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(250, 186);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // BTN_LOG
            // 
            this.BTN_LOG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_LOG.Font = new System.Drawing.Font("Yu Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_LOG.Location = new System.Drawing.Point(8, 144);
            this.BTN_LOG.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.BTN_LOG.Name = "BTN_LOG";
            this.BTN_LOG.Size = new System.Drawing.Size(234, 36);
            this.BTN_LOG.TabIndex = 5;
            this.BTN_LOG.Text = "로그보기";
            this.BTN_LOG.UseVisualStyleBackColor = true;
            this.BTN_LOG.Click += new System.EventHandler(this.LOG_Click);
            // 
            // BTN_SETPROPERTY
            // 
            this.BTN_SETPROPERTY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_SETPROPERTY.Font = new System.Drawing.Font("Yu Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_SETPROPERTY.Location = new System.Drawing.Point(8, 52);
            this.BTN_SETPROPERTY.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.BTN_SETPROPERTY.Name = "BTN_SETPROPERTY";
            this.BTN_SETPROPERTY.Size = new System.Drawing.Size(234, 34);
            this.BTN_SETPROPERTY.TabIndex = 3;
            this.BTN_SETPROPERTY.Text = "초기자산 설정 / 자산 현황";
            this.BTN_SETPROPERTY.UseVisualStyleBackColor = true;
            this.BTN_SETPROPERTY.Click += new System.EventHandler(this.SETPROPERTY_Click);
            // 
            // BTN_PUMPING
            // 
            this.BTN_PUMPING.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_PUMPING.Font = new System.Drawing.Font("Yu Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_PUMPING.Location = new System.Drawing.Point(8, 98);
            this.BTN_PUMPING.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.BTN_PUMPING.Name = "BTN_PUMPING";
            this.BTN_PUMPING.Size = new System.Drawing.Size(234, 34);
            this.BTN_PUMPING.TabIndex = 4;
            this.BTN_PUMPING.Text = "펌핑 설정";
            this.BTN_PUMPING.UseVisualStyleBackColor = true;
            this.BTN_PUMPING.Click += new System.EventHandler(this.PUMPING_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Location = new System.Drawing.Point(5, 204);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(250, 385);
            this.panel2.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.FEE_SPIN, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.MIN_SPIN, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.UNIT_SPIN, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.BTN_CANCEL, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.IDOK, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.COMB_BA_SITE, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.COMB_CNY, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label23, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label21, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label19, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.COMB_SITE, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel6, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.FEE, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.TIME, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 8);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(-1, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 10;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.831169F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.90909F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(250, 385);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // FEE_SPIN
            // 
            this.FEE_SPIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FEE_SPIN.Location = new System.Drawing.Point(125, 275);
            this.FEE_SPIN.Margin = new System.Windows.Forms.Padding(0, 9, 20, 3);
            this.FEE_SPIN.Name = "FEE_SPIN";
            this.FEE_SPIN.Size = new System.Drawing.Size(105, 20);
            this.FEE_SPIN.TabIndex = 35;
            this.FEE_SPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // MIN_SPIN
            // 
            this.MIN_SPIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MIN_SPIN.Location = new System.Drawing.Point(125, 237);
            this.MIN_SPIN.Margin = new System.Windows.Forms.Padding(0, 9, 20, 3);
            this.MIN_SPIN.Name = "MIN_SPIN";
            this.MIN_SPIN.Size = new System.Drawing.Size(105, 20);
            this.MIN_SPIN.TabIndex = 34;
            this.MIN_SPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // UNIT_SPIN
            // 
            this.UNIT_SPIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UNIT_SPIN.Location = new System.Drawing.Point(125, 199);
            this.UNIT_SPIN.Margin = new System.Windows.Forms.Padding(0, 9, 20, 3);
            this.UNIT_SPIN.Name = "UNIT_SPIN";
            this.UNIT_SPIN.Size = new System.Drawing.Size(105, 20);
            this.UNIT_SPIN.TabIndex = 15;
            this.UNIT_SPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // BTN_CANCEL
            // 
            this.BTN_CANCEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_CANCEL.Font = new System.Drawing.Font("Yu Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_CANCEL.Location = new System.Drawing.Point(125, 349);
            this.BTN_CANCEL.Margin = new System.Windows.Forms.Padding(0, 7, 12, 7);
            this.BTN_CANCEL.Name = "BTN_CANCEL";
            this.BTN_CANCEL.Size = new System.Drawing.Size(113, 29);
            this.BTN_CANCEL.TabIndex = 33;
            this.BTN_CANCEL.Text = "중지/주문취소";
            this.BTN_CANCEL.UseVisualStyleBackColor = true;
            this.BTN_CANCEL.Click += new System.EventHandler(this.BTN_CANCEL_Click);
            // 
            // IDOK
            // 
            this.IDOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.IDOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IDOK.Font = new System.Drawing.Font("Yu Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDOK.Location = new System.Drawing.Point(30, 349);
            this.IDOK.Margin = new System.Windows.Forms.Padding(30, 7, 10, 7);
            this.IDOK.Name = "IDOK";
            this.IDOK.Size = new System.Drawing.Size(85, 29);
            this.IDOK.TabIndex = 32;
            this.IDOK.Text = "시작";
            this.IDOK.UseVisualStyleBackColor = true;
            this.IDOK.Click += new System.EventHandler(this.IDOK_Click);
            // 
            // COMB_BA_SITE
            // 
            this.COMB_BA_SITE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.COMB_BA_SITE.Enabled = false;
            this.COMB_BA_SITE.FormattingEnabled = true;
            this.COMB_BA_SITE.Location = new System.Drawing.Point(125, 85);
            this.COMB_BA_SITE.Margin = new System.Windows.Forms.Padding(0, 9, 18, 0);
            this.COMB_BA_SITE.Name = "COMB_BA_SITE";
            this.COMB_BA_SITE.Size = new System.Drawing.Size(107, 21);
            this.COMB_BA_SITE.TabIndex = 23;
            // 
            // COMB_CNY
            // 
            this.COMB_CNY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.COMB_CNY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.COMB_CNY.FormattingEnabled = true;
            this.COMB_CNY.Location = new System.Drawing.Point(125, 47);
            this.COMB_CNY.Margin = new System.Windows.Forms.Padding(0, 9, 18, 0);
            this.COMB_CNY.Name = "COMB_CNY";
            this.COMB_CNY.Size = new System.Drawing.Size(107, 21);
            this.COMB_CNY.TabIndex = 22;
            this.COMB_CNY.TextChanged += new System.EventHandler(this.COMB_CNY_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(12, 228);
            this.label23.Margin = new System.Windows.Forms.Padding(12, 0, 3, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(110, 38);
            this.label23.TabIndex = 20;
            this.label23.Text = "최소 주문수량";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(12, 190);
            this.label21.Margin = new System.Windows.Forms.Padding(12, 0, 3, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(110, 38);
            this.label21.TabIndex = 18;
            this.label21.Text = "호가 단위";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(12, 152);
            this.label19.Margin = new System.Windows.Forms.Padding(12, 0, 3, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 38);
            this.label19.TabIndex = 16;
            this.label19.Text = "프리 미엄";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 110);
            this.label9.Margin = new System.Windows.Forms.Padding(12, 0, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 42);
            this.label9.TabIndex = 14;
            this.label9.Text = "수량 비율";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 76);
            this.label6.Margin = new System.Windows.Forms.Padding(12, 0, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 34);
            this.label6.TabIndex = 12;
            this.label6.Text = "기준 거래소";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 38);
            this.label3.Margin = new System.Windows.Forms.Padding(12, 0, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 38);
            this.label3.TabIndex = 10;
            this.label3.Text = "페어";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(12, 0, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 38);
            this.label2.TabIndex = 8;
            this.label2.Text = "메이킹 거래소";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // COMB_SITE
            // 
            this.COMB_SITE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.COMB_SITE.Enabled = false;
            this.COMB_SITE.FormattingEnabled = true;
            this.COMB_SITE.Location = new System.Drawing.Point(125, 9);
            this.COMB_SITE.Margin = new System.Windows.Forms.Padding(0, 9, 18, 0);
            this.COMB_SITE.Name = "COMB_SITE";
            this.COMB_SITE.Size = new System.Drawing.Size(107, 21);
            this.COMB_SITE.TabIndex = 5;
            // 
            // panel5
            // 
            this.panel5.AutoSize = true;
            this.panel5.Controls.Add(this.RATE_SPIN);
            this.panel5.Controls.Add(this.numericUpDown5);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(128, 113);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(0, 9, 40, 0);
            this.panel5.Size = new System.Drawing.Size(119, 36);
            this.panel5.TabIndex = 21;
            // 
            // RATE_SPIN
            // 
            this.RATE_SPIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RATE_SPIN.Location = new System.Drawing.Point(0, 9);
            this.RATE_SPIN.Margin = new System.Windows.Forms.Padding(3, 10, 20, 3);
            this.RATE_SPIN.Name = "RATE_SPIN";
            this.RATE_SPIN.Size = new System.Drawing.Size(79, 20);
            this.RATE_SPIN.TabIndex = 17;
            this.RATE_SPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericUpDown5.Location = new System.Drawing.Point(0, 9);
            this.numericUpDown5.Margin = new System.Windows.Forms.Padding(3, 10, 20, 3);
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(79, 20);
            this.numericUpDown5.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(85, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "%";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.PREMIUM_SPIN);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Location = new System.Drawing.Point(128, 155);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(0, 7, 40, 0);
            this.panel6.Size = new System.Drawing.Size(119, 32);
            this.panel6.TabIndex = 24;
            // 
            // PREMIUM_SPIN
            // 
            this.PREMIUM_SPIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PREMIUM_SPIN.Location = new System.Drawing.Point(0, 7);
            this.PREMIUM_SPIN.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.PREMIUM_SPIN.Name = "PREMIUM_SPIN";
            this.PREMIUM_SPIN.Size = new System.Drawing.Size(79, 20);
            this.PREMIUM_SPIN.TabIndex = 18;
            this.PREMIUM_SPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(84, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 5, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "%";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FEE
            // 
            this.FEE.AutoSize = true;
            this.FEE.Dock = System.Windows.Forms.DockStyle.Left;
            this.FEE.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FEE.Location = new System.Drawing.Point(12, 266);
            this.FEE.Margin = new System.Windows.Forms.Padding(12, 0, 3, 0);
            this.FEE.Name = "FEE";
            this.FEE.Size = new System.Drawing.Size(44, 38);
            this.FEE.TabIndex = 27;
            this.FEE.Text = "수수료";
            this.FEE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TIME
            // 
            this.TIME.AutoSize = true;
            this.TIME.Dock = System.Windows.Forms.DockStyle.Left;
            this.TIME.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TIME.Location = new System.Drawing.Point(12, 304);
            this.TIME.Margin = new System.Windows.Forms.Padding(12, 0, 3, 0);
            this.TIME.Name = "TIME";
            this.TIME.Size = new System.Drawing.Size(79, 38);
            this.TIME.TabIndex = 28;
            this.TIME.Text = "API 호출시간";
            this.TIME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.API_CALL_SPIN, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(125, 307);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(122, 32);
            this.tableLayoutPanel4.TabIndex = 36;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(95, 5);
            this.label4.Margin = new System.Windows.Forms.Padding(10, 5, 12, 0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.label4.Size = new System.Drawing.Size(15, 27);
            this.label4.TabIndex = 37;
            this.label4.Text = "초";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // API_CALL_SPIN
            // 
            this.API_CALL_SPIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.API_CALL_SPIN.Location = new System.Drawing.Point(0, 6);
            this.API_CALL_SPIN.Margin = new System.Windows.Forms.Padding(0, 6, 0, 3);
            this.API_CALL_SPIN.Name = "API_CALL_SPIN";
            this.API_CALL_SPIN.Size = new System.Drawing.Size(85, 20);
            this.API_CALL_SPIN.TabIndex = 36;
            this.API_CALL_SPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // BTN_ALL_CANCEL
            // 
            this.BTN_ALL_CANCEL.Font = new System.Drawing.Font("Yu Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ALL_CANCEL.Location = new System.Drawing.Point(15, 15);
            this.BTN_ALL_CANCEL.Name = "BTN_ALL_CANCEL";
            this.BTN_ALL_CANCEL.Size = new System.Drawing.Size(220, 30);
            this.BTN_ALL_CANCEL.TabIndex = 4;
            this.BTN_ALL_CANCEL.Text = "모든 주문 취소/중지";
            this.BTN_ALL_CANCEL.UseVisualStyleBackColor = true;
            this.BTN_ALL_CANCEL.Click += new System.EventHandler(this.BTN_ALL_CANCEL_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.BTN_ALL_CANCEL);
            this.panel3.Location = new System.Drawing.Point(5, 593);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(250, 60);
            this.panel3.TabIndex = 20;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.tableLayoutPanel3);
            this.panel4.Location = new System.Drawing.Point(261, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(720, 60);
            this.panel4.TabIndex = 21;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 8;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.Controls.Add(this.label17, 7, 0);
            this.tableLayoutPanel3.Controls.Add(this.label13, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label12, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label16, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.label15, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.label11, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label14, 4, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(712, 52);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Yu Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(642, 12);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 12, 3, 12);
            this.label17.Name = "label17";
            this.label17.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.label17.Size = new System.Drawing.Size(67, 28);
            this.label17.TabIndex = 29;
            this.label17.Text = "거래중";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Leelawadee UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(287, 12);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 12, 3, 12);
            this.label13.Name = "label13";
            this.label13.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.label13.Size = new System.Drawing.Size(100, 28);
            this.label13.TabIndex = 27;
            this.label13.Text = "0$";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(198, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 52);
            this.label12.TabIndex = 22;
            this.label12.Text = "현재 손실액";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(553, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 52);
            this.label16.TabIndex = 26;
            this.label16.Text = "상태";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Leelawadee UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(482, 12);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 12, 3, 12);
            this.label15.Name = "label15";
            this.label15.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.label15.Size = new System.Drawing.Size(65, 28);
            this.label15.TabIndex = 28;
            this.label15.Text = "10$";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Leelawadee UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(92, 12);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 12, 3, 12);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.label11.Size = new System.Drawing.Size(100, 28);
            this.label11.TabIndex = 21;
            this.label11.Text = "0$";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 52);
            this.label10.TabIndex = 20;
            this.label10.Text = "허용 손실액";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(393, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 52);
            this.label14.TabIndex = 24;
            this.label14.Text = "수수료";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.Pair,
            this.Basic,
            this.Rate,
            this.Premium,
            this.Unit,
            this.MinOrder,
            this.Status,
            this.Method});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dataGridView1.Location = new System.Drawing.Point(261, 108);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView1.Size = new System.Drawing.Size(720, 545);
            this.dataGridView1.TabIndex = 22;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // No
            // 
            this.No.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.No.Frozen = true;
            this.No.HeaderText = "번호";
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.No.Width = 50;
            // 
            // Pair
            // 
            this.Pair.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Pair.Frozen = true;
            this.Pair.HeaderText = "페어";
            this.Pair.Name = "Pair";
            this.Pair.ReadOnly = true;
            this.Pair.Width = 90;
            // 
            // Basic
            // 
            this.Basic.Frozen = true;
            this.Basic.HeaderText = "기준 거래소";
            this.Basic.Name = "Basic";
            this.Basic.ReadOnly = true;
            this.Basic.Width = 90;
            // 
            // Rate
            // 
            this.Rate.Frozen = true;
            this.Rate.HeaderText = "수량비율";
            this.Rate.Name = "Rate";
            this.Rate.ReadOnly = true;
            this.Rate.Width = 75;
            // 
            // Premium
            // 
            this.Premium.Frozen = true;
            this.Premium.HeaderText = "프리미엄";
            this.Premium.Name = "Premium";
            this.Premium.ReadOnly = true;
            this.Premium.Width = 75;
            // 
            // Unit
            // 
            this.Unit.Frozen = true;
            this.Unit.HeaderText = "호가단위";
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            this.Unit.Width = 76;
            // 
            // MinOrder
            // 
            this.MinOrder.Frozen = true;
            this.MinOrder.HeaderText = "최소주문량";
            this.MinOrder.Name = "MinOrder";
            this.MinOrder.ReadOnly = true;
            this.MinOrder.Width = 87;
            // 
            // Status
            // 
            this.Status.Frozen = true;
            this.Status.HeaderText = "상태";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 54;
            // 
            // Method
            // 
            this.Method.HeaderText = "조작";
            this.Method.Name = "Method";
            this.Method.ReadOnly = true;
            this.Method.Text = "중지";
            this.Method.UseColumnTextForButtonValue = true;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.GLOBAL_LABEL);
            this.panel7.Location = new System.Drawing.Point(262, 69);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(517, 34);
            this.panel7.TabIndex = 23;
            // 
            // GLOBAL_LABEL
            // 
            this.GLOBAL_LABEL.AutoSize = true;
            this.GLOBAL_LABEL.Font = new System.Drawing.Font("Yu Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GLOBAL_LABEL.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.GLOBAL_LABEL.Location = new System.Drawing.Point(37, 8);
            this.GLOBAL_LABEL.Name = "GLOBAL_LABEL";
            this.GLOBAL_LABEL.Size = new System.Drawing.Size(0, 17);
            this.GLOBAL_LABEL.TabIndex = 0;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.initialize);
            this.panel8.Location = new System.Drawing.Point(785, 69);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(195, 34);
            this.panel8.TabIndex = 24;
            // 
            // initialize
            // 
            this.initialize.AutoSize = true;
            this.initialize.Location = new System.Drawing.Point(26, 9);
            this.initialize.Name = "initialize";
            this.initialize.Size = new System.Drawing.Size(81, 17);
            this.initialize.TabIndex = 0;
            this.initialize.Text = "자동재연결";
            this.initialize.UseVisualStyleBackColor = true;
            this.initialize.CheckedChanged += new System.EventHandler(this.initialize_CheckedChanged);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 660);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "오더 메이킹";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FEE_SPIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MIN_SPIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UNIT_SPIN)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RATE_SPIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PREMIUM_SPIN)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.API_CALL_SPIN)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BTN_APIKEY;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BTN_LOG;
        private System.Windows.Forms.Button BTN_PUMPING;
        private System.Windows.Forms.Button BTN_SETPROPERTY;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox COMB_SITE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BTN_ALL_CANCEL;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox COMB_BA_SITE;
        private System.Windows.Forms.ComboBox COMB_CNY;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label FEE;
        private System.Windows.Forms.Label TIME;
        private System.Windows.Forms.Button BTN_CANCEL;
        private System.Windows.Forms.Button IDOK;
        private System.Windows.Forms.NumericUpDown UNIT_SPIN;
        private System.Windows.Forms.NumericUpDown FEE_SPIN;
        private System.Windows.Forms.NumericUpDown MIN_SPIN;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.NumericUpDown RATE_SPIN;
        private System.Windows.Forms.NumericUpDown PREMIUM_SPIN;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown API_CALL_SPIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn No;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pair;
        private System.Windows.Forms.DataGridViewTextBoxColumn Basic;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Premium;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn MinOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewButtonColumn Method;
        private System.Windows.Forms.Panel panel7;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Label GLOBAL_LABEL;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.CheckBox initialize;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

