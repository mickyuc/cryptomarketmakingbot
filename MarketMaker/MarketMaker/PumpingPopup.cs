﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MarketMaker
{
    public partial class PumpingPopup : Form
    {
        public string selectedPair = "", selectedDirection = "";
        public bool first = false;
        public bool backgroundWorking = true;

        public PumpingPopup()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void PumpingPopup_Load(object sender, EventArgs e)
        {
            //메이킹 거래소 
            this.MAKING_SITE.Items.Add("CoinSky");
            this.MAKING_SITE.SelectedIndex = 0;
            this.MAKING_SITE.Enabled = false;

            //패어
            this.PAIR_COIN.Items.Add("SKY/KRW");
            this.PAIR_COIN.Items.Add("BDR/KRW");
            this.PAIR_COIN.SelectedIndex = 0;
            this.PAIR_COIN.Enabled = true;

            //방향
            this.DIRECT_PUMPING.Items.Add("상승");
            this.DIRECT_PUMPING.Items.Add("하락");
            this.DIRECT_PUMPING.Items.Add("기준코인");
            this.DIRECT_PUMPING.SelectedIndex = 2;
            this.DIRECT_PUMPING.Enabled = true;

            this.BASIC_COIN.Items.Add("Upbit-BTC");
            this.BASIC_COIN.Items.Add("Upbit-ETH");

            this.PREMIUM_SPIN.Visible = true;
            this.PREMIUM_SPIN.Visible = true;
            this.DESTINATION_SPIN.Visible = false;
            this.BASIC_COIN.Visible = true;
            this.FIRST_VALUE.Visible = false;
            this.CONFIG_YN.Visible = false;
            this.label7.Text = "기준코인";
            this.label3.Visible = false;
            this.BASIC_COIN.SelectedIndex = 0;

            selectedPair = "SKY/KRW";
            selectedDirection = "기준코인";

            //적용 시간
            this.APPLY_TIME.Value = 20;
            this.APPLY_TIME.DecimalPlaces = 0;
            this.APPLY_TIME.Increment = 1M;
            this.APPLY_TIME.Minimum = 0;

            //호가 단위
            this.UNIT_SPIN.Value = 0;
            this.UNIT_SPIN.DecimalPlaces = 0;
            this.UNIT_SPIN.Increment = 1M;
            this.UNIT_SPIN.Minimum = 0;

            //최소 주문수량
            this.MIN_SPIN.Value = 0.000M;
            this.MIN_SPIN.DecimalPlaces = 3;
            this.MIN_SPIN.Increment = 0.001M;
            this.MIN_SPIN.Minimum = 0;

            //수량비율
            this.RATE_SPIN1.Value = 100;
            this.RATE_SPIN1.DecimalPlaces = 0;
            this.RATE_SPIN1.Increment = 1;
            this.RATE_SPIN1.Minimum = 0;
            this.RATE_SPIN1.Maximum = 100;

            //spin 설정
            //프리미움 설정
            this.PREMIUM_SPIN.Value = 0;
            this.PREMIUM_SPIN.DecimalPlaces = 2;
            this.PREMIUM_SPIN.Increment = 0.01M;
            this.PREMIUM_SPIN.Maximum = 100;
            this.PREMIUM_SPIN.Minimum = -100;

            //delay 설정
            this.API_CALLTIME.Value = 0.1M;
            this.API_CALLTIME.DecimalPlaces = 3;
            this.API_CALLTIME.Increment = 0.001M;
            this.PREMIUM_SPIN.Minimum = 0;


            if (!Program.pumpingStatus[selectedPair].up.running)
            {
                this.IDOK.Enabled = true;
                this.IDCANCEL.Enabled = false;
            }
            else
            {
                this.IDOK.Enabled = false;
                this.IDCANCEL.Enabled = true;
            }

            first = true;
            Program.runningPumpingPair = 0;

            foreach (KeyValuePair<string, PumpingSetting> item in Program.pumpingStatus)
            {
                if (item.Value.up.running)
                {
                    AddDataTableRow(item.Value.pair, "상승");
                }
                if (item.Value.down.running)
                {
                    AddDataTableRow(item.Value.pair, "하락");
                }
                if (item.Value.baseCoin.running)
                {
                    AddDataTableRow(item.Value.pair, "기준코인");
                }
            }

            if (Program.pumpingStatus[selectedPair].up.running)
            {
                this.IDOK.Enabled = false;
                this.IDCANCEL.Enabled = true;
            }
            backgroundWorker.RunWorkerAsync();

        }

        private void DIRECT_PUMPING_TextChanged_1(object sender, EventArgs e)
        {

            selectedDirection = this.DIRECT_PUMPING.GetItemText(this.DIRECT_PUMPING.SelectedItem);

            if (first)
            {
                if (selectedDirection == "상승")
                {
                    if (Program.pumpingStatus[selectedPair].up.running)
                    {
                        this.IDOK.Enabled = false;    this.IDCANCEL.Enabled = true;
                    }
                    else
                    {
                        this.IDOK.Enabled = true;      this.IDCANCEL.Enabled = false;
                    }

                    this.PREMIUM_SPIN.Visible = false; this.label7.Text = "목표";
                    this.label1.Text = "초기값"; this.label2.Visible = false; this.CONFIG_YN.Visible = true;
                    this.DESTINATION_SPIN.Visible = true; this.BASIC_COIN.Visible = false; this.label3.Visible = true; this.FIRST_VALUE.Visible = true;
                    //목표 설정 
                    this.DESTINATION_SPIN.Value = 100;
                    this.DESTINATION_SPIN.DecimalPlaces = 0;
                    this.DESTINATION_SPIN.Increment = 1M;
                    this.DESTINATION_SPIN.Minimum = 100;

                    ///////////////////////////////////////////////////////////////
                    //spin  설정 
                    //프리미움 설정
                    this.PREMIUM_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.premium);
                    //목표 설정 
                    this.DESTINATION_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.destination);
                    //적용 시간
                    this.APPLY_TIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.applyTime);
                    //호가 단위
                    this.UNIT_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.hoga_unit);
                    //최소 주문수량
                    this.MIN_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.min_order);
                    //수량비율
                    this.RATE_SPIN1.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.rate);
                    //딜레이
                    this.API_CALLTIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.apiCallTime);

                }
                if (selectedDirection == "하락")
                {
                    if (Program.pumpingStatus[selectedPair].down.running)
                    {
                        this.IDOK.Enabled = false; this.IDCANCEL.Enabled = true;
                    }
                    else
                    {
                        this.IDOK.Enabled = true; this.IDCANCEL.Enabled = false;
                    }

                    this.PREMIUM_SPIN.Visible = false; this.label7.Text = "목표"; this.label1.Text = "초기값"; this.label2.Visible = false;
                    this.CONFIG_YN.Visible = true; this.DESTINATION_SPIN.Visible = true;
                    this.BASIC_COIN.Visible = false; this.label3.Visible = true; this.FIRST_VALUE.Visible = true;

                    //목표 설정 
                    this.DESTINATION_SPIN.Value = 100;
                    this.DESTINATION_SPIN.DecimalPlaces = 0;
                    this.DESTINATION_SPIN.Increment = 1M;
                    this.DESTINATION_SPIN.Minimum = 0;
                    ///////////////////////////////////////////////////////////////////////

                    //spin  설정 
                    //프리미움 설정
                    this.PREMIUM_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.premium);
                    //목표 설정 
                    this.DESTINATION_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.destination);
                    //적용 시간
                    this.APPLY_TIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.applyTime);
                    //호가 단위
                    this.UNIT_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.hoga_unit);
                    //최소 주문수량
                    this.MIN_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.min_order);
                    //수량비율
                    this.RATE_SPIN1.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.rate);
                    //delay
                    this.API_CALLTIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.apiCallTime);

                }
                if (selectedDirection == "기준코인")
                {

                    this.label7.Text = "기준코인";

                    if (Program.pumpingStatus[selectedPair].baseCoin.running)
                    {
                        this.IDOK.Enabled = false; this.IDCANCEL.Enabled = true;
                    }
                    else
                    {
                        this.IDOK.Enabled = true; this.IDCANCEL.Enabled = false;
                    }

                    this.PREMIUM_SPIN.Visible = true; this.label7.Text = "기준코인"; this.label1.Text = "수량비율"; this.label2.Visible = true; this.CONFIG_YN.Visible = false;
                    this.DESTINATION_SPIN.Visible = false; this.BASIC_COIN.Visible = true; this.label3.Visible = false; this.FIRST_VALUE.Visible = false;
                    /////////////////////////////////////////////////////////////////////////////////////////////
                    //spin  설정 
                    //프리미움 설정
                    this.PREMIUM_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.premium);
                    //목표 설정 
                    this.DESTINATION_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.destination);
                    //적용 시간
                    this.APPLY_TIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.applyTime);
                    //호가 단위
                    this.UNIT_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.hoga_unit);
                    //최소 주문수량
                    this.MIN_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.min_order);
                    //수량비율
                    this.RATE_SPIN1.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.rate);
                    //delay
                    this.API_CALLTIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.apiCallTime);
                }
            }
        }

        private void PAIR_COIN_TextChanged(object sender, EventArgs e)
        {
            selectedPair = this.DIRECT_PUMPING.GetItemText(this.DIRECT_PUMPING.SelectedItem);

            if (first)
            {
                if (selectedPair == "SKY/KRW")
                {
                    this.BASIC_COIN.SelectedIndex = 0;
                }
                if (selectedPair == "BDR/KRW")
                {
                    this.BASIC_COIN.SelectedIndex = 1;
                }

                if (selectedDirection == "상승")
                {
                    //spin  설정 
                    //프리미움 설정
                    this.PREMIUM_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.premium);
                    //목표 설정 
                    this.DESTINATION_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.destination);
                    //적용 시간
                    this.APPLY_TIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.applyTime);
                    //호가 단위
                    this.UNIT_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.hoga_unit);
                    //최소 주문수량
                    this.MIN_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.min_order);
                    //수량비율
                    this.RATE_SPIN1.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.rate);
                    //delay
                    this.API_CALLTIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].up.apiCallTime);
                }
                if (selectedDirection == "하락")
                {
                    //spin  설정 
                    //프리미움 설정
                    this.PREMIUM_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.premium);
                    //목표 설정 
                    this.DESTINATION_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.destination);
                    //적용 시간
                    this.APPLY_TIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.applyTime);
                    //호가 단위
                    this.UNIT_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.hoga_unit);
                    //최소 주문수량
                    this.MIN_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.min_order);
                    //수량비율
                    this.RATE_SPIN1.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.rate);
                    //delay
                    this.API_CALLTIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].down.apiCallTime);
                }
                if (selectedDirection == "기준코인")
                {
                    //spin  설정 
                    //프리미움 설정
                    this.PREMIUM_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.premium);
                    //목표 설정 
                    this.DESTINATION_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.destination);
                    //적용 시간
                    this.APPLY_TIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.applyTime);
                    //호가 단위
                    this.UNIT_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.hoga_unit);
                    //최소 주문수량
                    this.MIN_SPIN.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.min_order);
                    //수량비율
                    this.RATE_SPIN1.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.rate);
                    //delay
                    this.API_CALLTIME.Value = decimal.Parse(Program.pumpingStatus[selectedPair].baseCoin.apiCallTime);
                }
            }
        }

        private void AddDataTableRow(string selectedP , string selectedD)
        {
            /* dataGrid View */
            Program.runningPumpingPair++;
            if (selectedD == "상승")
            {
                string[] row = new string[] { Program.runningPumpingPair.ToString(), selectedP, selectedD, Program.pumpingStatus[selectedP].up.destination + "%", Program.pumpingStatus[selectedP].up.applyTime + ":00:00", Program.pumpingStatus[selectedP].up.curSellValFirst.ToString(), "1", "0%" };
                dataGridView1.Rows.Add(row);
            }
            if (selectedD == "하락")
            {
                string[] row = new string[] { Program.runningPumpingPair.ToString(), selectedP, selectedD, Program.pumpingStatus[selectedP].down.destination + "%", Program.pumpingStatus[selectedP].down.applyTime + ":00:00", Program.pumpingStatus[selectedP].down.curBuyValFirst.ToString(), "1", "0%" };
                dataGridView1.Rows.Add(row);
            }
            if (selectedD == "기준코인")
            {
                string[] row = new string[] { Program.runningPumpingPair.ToString(), selectedP, selectedD, "", Program.pumpingStatus[selectedP].baseCoin.applyTime + ":00:00", "1", "2", "0%" };
                dataGridView1.Rows.Add(row);
            }
        }


        private void getSpinnerVal()
        {

            if (selectedDirection == "상승")
            {

                //프리미움 설정
                Program.pumpingStatus[selectedPair].up.premium = this.PREMIUM_SPIN.Value.ToString();
                //목표 설정 
                Program.pumpingStatus[selectedPair].up.destination = this.DESTINATION_SPIN.Value.ToString();
                //적용 시간
                Program.pumpingStatus[selectedPair].up.applyTime = this.APPLY_TIME.Value.ToString();
                //호가 단위
                Program.pumpingStatus[selectedPair].up.hoga_unit = this.UNIT_SPIN.Value.ToString();
                //최소 주문수량
                Program.pumpingStatus[selectedPair].up.min_order = this.MIN_SPIN.Value.ToString();
                //수량비율
                Program.pumpingStatus[selectedPair].up.rate = this.PREMIUM_SPIN.Value.ToString();
                //apiCallTime
                Program.pumpingStatus[selectedPair].up.apiCallTime = this.API_CALLTIME.Value.ToString();
            }
            if (selectedDirection == "하락")
            {

                //프리미움 설정
                Program.pumpingStatus[selectedPair].down.premium = this.PREMIUM_SPIN.Value.ToString();
                //목표 설정 
                Program.pumpingStatus[selectedPair].down.destination = this.DESTINATION_SPIN.Value.ToString();
                //적용 시간
                Program.pumpingStatus[selectedPair].down.applyTime = this.APPLY_TIME.Value.ToString();
                //호가 단위
                Program.pumpingStatus[selectedPair].down.hoga_unit = this.UNIT_SPIN.Value.ToString();
                //최소 주문수량
                Program.pumpingStatus[selectedPair].down.min_order = this.MIN_SPIN.Value.ToString();
                //수량비율
                Program.pumpingStatus[selectedPair].down.rate = this.PREMIUM_SPIN.Value.ToString();
                //apiCallTime
                Program.pumpingStatus[selectedPair].down.apiCallTime = this.API_CALLTIME.Value.ToString();
            }
            if (selectedDirection == "기준코인")
            {

                //프리미움 설정
                Program.pumpingStatus[selectedPair].baseCoin.premium = this.PREMIUM_SPIN.Value.ToString();
                //목표 설정 
                Program.pumpingStatus[selectedPair].baseCoin.destination = this.DESTINATION_SPIN.Value.ToString();
                //적용 시간
                Program.pumpingStatus[selectedPair].baseCoin.applyTime = this.APPLY_TIME.Value.ToString();
                //호가 단위
                Program.pumpingStatus[selectedPair].baseCoin.hoga_unit = this.UNIT_SPIN.Value.ToString();
                //최소 주문수량
                Program.pumpingStatus[selectedPair].baseCoin.min_order = this.MIN_SPIN.Value.ToString();
                //수량비율
                Program.pumpingStatus[selectedPair].baseCoin.rate = this.PREMIUM_SPIN.Value.ToString();
                //apiCallTime
                Program.pumpingStatus[selectedPair].baseCoin.apiCallTime = this.API_CALLTIME.Value.ToString();
            }

        }


        private void CANCEL_ALL_Click(object sender, EventArgs e)
        {

            string pair, direction;
            while (dataGridView1.RowCount > 1)
            {
                pair = dataGridView1.Rows[0].Cells[1].Value.ToString();
                direction = dataGridView1.Rows[0].Cells[2].Value.ToString();
                if (direction == "상승")
                    Program.pumpingStatus[pair].up.running = false;
                if (direction == "하락")
                    Program.pumpingStatus[pair].down.running = false;
                if (direction == "기준코인")
                    Program.pumpingStatus[pair].baseCoin.running = false;
                RemoveRow(pair, direction);
            }
            this.IDOK.Enabled = true;
            this.IDCANCEL.Enabled = false;

        }
        private void RemoveRow(String selectedP , String selectedD)
        {
            string pair = "", direction = "";
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                pair = dataGridView1.Rows[i].Cells[1].Value.ToString();
                direction = dataGridView1.Rows[i].Cells[2].Value.ToString();
                if (pair == selectedP && direction == selectedD)
                {
                    dataGridView1.Rows.RemoveAt(i);
                    if (Program.runningPumpingPair > 0)
                        Program.runningPumpingPair--;
                    break;
                }
            }
            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                dataGridView1.Rows[i].Cells[0].Value = (i + 1).ToString();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {

                if (e.RowIndex == -1)
                    return;

                if (dataGridView1.RowCount - 1 == e.RowIndex)
                    return;

                string pair = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                string direction = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();

                if (direction == "상승")
                {
                    DialogResult result = MessageBox.Show(pair + "에 대한 펌핑(상승)을 중지하시겠습니까?", "중지", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    if (!result.Equals(DialogResult.OK))
                    {
                        return;
                    }
                }
                else if (direction == "하락")
                {
                    DialogResult result = MessageBox.Show(pair + "에 대한 펌핑(하락)을 중지하시겠습니까?", "중지", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    if (!result.Equals(DialogResult.OK))
                    {
                        return;
                    }
                }
                else if (direction == "기준코인")
                {
                    DialogResult result = MessageBox.Show(pair + "에 대한 기준코인 따라가기를 중지하시겠습니까?", "중지", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    if (!result.Equals(DialogResult.OK))
                    {
                        return;
                    }
                }

                RemoveRow(pair, direction);

                if (pair == selectedPair && direction == selectedDirection)
                {
                    this.IDOK.Enabled = true;
                    this.IDCANCEL.Enabled = false;
                }

                if (direction == "상승")
                {
                    Program.pumpingStatus[pair].up.running = false;
                    if (Program.pumpingStatus[pair].up.thread != null)
                    {
                        Program.pumpingStatus[pair].up.thread.Abort();
                        Program.pumpingStatus[pair].up.thread = null;
                    }
                    if (Program.pumpingStatus[pair].up.timeThread != null)
                    {
                        Program.pumpingStatus[pair].up.timeThread.Abort();
                        Program.pumpingStatus[pair].up.timeThread = null;
                    }
                }
                if (direction == "하락")
                {
                    Program.pumpingStatus[pair].down.running = false;
                    if (Program.pumpingStatus[pair].down.thread != null)
                    {
                        Program.pumpingStatus[pair].down.thread.Abort();
                        Program.pumpingStatus[pair].down.thread = null;
                    }
                    if (Program.pumpingStatus[pair].down.timeThread != null)
                    {
                        Program.pumpingStatus[pair].down.timeThread.Abort();
                        Program.pumpingStatus[pair].down.timeThread = null;
                    }
                }
                if (direction == "기준코인")
                {
                    Program.pumpingStatus[pair].baseCoin.running = false;
                    if (Program.pumpingStatus[pair].baseCoin.thread != null)
                    {
                        Program.pumpingStatus[pair].baseCoin.thread.Abort();
                        Program.pumpingStatus[pair].baseCoin.thread = null;
                    }
                    if (Program.pumpingStatus[pair].baseCoin.timeThread != null)
                    {
                        Program.pumpingStatus[pair].baseCoin.timeThread.Abort();
                        Program.pumpingStatus[pair].baseCoin.timeThread = null;
                    }
                }
            }
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 10)
            {
                foreach (KeyValuePair<string, PumpingSetting> item in Program.pumpingStatus)
                {

                    if (item.Value.up.running)
                    {

                        int hh = 0, mm = 0, ss = 0;
                        string s_hh = "", s_mm = "", s_ss = "";
                        hh = item.Value.up.applyTimeCounter / 3600000;
                        mm = (item.Value.up.applyTimeCounter - (item.Value.up.applyTimeCounter / 3600000) * 3600000) / 60000;
                        ss = (item.Value.up.applyTimeCounter % 60000) / 1000;

                        if (hh < 10)
                            s_hh = "0" + hh.ToString();
                        else
                            s_hh = hh.ToString();

                        if (mm < 10)
                            s_mm = "0" + mm.ToString();
                        else
                            s_mm = mm.ToString();

                        if (ss < 10)
                            s_ss = "0" + ss.ToString();
                        else
                            s_ss = ss.ToString();

                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            if (dataGridView1.Rows[i].Cells[1].Value.ToString() == item.Value.pair)
                            {
                                if (dataGridView1.Rows[i].Cells[2].Value.ToString() == "상승")
                                {
                                    int pros = 0;
                                    pros = item.Value.up.applyTimeCounter * 100 / (Int32.Parse(item.Value.up.applyTime) * 60000);
                                    pros = 100 - pros;
                                    dataGridView1[7, i].Value = pros.ToString() + "%";
                                    dataGridView1[4, i].Value = s_hh + ":" + s_mm + ":" + s_ss;
                                    dataGridView1[6, i].Value = item.Value.up.curPriceVal.ToString();
                                    break;
                                }
                            }
                        }
                    }
                    if (item.Value.down.running)
                    {
                        int hh = 0, mm = 0, ss = 0;
                        string s_hh = "", s_mm = "", s_ss = "";
                        hh = item.Value.down.applyTimeCounter / 3600000;
                        mm = (item.Value.down.applyTimeCounter - (item.Value.down.applyTimeCounter / 3600000) * 3600000) / 60000;
                        ss = (item.Value.down.applyTimeCounter % 60000) / 1000;

                        if (hh < 10)
                            s_hh = "0" + hh.ToString();
                        else
                            s_hh = hh.ToString();

                        if (mm < 10)
                            s_mm = "0" + mm.ToString();
                        else
                            s_mm = mm.ToString();

                        if (ss < 10)
                            s_ss = "0" + ss.ToString();
                        else
                            s_ss = ss.ToString();

                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            if (dataGridView1.Rows[i].Cells[1].Value.ToString() == item.Value.pair)
                            {
                                if (dataGridView1.Rows[i].Cells[2].Value.ToString() == "하락")
                                {
                                    int pros = 0;
                                    pros = item.Value.down.applyTimeCounter * 100 / (Int32.Parse(item.Value.down.applyTime) * 60000);
                                    pros = 100 - pros;
                                    dataGridView1[7, i].Value = pros.ToString() + "%";
                                    dataGridView1[4, i].Value = s_hh + ":" + s_mm + ":" + s_ss;
                                    dataGridView1[6, i].Value = item.Value.down.curPriceVal.ToString();
                                    break;
                                }
                            }
                        }
                    }

                    if (item.Value.baseCoin.running)
                    {
                        int hh = 0, mm = 0, ss = 0;
                        string s_hh = "", s_mm = "", s_ss = "";
                        hh = item.Value.baseCoin.applyTimeCounter / 3600000;
                        mm = (item.Value.baseCoin.applyTimeCounter - (item.Value.baseCoin.applyTimeCounter / 3600000) * 3600000) / 60000;
                        ss = (item.Value.baseCoin.applyTimeCounter % 60000) / 1000;
                        if (hh < 10)
                            s_hh = "0" + hh.ToString();
                        else
                            s_hh = hh.ToString();
                        if (mm < 10)
                            s_mm = "0" + mm.ToString();
                        else
                            s_mm = mm.ToString();
                        if (ss < 10)
                            s_ss = "0" + ss.ToString();
                        else
                            s_ss = ss.ToString();

                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            if (dataGridView1.Rows[i].Cells[1].Value.ToString() == item.Value.pair)
                            {
                                if (dataGridView1.Rows[i].Cells[2].Value.ToString() == "기준코인")
                                {
                                    int pros = 0;
                                    pros = item.Value.baseCoin.applyTimeCounter * 100 / (Int32.Parse(item.Value.baseCoin.applyTime) * 60000);
                                    pros = 100 - pros;
                                    dataGridView1[7, i].Value = pros.ToString() + "%";
                                    dataGridView1[4, i].Value = s_hh + ":" + s_mm + ":" + s_ss;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (backgroundWorking)
            {
                backgroundWorker.ReportProgress(10);
                System.Threading.Thread.Sleep(500);
            }
        }

        private void IDCANCEL_Click_1(object sender, EventArgs e)
        {
            if (selectedDirection == "상승")
            {
                DialogResult result = MessageBox.Show(selectedPair + "에 대한 펌핑(상승)을 중지하시겠습니까?", "중지", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (!result.Equals(DialogResult.OK))
                {
                    return;
                }
            }
            else if (selectedDirection == "하락")
            {
                DialogResult result = MessageBox.Show(selectedPair + "에 대한 펌핑(하락)을 중지하시겠습니까?", "중지", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (!result.Equals(DialogResult.OK))
                {
                    return;
                }
            }
            else if (selectedDirection == "기준코인")
            {
                DialogResult result = MessageBox.Show(selectedPair + "에 대한 기준코인 따라가기를 중지하시겠습니까?", "중지", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (!result.Equals(DialogResult.OK))
                {
                    return;
                }
            }

            RemoveRow(selectedPair, selectedDirection);
            this.IDOK.Enabled = true;
            this.IDCANCEL.Enabled = false;

            if (selectedDirection == "상승")
            {
                Program.pumpingStatus[selectedPair].up.running = false;
                if (Program.pumpingStatus[selectedPair].up.thread != null)
                {
                    Program.pumpingStatus[selectedPair].up.thread.Abort();
                    Program.pumpingStatus[selectedPair].up.thread = null;
                }
                if (Program.pumpingStatus[selectedPair].up.timeThread != null)
                {
                    Program.pumpingStatus[selectedPair].up.timeThread.Abort();
                    Program.pumpingStatus[selectedPair].up.timeThread = null;
                }
            }
            if (selectedDirection == "하락")
            {

                Program.pumpingStatus[selectedPair].down.running = false;
                if (Program.pumpingStatus[selectedPair].down.thread != null)
                {
                    Program.pumpingStatus[selectedPair].down.thread.Abort();
                    Program.pumpingStatus[selectedPair].down.thread = null;
                }
                if (Program.pumpingStatus[selectedPair].down.timeThread != null)
                {
                    Program.pumpingStatus[selectedPair].down.timeThread.Abort();
                    Program.pumpingStatus[selectedPair].down.timeThread = null;
                }

            }
            if (selectedDirection == "기준코인")
            {
                Program.pumpingStatus[selectedPair].baseCoin.running = false;
                if (Program.pumpingStatus[selectedPair].baseCoin.thread != null)
                {
                    Program.pumpingStatus[selectedPair].baseCoin.thread.Abort();
                    Program.pumpingStatus[selectedPair].baseCoin.thread = null;
                }
                if (Program.pumpingStatus[selectedPair].baseCoin.timeThread != null)
                {
                    Program.pumpingStatus[selectedPair].baseCoin.timeThread.Abort();
                    Program.pumpingStatus[selectedPair].baseCoin.timeThread = null;
                }
            }
        }

        private void IDOK_Click(object sender, EventArgs e)
        {
            getSpinnerVal();

            if (selectedDirection == "상승")
            {

                Program.pumpingStatus[selectedPair].up.applyTime = this.APPLY_TIME.Value.ToString();
                Program.pumpingStatus[selectedPair].up.destination = this.DESTINATION_SPIN.Value.ToString();

                Program.pumpingStatus[selectedPair].up.running = true;
                Program.pumpingStatus[selectedPair].up.timeElapsed = 0;
                Program.pumpingStatus[selectedPair].up.timePass = 0;


                Program.pumpingStatus[selectedPair].up.prevSetting(double.Parse(this.FIRST_VALUE.Text), this.CONFIG_YN.Enabled);

                if (Program.pumpingStatus[selectedPair].up.thread == null)
                {
                    Program.pumpingStatus[selectedPair].up.thread = new Thread(new ThreadStart(Program.pumpingStatus[selectedPair].up.pumping_logic_up));
                    Program.pumpingStatus[selectedPair].up.thread.Start();
                }

                if (Program.pumpingStatus[selectedPair].up.timeThread == null)
                {
                    Program.pumpingStatus[selectedPair].up.timeThread = new Thread(new ThreadStart(Program.pumpingStatus[selectedPair].up.timeCounter));
                    Program.pumpingStatus[selectedPair].up.timeThread.Start();
                }

            }
            if (selectedDirection == "하락")
            {
                Program.pumpingStatus[selectedPair].down.applyTime = this.APPLY_TIME.Value.ToString();
                Program.pumpingStatus[selectedPair].down.destination = this.DESTINATION_SPIN.Value.ToString();

                Program.pumpingStatus[selectedPair].down.running = true;
                Program.pumpingStatus[selectedPair].down.timeElapsed = 0;

                Program.pumpingStatus[selectedPair].down.running = true;
                Program.pumpingStatus[selectedPair].down.timeElapsed = 0;

                //초기설정
                Program.pumpingStatus[selectedPair].down.prevSettingDown(double.Parse(this.FIRST_VALUE.Text), this.CONFIG_YN.Enabled);

                if (Program.pumpingStatus[selectedPair].down.thread == null)
                {

                    Program.pumpingStatus[selectedPair].down.thread = new Thread(new ThreadStart(Program.pumpingStatus[selectedPair].down.pumping_logic_down));
                    Program.pumpingStatus[selectedPair].down.thread.Start();

                }

                if (Program.pumpingStatus[selectedPair].down.timeThread == null)
                {
                    Program.pumpingStatus[selectedPair].down.timeThread = new Thread(new ThreadStart(Program.pumpingStatus[selectedPair].down.timeCounter));
                    Program.pumpingStatus[selectedPair].down.timeThread.Start();
                }
            }

            if (selectedDirection == "기준코인")
            {
                /* RestClient client = new RestClient();
                 var values = client.getOrderBookFromBittrex(selectedPair);

                 if (values == null)
                 {
                     MessageBox.Show(Constants.MICROSERVER_ERROR_MSG, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                     return;
                 }

                 client = null;
                 client = new RestClient();
                 var values1 = client.getMyOrder(selectedPair);

                 if (values1 == null)
                 {
                     MessageBox.Show(Constants.MICROSERVER_ERROR_MSG, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                     return;
                 }

                 // mic 코인인경우 bittrex이용
                 //if(selectedPair == "MIC/BTC")
                 //if (selectedPair == "ETH/BTC")
                 //{
                     if (!Program.networkStatus["bittrex"].websocketConnection)
                     {

                         try
                         {
                             BITTREX_CLASS.btx.setConnection();
                         }
                         catch (System.AggregateException)
                         {
                             MessageBox.Show("인터넷에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                         }

                         Program.networkStatus["bittrex"].websocketConnection = true;

                     }

                     if (!Program.orderStatus["DASH/BTC"].connectStatus)
                     {
                         BITTREX_CLASS.connectBitTrexWebSocket(selectedPair);
                         Program.orderStatus[selectedPair].connectStatus = true;
                     }

               //  }

                 Program.pumpingStatus[selectedPair].baseCoin.applyTime = this.APPLY_TIME.Value.ToString();
                 Program.pumpingStatus[selectedPair].baseCoin.destination = this.DESTINATION_SPIN.Value.ToString();

                 for (int i = 0; i < values.result.buy.Count; i++)
                 {


                     values.result.buy[i].Quantity *= 2;
                     values.result.buy[i].Rate /= 2;
                 }
                 for (int i = 0; i < values.result.sell.Count; i++)
                 {
                     values.result.sell[i].Quantity *= 2;
                     values.result.sell[i].Rate /= 2;
                 }

                 CommonFunc.SetOrderFromBittrexApiOrderBook(values.result.buy, values.result.sell, values1.d.item, selectedPair, false);

                 if (Program.pumpingStatus[selectedPair].baseCoin.thread == null)
                 {

                     Program.pumpingStatus[selectedPair].baseCoin.thread = new Thread(new ThreadStart(Program.pumpingStatus[selectedPair].up.follow_basecoin));
                     Program.pumpingStatus[selectedPair].baseCoin.thread.Start();

                 }

                 Program.pumpingStatus[selectedPair].baseCoin.running = true;
                 Program.pumpingStatus[selectedPair].baseCoin.timeElapsed = 0; */
            }


            this.IDOK.Enabled = false;
            this.IDCANCEL.Enabled = true;

            AddDataTableRow(selectedPair, selectedDirection); 
        }
    }
}
