﻿namespace MarketMaker
{
    partial class PumpingPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PumpingPopup));
            this.panel1 = new System.Windows.Forms.Panel();
            this.IDCANCEL = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CANCEL_ALL = new System.Windows.Forms.Button();
            this.IDOK = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.API_CALLTIME = new System.Windows.Forms.NumericUpDown();
            this.CONFIG_YN = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FIRST_VALUE = new System.Windows.Forms.TextBox();
            this.BASIC_COIN = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PREMIUM_SPIN = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.PAIR_COIN = new System.Windows.Forms.ComboBox();
            this.DIRECT_PUMPING = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.MAKING_SITE = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.RATE_SPIN1 = new System.Windows.Forms.NumericUpDown();
            this.DESTINATION_SPIN = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.APPLY_TIME = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.UNIT_SPIN = new System.Windows.Forms.NumericUpDown();
            this.MIN_SPIN = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pair = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Basic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Premium = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MinOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Method = new System.Windows.Forms.DataGridViewButtonColumn();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.API_CALLTIME)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PREMIUM_SPIN)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RATE_SPIN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DESTINATION_SPIN)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.APPLY_TIME)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UNIT_SPIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MIN_SPIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.IDCANCEL);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.IDOK);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.API_CALLTIME);
            this.panel1.Controls.Add(this.CONFIG_YN);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.FIRST_VALUE);
            this.panel1.Controls.Add(this.BASIC_COIN);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.PREMIUM_SPIN);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.RATE_SPIN1);
            this.panel1.Controls.Add(this.DESTINATION_SPIN);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 482);
            this.panel1.TabIndex = 1;
            // 
            // IDCANCEL
            // 
            this.IDCANCEL.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.IDCANCEL.Font = new System.Drawing.Font("Yu Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDCANCEL.Location = new System.Drawing.Point(170, 379);
            this.IDCANCEL.Margin = new System.Windows.Forms.Padding(10, 10, 20, 15);
            this.IDCANCEL.Name = "IDCANCEL";
            this.IDCANCEL.Size = new System.Drawing.Size(108, 35);
            this.IDCANCEL.TabIndex = 40;
            this.IDCANCEL.Text = "중지/주문취소";
            this.IDCANCEL.UseVisualStyleBackColor = true;
            this.IDCANCEL.Click += new System.EventHandler(this.IDCANCEL_Click_1);
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(259, 342);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 16);
            this.label13.TabIndex = 44;
            this.label13.Text = "초";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.CANCEL_ALL);
            this.panel2.Location = new System.Drawing.Point(-1, 430);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(8);
            this.panel2.Size = new System.Drawing.Size(300, 52);
            this.panel2.TabIndex = 2;
            // 
            // CANCEL_ALL
            // 
            this.CANCEL_ALL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CANCEL_ALL.Enabled = false;
            this.CANCEL_ALL.Font = new System.Drawing.Font("Yu Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CANCEL_ALL.Location = new System.Drawing.Point(8, 8);
            this.CANCEL_ALL.Margin = new System.Windows.Forms.Padding(10);
            this.CANCEL_ALL.Name = "CANCEL_ALL";
            this.CANCEL_ALL.Size = new System.Drawing.Size(282, 34);
            this.CANCEL_ALL.TabIndex = 5;
            this.CANCEL_ALL.Text = "모든 주문 취소/중지";
            this.CANCEL_ALL.UseVisualStyleBackColor = true;
            this.CANCEL_ALL.Click += new System.EventHandler(this.CANCEL_ALL_Click);
            // 
            // IDOK
            // 
            this.IDOK.Font = new System.Drawing.Font("Yu Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDOK.Location = new System.Drawing.Point(45, 379);
            this.IDOK.Margin = new System.Windows.Forms.Padding(40, 10, 10, 15);
            this.IDOK.Name = "IDOK";
            this.IDOK.Size = new System.Drawing.Size(88, 35);
            this.IDOK.TabIndex = 39;
            this.IDOK.Text = "시작";
            this.IDOK.UseVisualStyleBackColor = true;
            this.IDOK.Click += new System.EventHandler(this.IDOK_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(65, 340);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 16);
            this.label12.TabIndex = 43;
            this.label12.Text = "delay";
            // 
            // API_CALLTIME
            // 
            this.API_CALLTIME.Location = new System.Drawing.Point(152, 340);
            this.API_CALLTIME.Margin = new System.Windows.Forms.Padding(3, 5, 25, 3);
            this.API_CALLTIME.Name = "API_CALLTIME";
            this.API_CALLTIME.Size = new System.Drawing.Size(102, 20);
            this.API_CALLTIME.TabIndex = 42;
            // 
            // CONFIG_YN
            // 
            this.CONFIG_YN.AutoSize = true;
            this.CONFIG_YN.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CONFIG_YN.Location = new System.Drawing.Point(59, 304);
            this.CONFIG_YN.Name = "CONFIG_YN";
            this.CONFIG_YN.Size = new System.Drawing.Size(114, 20);
            this.CONFIG_YN.TabIndex = 41;
            this.CONFIG_YN.Text = "초기값 설정여부";
            this.CONFIG_YN.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 306);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 40;
            this.label2.Text = "프리미엄";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 266);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 39;
            this.label1.Text = "수량비율";
            // 
            // FIRST_VALUE
            // 
            this.FIRST_VALUE.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FIRST_VALUE.Location = new System.Drawing.Point(152, 264);
            this.FIRST_VALUE.Margin = new System.Windows.Forms.Padding(3, 6, 25, 3);
            this.FIRST_VALUE.Name = "FIRST_VALUE";
            this.FIRST_VALUE.Size = new System.Drawing.Size(110, 20);
            this.FIRST_VALUE.TabIndex = 36;
            this.FIRST_VALUE.Text = "1";
            // 
            // BASIC_COIN
            // 
            this.BASIC_COIN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BASIC_COIN.FormattingEnabled = true;
            this.BASIC_COIN.Location = new System.Drawing.Point(152, 117);
            this.BASIC_COIN.Name = "BASIC_COIN";
            this.BASIC_COIN.Size = new System.Drawing.Size(116, 21);
            this.BASIC_COIN.TabIndex = 27;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(260, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 16);
            this.label3.TabIndex = 26;
            this.label3.Text = "%";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PREMIUM_SPIN
            // 
            this.PREMIUM_SPIN.Location = new System.Drawing.Point(152, 303);
            this.PREMIUM_SPIN.Margin = new System.Windows.Forms.Padding(3, 5, 25, 3);
            this.PREMIUM_SPIN.Name = "PREMIUM_SPIN";
            this.PREMIUM_SPIN.Size = new System.Drawing.Size(110, 20);
            this.PREMIUM_SPIN.TabIndex = 38;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.PAIR_COIN, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.DIRECT_PUMPING, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.MAKING_SITE, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 15);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(284, 96);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 32);
            this.label9.TabIndex = 20;
            this.label9.Text = "페어";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PAIR_COIN
            // 
            this.PAIR_COIN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PAIR_COIN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PAIR_COIN.Enabled = false;
            this.PAIR_COIN.FormattingEnabled = true;
            this.PAIR_COIN.Location = new System.Drawing.Point(148, 37);
            this.PAIR_COIN.Margin = new System.Windows.Forms.Padding(6, 5, 20, 3);
            this.PAIR_COIN.Name = "PAIR_COIN";
            this.PAIR_COIN.Size = new System.Drawing.Size(116, 21);
            this.PAIR_COIN.TabIndex = 21;
            // 
            // DIRECT_PUMPING
            // 
            this.DIRECT_PUMPING.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DIRECT_PUMPING.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DIRECT_PUMPING.Enabled = false;
            this.DIRECT_PUMPING.FormattingEnabled = true;
            this.DIRECT_PUMPING.Location = new System.Drawing.Point(148, 69);
            this.DIRECT_PUMPING.Margin = new System.Windows.Forms.Padding(6, 5, 20, 3);
            this.DIRECT_PUMPING.Name = "DIRECT_PUMPING";
            this.DIRECT_PUMPING.Size = new System.Drawing.Size(116, 21);
            this.DIRECT_PUMPING.TabIndex = 23;
            this.DIRECT_PUMPING.TextChanged += new System.EventHandler(this.DIRECT_PUMPING_TextChanged_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 32);
            this.label10.TabIndex = 19;
            this.label10.Text = "메이킹 거래소";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MAKING_SITE
            // 
            this.MAKING_SITE.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MAKING_SITE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MAKING_SITE.Enabled = false;
            this.MAKING_SITE.FormattingEnabled = true;
            this.MAKING_SITE.Location = new System.Drawing.Point(148, 5);
            this.MAKING_SITE.Margin = new System.Windows.Forms.Padding(6, 5, 20, 3);
            this.MAKING_SITE.Name = "MAKING_SITE";
            this.MAKING_SITE.Size = new System.Drawing.Size(116, 21);
            this.MAKING_SITE.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 32);
            this.label8.TabIndex = 22;
            this.label8.Text = "방향";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RATE_SPIN1
            // 
            this.RATE_SPIN1.Location = new System.Drawing.Point(152, 264);
            this.RATE_SPIN1.Margin = new System.Windows.Forms.Padding(3, 5, 25, 3);
            this.RATE_SPIN1.Name = "RATE_SPIN1";
            this.RATE_SPIN1.Size = new System.Drawing.Size(110, 20);
            this.RATE_SPIN1.TabIndex = 37;
            // 
            // DESTINATION_SPIN
            // 
            this.DESTINATION_SPIN.Location = new System.Drawing.Point(152, 118);
            this.DESTINATION_SPIN.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.DESTINATION_SPIN.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.DESTINATION_SPIN.Name = "DESTINATION_SPIN";
            this.DESTINATION_SPIN.Size = new System.Drawing.Size(102, 20);
            this.DESTINATION_SPIN.TabIndex = 25;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.UNIT_SPIN, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.MIN_SPIN, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(11, 145);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(276, 111);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 37);
            this.label4.TabIndex = 12;
            this.label4.Text = "최소 주문수량";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 37);
            this.label5.TabIndex = 13;
            this.label5.Text = "호가단위";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 37);
            this.label6.TabIndex = 14;
            this.label6.Text = "적용시간";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Controls.Add(this.APPLY_TIME, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label11, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(141, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(132, 25);
            this.tableLayoutPanel3.TabIndex = 26;
            // 
            // APPLY_TIME
            // 
            this.APPLY_TIME.Location = new System.Drawing.Point(0, 5);
            this.APPLY_TIME.Margin = new System.Windows.Forms.Padding(0, 5, 3, 3);
            this.APPLY_TIME.Name = "APPLY_TIME";
            this.APPLY_TIME.Size = new System.Drawing.Size(102, 20);
            this.APPLY_TIME.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(108, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 25);
            this.label11.TabIndex = 1;
            this.label11.Text = "분";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UNIT_SPIN
            // 
            this.UNIT_SPIN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UNIT_SPIN.Location = new System.Drawing.Point(141, 45);
            this.UNIT_SPIN.Margin = new System.Windows.Forms.Padding(3, 8, 25, 3);
            this.UNIT_SPIN.Name = "UNIT_SPIN";
            this.UNIT_SPIN.Size = new System.Drawing.Size(110, 20);
            this.UNIT_SPIN.TabIndex = 27;
            // 
            // MIN_SPIN
            // 
            this.MIN_SPIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MIN_SPIN.Location = new System.Drawing.Point(141, 82);
            this.MIN_SPIN.Margin = new System.Windows.Forms.Padding(3, 8, 25, 3);
            this.MIN_SPIN.Name = "MIN_SPIN";
            this.MIN_SPIN.Size = new System.Drawing.Size(110, 20);
            this.MIN_SPIN.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(59, 120);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 16);
            this.label7.TabIndex = 24;
            this.label7.Text = "목표";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.Pair,
            this.Basic,
            this.Rate,
            this.Premium,
            this.Unit,
            this.MinOrder,
            this.Status,
            this.Method});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dataGridView1.Location = new System.Drawing.Point(309, 16);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView1.Size = new System.Drawing.Size(637, 482);
            this.dataGridView1.TabIndex = 23;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // No
            // 
            this.No.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.No.Frozen = true;
            this.No.HeaderText = "번호";
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.No.Width = 50;
            // 
            // Pair
            // 
            this.Pair.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Pair.Frozen = true;
            this.Pair.HeaderText = "페어";
            this.Pair.Name = "Pair";
            this.Pair.ReadOnly = true;
            this.Pair.Width = 70;
            // 
            // Basic
            // 
            this.Basic.Frozen = true;
            this.Basic.HeaderText = "방향";
            this.Basic.Name = "Basic";
            this.Basic.ReadOnly = true;
            this.Basic.Width = 60;
            // 
            // Rate
            // 
            this.Rate.Frozen = true;
            this.Rate.HeaderText = "목표";
            this.Rate.Name = "Rate";
            this.Rate.ReadOnly = true;
            this.Rate.Width = 60;
            // 
            // Premium
            // 
            this.Premium.Frozen = true;
            this.Premium.HeaderText = "남은시간";
            this.Premium.Name = "Premium";
            this.Premium.ReadOnly = true;
            this.Premium.Width = 75;
            // 
            // Unit
            // 
            this.Unit.Frozen = true;
            this.Unit.HeaderText = "시작가";
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            this.Unit.Width = 76;
            // 
            // MinOrder
            // 
            this.MinOrder.Frozen = true;
            this.MinOrder.HeaderText = "현재가";
            this.MinOrder.Name = "MinOrder";
            this.MinOrder.ReadOnly = true;
            this.MinOrder.Width = 87;
            // 
            // Status
            // 
            this.Status.Frozen = true;
            this.Status.HeaderText = "펌핑";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 54;
            // 
            // Method
            // 
            this.Method.HeaderText = "조작";
            this.Method.Name = "Method";
            this.Method.ReadOnly = true;
            this.Method.Text = "중지";
            this.Method.UseColumnTextForButtonValue = true;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            // 
            // PumpingPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 503);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PumpingPopup";
            this.Text = "펌핑설정";
            this.Load += new System.EventHandler(this.PumpingPopup_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.API_CALLTIME)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PREMIUM_SPIN)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RATE_SPIN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DESTINATION_SPIN)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.APPLY_TIME)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UNIT_SPIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MIN_SPIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button CANCEL_ALL;
        private System.Windows.Forms.ComboBox MAKING_SITE;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox PAIR_COIN;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox DIRECT_PUMPING;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown DESTINATION_SPIN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox BASIC_COIN;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.NumericUpDown APPLY_TIME;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown UNIT_SPIN;
        private System.Windows.Forms.NumericUpDown MIN_SPIN;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn No;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pair;
        private System.Windows.Forms.DataGridViewTextBoxColumn Basic;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Premium;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn MinOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewButtonColumn Method;
        private System.Windows.Forms.TextBox FIRST_VALUE;
        private System.Windows.Forms.NumericUpDown RATE_SPIN1;
        private System.Windows.Forms.NumericUpDown PREMIUM_SPIN;
        private System.Windows.Forms.Button IDOK;
        private System.Windows.Forms.Button IDCANCEL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox CONFIG_YN;
        private System.Windows.Forms.NumericUpDown API_CALLTIME;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
    }
}