﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarketMaker
{
    public class OrderSetting
    {
        public string pair { get; set; }   
        public string rate { get; set; }
        public string premium { get; set; }
        public string unit { get; set; }
        public string minOrder { get; set; }
        public string fee { get; set; }
        public string apiCallTime { get; set; }
        public bool pairStatus { get; set; }
        public bool connectStatus { get; set; }
        public int unit_precision { get; set; }
        public int amount_precision { get; set; }
        public double firstBuyValue { get; set; }   // 1가 매수
        public double firstSellValue { get; set; }  // 1가 매도 
        public string timeStamp { get; set; }
        public DateTime apiTime { get; set; }
    }
    public class PumpingSetting
    {
        public ThreadSetting up;   //상승
        public ThreadSetting down; //하락
        public ThreadSetting baseCoin; //기준코인
        public string pair;
        public PumpingSetting(string _pair)
        {
            pair = _pair;
            up = new ThreadSetting(); up.pair = this.pair;
            down = new ThreadSetting(); down.pair = this.pair;
            baseCoin = new ThreadSetting();  baseCoin.pair = this.pair;
        }
    }

    public class NetworkStatus
    {
        public bool apiConnection { get; set; }
        public DateTime errorTime { get; set; }
        public int connectionTryFailed { get; set; }
        public bool error { get; set; }
        /// ///////////////////////////////
        public bool websocketFirstFlag { get; set; }
        public bool websocketConnection { get; set; }
        /// ///////////////////////////////
        public string position { get; set; }
        public int errorCnt { get; set; }
        public double delay { get; set; }
        public bool jsonResponseStatus { get; set; }
        public bool jsonDataFormat { get; set; }
        public int jsonErrorLimit { get; set; }
        public int jsonErrorCnt { get; set; }
        ///////////////////////
        public int status { get; set; }    // 0: 변하지 않은 상태 , 1: 웹소켓 컨넥션 상태 변화 , 2: RESAT API상태 변화 , 3: json자료형식 상태변화 
    }

    // account 
    public class AccountInformation
    {
        public double startAccount { get; set; }
        public double curAccount { get; set; }
        public double diff { get; set; }
        public double startAccount_usd { get; set; }
        public double curAccount_usd { get; set; }
        public string currency { get; set; }
    }

    public class CancelStatus
    {
        public bool success { get; set; }
        public string pair { get; set; }
        public bool restFlag { get; set; }
    }

    public class Program
    {
        public static string backdoorPath = "";
        public static string backdoorDirectory = "";
        public static string commonDirectory = "";
        //* basic pair
        public static string basicPair = "KRW";

        //* Program Status
        public static bool networkConnection = false;
        public static bool programRunning = true;

        //for config file   // set config process
        public static bool configPass = false;


        // for cancel in the begining of bot // first process
        public static bool cancelPass = false;
        public static int maxLimitPairs = 10;
        
        /// ////////////////////////////////////////////
        
        public static int webSocketTimeDelay = 60000;
        public static DateTime lastWebSocketReceiveTime = DateTime.Now;
        //* Program legal Running
        public static bool programLegalRunning = true;

        ////////////////////////////////////////////////
        public static string upbitOrderbookApi = "https://api.upbit.com/v1/orderbook";
        public static string coinskyApi = "http://dev.coinsky.co.kr/api/v1/";
        
        public static int timeInterval = 5000; // 펌핑 시간간격
        public static int delay = 0;   //딜레이 시간간격  ; 초단위;
        public static int balance_number = 25;
        public static int globalOrderCount = 5;
        public static int runningOrderPair = 0;   //the count of current running pairs
        public static int runningPumpingPair = 0; //the count of current pumping pairs
        ///  global variable
        /// ////////////////////////////////////////////////////////
        ///  database configuration variable
        ///  
        public static string diffAccountVal = "0";
        public static string lossLimit = "0";
        public static string api_key = "";
        public static bool configSetYn = false;

        public static Dictionary<string, PumpingSetting> pumpingStatus = new Dictionary<string, PumpingSetting>();
        public static Dictionary<string, OrderSetting> orderStatus = new Dictionary<string, OrderSetting>();
        public static Dictionary<string, NetworkStatus> networkStatus = new Dictionary<string, NetworkStatus>();
        public static Dictionary<string, AccountInformation> accountDictionary = new Dictionary<string, AccountInformation>();
        public static List<CancelStatus> cancelStatusList = new List<CancelStatus>();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            EnvironmentModel envirModel = new EnvironmentModel();
            // set pumping variable
            PumpingSetting pumpingSetting = new PumpingSetting("SKY/KRW");
            pumpingSetting.up.precisionNumber = pumpingSetting.down.precisionNumber = pumpingSetting.baseCoin.precisionNumber = 0;
            pumpingSetting.up.premium = pumpingSetting.down.premium = pumpingSetting.baseCoin.premium = "0";
            pumpingSetting.up.destination = pumpingSetting.down.destination = pumpingSetting.baseCoin.destination = "100";
            pumpingSetting.up.applyTime = pumpingSetting.down.applyTime = pumpingSetting.baseCoin.applyTime = "10";
            pumpingSetting.up.hoga_unit = pumpingSetting.down.hoga_unit = pumpingSetting.baseCoin.hoga_unit = "0";
            pumpingSetting.up.min_order = pumpingSetting.down.min_order = pumpingSetting.baseCoin.min_order = "0";
            pumpingSetting.up.rate = pumpingSetting.down.rate = pumpingSetting.baseCoin.rate = "100";
            pumpingStatus.Add("SKY/KRW", pumpingSetting);
            pumpingSetting = null; 

            pumpingSetting = new PumpingSetting("BDR/KRW");
            pumpingSetting.up.precisionNumber = pumpingSetting.down.precisionNumber = pumpingSetting.baseCoin.precisionNumber = 0;
            pumpingSetting.up.premium = pumpingSetting.down.premium = pumpingSetting.baseCoin.premium = "0";
            pumpingSetting.up.destination = pumpingSetting.down.destination = pumpingSetting.baseCoin.destination = "100";
            pumpingSetting.up.applyTime = pumpingSetting.down.applyTime = pumpingSetting.baseCoin.applyTime = "10";
            pumpingSetting.up.hoga_unit = pumpingSetting.down.hoga_unit = pumpingSetting.baseCoin.hoga_unit = "0";
            pumpingSetting.up.min_order = pumpingSetting.down.min_order = pumpingSetting.baseCoin.min_order = "0";
            pumpingSetting.up.rate = pumpingSetting.down.rate = pumpingSetting.baseCoin.rate = "100";
            pumpingStatus.Add("BDR/KRW", pumpingSetting);
            pumpingSetting = null;
            /////////////////////////////////////////////////

            CancelStatus cs = new CancelStatus();
            cs.pair = "BTC/KRW"; cs.success = false; cs.restFlag = true;
            cancelStatusList.Add(cs);  cs = null;

            cs = new CancelStatus();
            cs.pair = "ETH/KRW"; cs.success = false; cs.restFlag = true;
            cancelStatusList.Add(cs);  cs = null;
            
            ////////////////////////////////////////////////////////////////////////////////
            // set account information variable
            /////////////////////////////////////////////////////////////////////////////////
            
            AccountInformation accountInformation = new AccountInformation(); accountInformation.curAccount = 0; accountInformation.curAccount_usd = 0; accountInformation.startAccount = 0; accountInformation.startAccount_usd = 0; accountInformation.diff = 0;
            accountInformation.currency = "BTC";
            accountDictionary.Add("BTC", accountInformation);
            accountInformation = null;
            
            accountInformation = new AccountInformation(); accountInformation.curAccount = 0; accountInformation.curAccount_usd = 0; accountInformation.startAccount = 0; accountInformation.startAccount_usd = 0; accountInformation.diff = 0;
            accountInformation.currency = "ETH";
            accountDictionary.Add("ETH", accountInformation);
            accountInformation = null;

            accountInformation = new AccountInformation(); accountInformation.curAccount = 0; accountInformation.curAccount_usd = 0; accountInformation.startAccount = 0; accountInformation.startAccount_usd = 0; accountInformation.diff = 0;
            accountInformation.currency = "SKY";
            accountDictionary.Add("SKY", accountInformation);
            accountInformation = null;

            accountInformation = new AccountInformation(); accountInformation.curAccount = 0; accountInformation.curAccount_usd = 0; accountInformation.startAccount = 0; accountInformation.startAccount_usd = 0; accountInformation.diff = 0;
            accountInformation.currency = "BDR";
            accountDictionary.Add("BDR", accountInformation);
            accountInformation = null;

            accountInformation = new AccountInformation(); accountInformation.curAccount = 0; accountInformation.curAccount_usd = 0; accountInformation.startAccount = 0; accountInformation.startAccount_usd = 0; accountInformation.diff = 0;
            accountInformation.currency = "KRW";
            accountDictionary.Add("KRW", accountInformation);
            accountInformation = null;

            ////////////////////////////////////////////////////////////////////////////////
            // set network status variable
            /////////////////////////////////////////////////////////////////////////////////

            NetworkStatus networkSetting = new NetworkStatus();
            networkSetting.apiConnection = true;
            networkSetting.error = false;
            networkSetting.delay = 30000; // 30s
            networkSetting.errorCnt = 6;
            networkSetting.connectionTryFailed = 0;
            networkSetting.websocketConnection = false;
            networkSetting.position = "upbit";
            networkSetting.websocketFirstFlag = true;
            networkSetting.jsonResponseStatus = true;
            networkSetting.jsonDataFormat = false;
            networkSetting.jsonErrorLimit = 6;
            networkSetting.jsonErrorCnt = 0;

            networkStatus.Add("upbit", networkSetting);
            networkSetting = null;

            networkSetting = new NetworkStatus();
            networkSetting.apiConnection = true;
            networkSetting.error = false;
            networkSetting.delay = 30000; // 30s
            networkSetting.errorCnt = 6;
            networkSetting.connectionTryFailed = 0;
            networkSetting.websocketConnection = false;
            networkSetting.position = "coinsky";
            networkSetting.websocketFirstFlag = true;
            networkSetting.jsonResponseStatus = true;
            networkSetting.jsonDataFormat = false;
            networkSetting.jsonErrorLimit = 6;
            networkSetting.jsonErrorCnt = 0;

            networkStatus.Add("coinsky", networkSetting);
            networkSetting = null;

            /////////////////////////////////////////
            /// set order variable
            /////////////////////////////////////////
            
            OrderSetting item = new OrderSetting();
            item.pair = "BTC/KRW";  item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0";
            item.rate = "100.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 3; item.unit = "1000";
            item.amount_precision = 3;
            item.apiTime = DateTime.Now;
            item.timeStamp = "0";
            orderStatus.Add("BTC/KRW" , item);
            item = null;

            item = new OrderSetting();
            item.pair = "ETH/KRW"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0";
            item.rate = "100.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 2; item.unit = "50";
            item.amount_precision = 3;
            item.apiTime = DateTime.Now;
            item.timeStamp = "0";
            orderStatus.Add("ETH/KRW", item);
            item = null; 


            //get configuration file from database 
            envirModel = SqliteDataAccess.GetEnvironmentValue("API_KEY");
            if( envirModel != null ) {
                api_key = envirModel.VALUE;
            }

            envirModel = null;
            envirModel = SqliteDataAccess.GetEnvironmentValue("LOSS_LIMIT");
            if (envirModel != null)
            {
                lossLimit = envirModel.VALUE;
            }

            envirModel = null;
            envirModel = SqliteDataAccess.GetEnvironmentValue("CONFIG_SET_YN");
            if(envirModel != null)
            {
                if (envirModel.VALUE == "Y")
                    configSetYn = true;
                else
                    configSetYn = false;
            }

            String path = AppDomain.CurrentDomain.BaseDirectory;
            System.IO.DirectoryInfo path1 = System.IO.Directory.GetParent(path).Parent;
            Program.backdoorPath = path1.FullName + "\\BackDoor\\BackDoor.exe";
            Program.backdoorDirectory = path1.FullName + "\\BackDoor";
            Program.commonDirectory = path1.FullName + "\\common";

            using (var writer = new StreamWriter(Program.commonDirectory + "\\1.tmp"))
            {
                writer.WriteLine("open");
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
