﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarketMaker
{
    public partial class PropertyPopup : Form
    {
        private bool isChangeLossLimit = false;

        public PropertyPopup()
        {
            InitializeComponent();
        }

        private void PropertyPopup_Load(object sender, EventArgs e)
        {
            /*double temp = 0;
            isChangeLossLimit = false;

            RestClient client = new RestClient();
            var values = client.getMyAccount();

            if(values == null) {
                MessageBox.Show(Constants.CONNECT_ERROR_MICRO, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < values.d.item.Count; i++)
            {
                temp = double.Parse(values.d.item[i].amount_usable);
                switch (values.d.item[i].coin_id)
                {
                    case "BTC":
                        this.BTC_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "ETH":
                        this.ETH_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "BCH":
                        this.BCH_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "LTC":
                        this.LTC_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "ETC":
                        this.ETC_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "XRP":
                        this.XRP_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "DASH":
                        this.DASH_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "ZEC":
                        this.ZEC_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "EOS":
                        this.EOS_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "MIC":
                        this.MIC_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "ELF":
                        this.ELF_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "KEY":
                        this.KEY_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "USDT":
                        this.USDT_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "TUSD":
                        this.TUSD_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    case "BRL":
                        this.BRL_CUR_ACCOUNT.Text = temp.ToString("F2");
                        break;
                    default:
                        break;
                }
            }
            

                this.ERC_CUR_ACCOUNT.Text = "0.00";
                this.ERC_ACCOUNT.Text = "0.00";

                List<AccountModel> accountModelList = new List<AccountModel>();
                accountModelList = SqliteDataAccess.GetAccountList();
                
                //fix constant
                if(accountModelList == null) {
                    MessageBox.Show(Constants.LOCAL_ACCOUNT_ERROR, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }

                for (int i = 0; i < accountModelList.Count; i++)
                {

                    temp = double.Parse(accountModelList[i].ORIGIN_AMOUNT);

                    switch (accountModelList[i].ACCOUNT_ID)
                    {
                        case "BTC":
                            this.BTC_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "ETH":
                            this.ETH_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "BCH":
                            this.BCH_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "LTC":
                            this.LTC_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "ETC":
                            this.ETC_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "XRP":
                            this.XRP_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "DASH":
                            this.DASH_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "ZEC":
                            this.ZEC_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "EOS":
                            this.EOS_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "MIC":
                            this.MIC_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "ELF":
                            this.ELF_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "KEY":
                            this.KEY_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "USDT":
                            this.USDT_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "TUSD":
                            this.TUSD_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        case "BRL":
                            this.BRL_ACCOUNT.Text = temp.ToString("F2");
                            break;
                        default:
                            break;
                    }

                }
            this.LOSSLIMIT.Text = Program.lossLimit; */
        }

        private void button1_Click(object sender, EventArgs e)
        {

            AccountModel accountModel = new AccountModel();
            double temp = 0; bool exception = false;
            //ETH
            if(this.ETH_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.ETH_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 ETH 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if(!exception) {
                accountModel.ACCOUNT_ID = "ETH";
                accountModel.ORIGIN_AMOUNT = this.ETH_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("ETH 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //BTC
            if (this.BTC_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.BTC_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 BTC 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "BTC";
                accountModel.ORIGIN_AMOUNT = this.BTC_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("BTC 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //BCH
            if (this.BCH_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.BCH_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 BCH 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "BCH";
                accountModel.ORIGIN_AMOUNT = this.BCH_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("BCH 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //LTC
            if (this.LTC_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.LTC_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 LTC 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "LTC";
                accountModel.ORIGIN_AMOUNT = this.LTC_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("LTC 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //ETC
            if (this.ETC_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.ETC_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 ETC 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "ETC";
                accountModel.ORIGIN_AMOUNT = this.ETC_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("ETC 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //XRP
            if (this.XRP_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.XRP_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 XRP 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "XRP";
                accountModel.ORIGIN_AMOUNT = this.XRP_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("XRP 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //DASH
            if (this.DASH_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.DASH_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 DASH 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "DASH";
                accountModel.ORIGIN_AMOUNT = this.DASH_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("DASH 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //ZEC
            if (this.ZEC_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.ZEC_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 ZEC 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "ZEC";
                accountModel.ORIGIN_AMOUNT = this.ZEC_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("ZEC 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //EOS
            if (this.EOS_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.EOS_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 EOS 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "EOS";
                accountModel.ORIGIN_AMOUNT = this.EOS_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("EOS 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //MIC
            if (this.MIC_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.MIC_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 MIC 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "MIC";
                accountModel.ORIGIN_AMOUNT = this.MIC_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("MIC 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //ELF
            if (this.ELF_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.ELF_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 ELF 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "ELF";
                accountModel.ORIGIN_AMOUNT = this.ELF_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("ELF 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //KEY
            if (this.KEY_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.KEY_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 KEY 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "KEY";
                accountModel.ORIGIN_AMOUNT = this.KEY_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("KEY 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //USDT
            if (this.USDT_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.USDT_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 USDT 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "USDT";
                accountModel.ORIGIN_AMOUNT = this.USDT_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("USDT 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //TUSD
            if (this.TUSD_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.TUSD_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 TUSD 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "TUSD";
                accountModel.ORIGIN_AMOUNT = this.TUSD_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("TUSD 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //BRL
            if (this.BRL_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.BRL_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 BRL 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "BRL";
                accountModel.ORIGIN_AMOUNT = this.BRL_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("BRL 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            //LOSS_LIMIT
            if (this.LOSSLIMIT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.LOSSLIMIT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 허용 손실액을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                EnvironmentModel envirModel = new EnvironmentModel();
                envirModel = SqliteDataAccess.GetEnvironmentValue("LOSS_LIMIT");

                if (envirModel != null)
                {
                    envirModel.VARIABLE = "LOSS_LIMIT";
                    envirModel.VALUE = this.LOSSLIMIT.Text;
                    if (!SqliteDataAccess.UpdateEnvironmentValue(envirModel))
                    {
                        MessageBox.Show("허용 손실액 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    envirModel = new EnvironmentModel();
                    envirModel.VARIABLE = "LOSS_LIMIT";
                    envirModel.VALUE = this.LOSSLIMIT.Text;
                    SqliteDataAccess.SaveEnvironmentValue(envirModel);
                }

                if(Program.lossLimit != LOSSLIMIT.Text) {
                    Program.lossLimit = LOSSLIMIT.Text;
                    isChangeLossLimit = true;
                }
                    
            }

            MessageBox.Show(Constants.SAVE_SUCCESS);

            this.DialogResult = DialogResult.OK;
            this.Close();

        }
        
        //현자산을 초기자산으로 적용
        private void button7_Click(object sender, EventArgs e)
        {
            AccountModel accountModel = new AccountModel();
            double temp = 0; bool exception = false;
            //ETH
            if (this.ETH_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.ETH_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 ETH 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "ETH";
                accountModel.ORIGIN_AMOUNT = this.ETH_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("ETH 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //BTC
            if (this.BTC_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.BTC_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 BTC 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "BTC";
                accountModel.ORIGIN_AMOUNT = this.BTC_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("BTC 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //BCH
            if (this.BCH_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.BCH_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 BCH 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "BCH";
                accountModel.ORIGIN_AMOUNT = this.BCH_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("BCH 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //LTC
            if (this.LTC_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.LTC_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 LTC 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "LTC";
                accountModel.ORIGIN_AMOUNT = this.LTC_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("LTC 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //ETC
            if (this.ETC_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.ETC_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 ETC 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "ETC";
                accountModel.ORIGIN_AMOUNT = this.ETC_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("ETC 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //XRP
            if (this.XRP_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.XRP_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 XRP 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "XRP";
                accountModel.ORIGIN_AMOUNT = this.XRP_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("XRP 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //DASH
            if (this.DASH_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.DASH_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 DASH 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "DASH";
                accountModel.ORIGIN_AMOUNT = this.DASH_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("DASH 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //ZEC
            if (this.ZEC_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.ZEC_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 ZEC 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "ZEC";
                accountModel.ORIGIN_AMOUNT = this.ZEC_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("ZEC 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //EOS
            if (this.EOS_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.EOS_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 EOS 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "EOS";
                accountModel.ORIGIN_AMOUNT = this.EOS_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("EOS 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //MIC
            if (this.MIC_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.MIC_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 MIC 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "MIC";
                accountModel.ORIGIN_AMOUNT = this.MIC_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("MIC 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //ELF
            if (this.ELF_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.ELF_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 ELF 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "ELF";
                accountModel.ORIGIN_AMOUNT = this.ELF_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("ELF 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //KEY
            if (this.KEY_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.KEY_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 KEY 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "KEY";
                accountModel.ORIGIN_AMOUNT = this.KEY_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("KEY 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //USDT
            if (this.USDT_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.USDT_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 USDT 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "USDT";
                accountModel.ORIGIN_AMOUNT = this.USDT_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("USDT 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //TUSD
            if (this.TUSD_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.TUSD_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 TUSD 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "TUSD";
                accountModel.ORIGIN_AMOUNT = this.TUSD_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("TUSD 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //BRL
            if (this.BRL_CUR_ACCOUNT.Text != "")
            {
                exception = false;
                try
                {
                    temp = double.Parse(this.BRL_CUR_ACCOUNT.Text);
                }
                catch
                {
                    MessageBox.Show("수값형식이 아님으로 BRL 초기자산을 보관할수 없습니다.");
                    exception = true;
                }
            }
            if (!exception)
            {
                accountModel.ACCOUNT_ID = "BRL";
                accountModel.ORIGIN_AMOUNT = this.BRL_CUR_ACCOUNT.Text;
                if (!SqliteDataAccess.SaveAccountValue(accountModel))
                {
                    MessageBox.Show("BRL 초기자산 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            MessageBox.Show("성과적으로 저장되었습니다.");

            this.Close();
        }

        public bool getIsChangeLossLimit() {
            return isChangeLossLimit;
        }
    }
}
