﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using WebSocketSharp;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace MarketMaker
{

    public partial class MainForm : Form
    {

        public bool configRunning = true;
        public int successCancelOrderCnt = 0;
        public string selectedPair = "BTC/KRW";
        public string methodPair = "";
         
        Thread threadBTCKRW, threadETHKRW;

        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description , int ReservedValue);

        public MainForm()
        {
            InitializeComponent();
        }

        //API KEY
        private void APIKEY_Click(object sender, EventArgs e)
        {
            ApiKey apiKeyForm = new ApiKey();
            if (apiKeyForm.ShowDialog(this) == DialogResult.OK) {
            }
            apiKeyForm.Dispose(); 
        }

        //SET PROPERTY
        private void SETPROPERTY_Click(object sender, EventArgs e)
        {
            PropertyPopup propertyForm = new PropertyPopup();

            if (propertyForm.ShowDialog(this) == DialogResult.OK)
            {
                if(propertyForm.getIsChangeLossLimit()) {
                    this.label11.Text = Program.lossLimit + "$";
                }
            }

            propertyForm.Dispose();
        }

        //SET PUMPING
        private void PUMPING_Click(object sender, EventArgs e)
        {
/*          MessageBox.Show("준비중입니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return; */

            PumpingPopup pumpingForm = new PumpingPopup();
            if (pumpingForm.ShowDialog(this) == DialogResult.OK)
            {
            }
            pumpingForm.Dispose();
        }

        //SET LOG
        private void LOG_Click(object sender, EventArgs e)
        {
            /*MessageBox.Show("준비중입니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return; */
            BotLog botLogForm = new BotLog();
            if (botLogForm.ShowDialog(this) == DialogResult.OK)
            {
            }
            botLogForm.Dispose();

        }
        
        private void MainForm_Load(object sender, EventArgs e)
        {

            this.GLOBAL_LABEL.Text = Constants.OFFLINE_MSG;
            /* */
            ComboboxItem item = new ComboboxItem();
            
            item.Text = "CoinSky";
            item.Value = "CoinSky";
            this.COMB_SITE.Items.Add(item);
            this.COMB_SITE.SelectedIndex = 0;
            item = null;

            /////////////////////////////////
            item = new ComboboxItem();
            //upbit     
            item.Text = "BTC/KRW"; item.Value = "1"; this.COMB_CNY.Items.Add(item);
            item = null;

            //upbit     
            item = new ComboboxItem();
            item.Text = "ETH/KRW"; item.Value = "2"; this.COMB_CNY.Items.Add(item);
            item = null;

            this.COMB_CNY.SelectedIndex = 0;

            ////////////////////////////////////////////
            item = new ComboboxItem();
            item.Text = "Upbit"; item.Value = "1";
            this.COMB_BA_SITE.Items.Add(item);
            this.COMB_BA_SITE.SelectedIndex = 0;
            item = null;

            ////////////////////////////////////////////
            if (Program.configSetYn)
                this.initialize.Checked = true;
            else
                this.initialize.Checked = false;

            ///////////////////////////////////////////           
            RATE_SPIN.Value = 100;
            RATE_SPIN.Maximum = 100;
            RATE_SPIN.Minimum = 0;

            PREMIUM_SPIN.Value = 0;
            PREMIUM_SPIN.DecimalPlaces = 2;
            PREMIUM_SPIN.Increment = 0.01M;
            PREMIUM_SPIN.Maximum = 100;
            PREMIUM_SPIN.Minimum = 0;

            UNIT_SPIN.Value = 1000;
            UNIT_SPIN.DecimalPlaces = 3;
            UNIT_SPIN.Increment = 1000;
            UNIT_SPIN.Minimum = 0;

            API_CALL_SPIN.Value = 0M;
            API_CALL_SPIN.DecimalPlaces = 3;
            API_CALL_SPIN.Increment = 0.001M;
            API_CALL_SPIN.Minimum = 0;

            ///////////////////////////////////////////
            this.label11.Text = Program.lossLimit + "$";
            ///////////////////////////////////////////
            
            this.GLOBAL_LABEL.AutoSize = true;
            this.IDOK.Select();
            this.BTN_CANCEL.Enabled = false;

            // start background worker
            backgroundWorker.RunWorkerAsync();
            backgroundWorker1.RunWorkerAsync();

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e) {

            if (e.RowIndex == -1)
                return;
            if (e.ColumnIndex == 8) {
                if (dataGridView1.RowCount - 1 == e.RowIndex)
                    return;
                string pair = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                
                DialogResult result = MessageBox.Show(pair + "주문도 취소하시겠습니까?", "주문취소", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                if (!result.Equals(DialogResult.OK))
                {
                    RemoveRow(pair);

                    if (pair == selectedPair)
                    {
                        this.IDOK.Enabled = true;
                        this.BTN_CANCEL.Enabled = false;
                    }
                    Program.orderStatus[pair].pairStatus = false;
                }
                else
                {
                    HttpClient httpClient = new HttpClient();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    cancelOrder(pair, httpClient);
                    if (pair == selectedPair)
                    {
                        this.IDOK.Enabled = true;
                        this.BTN_CANCEL.Enabled = false;
                    }
                }
            }
        }

        private void COMB_CNY_TextChanged(object sender, EventArgs e)
        {
            selectedPair = this.COMB_CNY.GetItemText(this.COMB_CNY.SelectedItem);

            switch (selectedPair)
            {
                case "BTC/KRW":
                    UNIT_SPIN.DecimalPlaces = 3;
                    UNIT_SPIN.Increment = 1000;
                    MIN_SPIN.DecimalPlaces = 3;
                    MIN_SPIN.Increment = 0.001M;
                    break;
                case "ETH/KRW":
                    UNIT_SPIN.DecimalPlaces = 2;
                    UNIT_SPIN.Increment = 50;
                    MIN_SPIN.DecimalPlaces = 3;
                    MIN_SPIN.Increment = 0.001M;
                    break;
                default:
                    break;
            }

            this.RATE_SPIN.Value = Decimal.Parse(Program.orderStatus[selectedPair].rate);
            this.PREMIUM_SPIN.Value = Decimal.Parse(Program.orderStatus[selectedPair].premium);
            this.UNIT_SPIN.Value = Decimal.Parse(Program.orderStatus[selectedPair].unit);
            this.MIN_SPIN.Value = Decimal.Parse(Program.orderStatus[selectedPair].minOrder);
            this.FEE_SPIN.Value = Decimal.Parse(Program.orderStatus[selectedPair].fee);
            this.API_CALL_SPIN.Value = Decimal.Parse(Program.orderStatus[selectedPair].apiCallTime);

            if (Program.orderStatus[selectedPair].pairStatus) {
                this.IDOK.Enabled = false;
                this.BTN_CANCEL.Enabled = true;
            }
            else {
                this.IDOK.Enabled = true;
                this.BTN_CANCEL.Enabled = false;
            }

        }

        private void AddDataTableRow(string selectedP)
        {
            /* dataGrid View */
            Program.runningOrderPair++;
            string[] row = new string[] { Program.runningOrderPair.ToString(), selectedP, "UPBIT", Program.orderStatus[selectedP].rate, Program.orderStatus[selectedP].premium, Program.orderStatus[selectedP].unit, Program.orderStatus[selectedP].minOrder, "실행중" };
            dataGridView1.Rows.Add(row);
            CommonFunc.saveBotLog(selectedP, Program.orderStatus[selectedP].rate , Program.orderStatus[selectedP].premium , "start");
            CommonFunc.saveConfig(selectedP);
        }

        private void RemoveRow(String selectedP)
        {
            //   dataGridView1.Rows.RemoveAt(nRow);
            string pair = "";
            for (int i = 0; i < dataGridView1.RowCount; i++) {
                pair = dataGridView1.Rows[i].Cells[1].Value.ToString();
                if (pair == selectedP) {
                    dataGridView1.Rows.RemoveAt(i);
                    if (Program.runningOrderPair > 0)
                        Program.runningOrderPair--;
                    break;
                }
            }
            for (int i = 0; i < dataGridView1.RowCount-1; i++) {
                dataGridView1.Rows[i].Cells[0].Value = (i + 1).ToString();
            }

            CommonFunc.saveBotLog(selectedP , Program.orderStatus[selectedP].rate , Program.orderStatus[selectedP].premium , "stop");
            CommonFunc.saveConfig(selectedP);
        }
       
        private bool startPair(string pair , bool first = true , bool middle = false) {

            Program.orderStatus[pair].pairStatus = true;
            
            if (pair == "BTC/KRW" || pair == "ETH/KRW")
            {

                RestClient client = new RestClient();
                var values = client.getOrderBookFromUpbit(pair);
                if (values == null)
                {
                    if (!middle)
                        Program.orderStatus[pair].pairStatus = false;
                    if (first)
                    {
                        this.GLOBAL_LABEL.Text = Constants.CONNECT_ERROR_BASIC;
                        MessageBox.Show(Constants.CONNECT_ERROR_BASIC, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    return false;
                }

                client = null;
                client = new RestClient();

                var values1 = client.getMyOrder(pair);

                if (values1 == null)
                {
                    if (!middle)
                        Program.orderStatus[pair].pairStatus = false;
                    if (first)
                    {
                        this.GLOBAL_LABEL.Text = Constants.CONNECT_ERROR_COINSKY;
                        MessageBox.Show(Constants.CONNECT_ERROR_COINSKY, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    return false;
                }


                UpbitOrderBookData ret = CommonFunc.getUpbitOrderbook(values[0].orderbook_units);

                CommonFunc.SetOrderFromUpbitApiOrderBook(ret.bids, ret.asks, values1.open_orders, pair);

                if (pair == "BTC/KRW" && threadBTCKRW == null)
                {
                    threadBTCKRW = new Thread(new ThreadStart(funcBTCKRW));
                    threadBTCKRW.Start();
                }
                else if (pair == "ETH/KRW" && threadETHKRW == null)
                {
                    threadETHKRW = new Thread(new ThreadStart(funcETHKRW));
                    threadETHKRW.Start();
                }
            }

            if(first)
            {
                this.IDOK.Enabled = false;
                this.BTN_CANCEL.Enabled = true;
                AddDataTableRow(pair);
                this.GLOBAL_LABEL.Text = pair + Constants.ORDER_SUCCESS;
            }

            return true;
        }
        //시작단추 클릭시
        private void IDOK_Click(object sender, EventArgs e)
        {
            if (!Program.programLegalRunning)
            {
                MessageBox.Show(Constants.ILLEGAL_STARTING, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }   
            DialogResult result = MessageBox.Show(selectedPair + "주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (!result.Equals(DialogResult.OK))
                return;
            Program.orderStatus[selectedPair].rate = this.RATE_SPIN.Value.ToString();
            Program.orderStatus[selectedPair].premium = this.PREMIUM_SPIN.Value.ToString();
            Program.orderStatus[selectedPair].unit = this.UNIT_SPIN.Value.ToString();
            Program.orderStatus[selectedPair].minOrder = this.MIN_SPIN.Value.ToString();
            Program.orderStatus[selectedPair].fee = this.FEE_SPIN.Value.ToString();
            Program.orderStatus[selectedPair].apiCallTime = this.API_CALL_SPIN.Value.ToString();
            this.GLOBAL_LABEL.Text = Constants.ORDER_READY;
            startPair(selectedPair);
        }

        public void connectBitfinexSocket(string marketName)
        {
            using (var ws = new WebSocket("wss://api.bitfinex.com/ws/2"))
            {
              /*  ws.OnMessage += (sender, e) => _OnMessage(sender, e);
                ws.OnError += (sender, e) => bitfinexDisconnect(sender, e);
                ws.Connect();
                ws.Send("{\"event\":\"subscribe\", \"channel\":\"book\", \"pair\":\"EOSBTC\"}");
                Console.ReadKey(true); */
                // Console.Read();
            }
        }

        //bitfinex callback
        public static void _OnMessage(object sender, MessageEventArgs e)
        {

            string[] separatingChars = { ",[" };
            string[] words = e.Data.Split(separatingChars, System.StringSplitOptions.RemoveEmptyEntries);
            string price = "", amount = "", count = "";
            if (words.Length == 2) {
                string[] separatingChars1 = { "[", "]" };
                string[] part = words[1].Split(separatingChars1, System.StringSplitOptions.RemoveEmptyEntries);

                if (part[0].Length > 0) {
                    string[] result = part[0].Split(',');
                    if (result.Length == 3) {
                        price = result[0];
                        count = result[1];
                        amount = result[2];
                    }
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {

            if(e.CloseReason == CloseReason.UserClosing) {
                using (var writer = new StreamWriter(Program.commonDirectory + "\\1.tmp"))
                {
                    writer.WriteLine("close");
                }
            }

            Program.orderStatus["BTC/KRW"].pairStatus = false;
            if (!(threadBTCKRW == null)) {
                threadBTCKRW.Abort();
            }

            Program.orderStatus["ETH/KRW"].pairStatus = false;
            if (!(threadETHKRW == null)) {
                threadETHKRW.Abort();
            }

            Program.programRunning = false;
            backgroundWorker.CancelAsync();
            backgroundWorker1.CancelAsync();
        }

        private void BTN_CANCEL_Click(object sender, EventArgs e) {
            DialogResult result = MessageBox.Show(selectedPair + "주문도 취소하시겠습니까?", "주문취소", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (!result.Equals(DialogResult.OK)) {
                this.IDOK.Enabled = true;
                this.BTN_CANCEL.Enabled = false;
                Program.orderStatus[selectedPair].pairStatus = false;
                RemoveRow(selectedPair);
            }
            else
            {
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                this.IDOK.Enabled = true;
                this.BTN_CANCEL.Enabled = false;
                cancelOrder(selectedPair, httpClient);
            }
        }

        //cancel All transaction
        private void BTN_ALL_CANCEL_Click(object sender, EventArgs e) {

            DialogResult result = MessageBox.Show("전체 주문도 취소하시겠습니까?", "주문취소", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (!result.Equals(DialogResult.OK))
            {
                while (dataGridView1.RowCount > 1)
                {
                    string pair = dataGridView1.Rows[0].Cells[1].Value.ToString();
                    Program.orderStatus[pair].pairStatus = false;
                    RemoveRow(pair);
                }

                this.IDOK.Enabled = true;
                this.BTN_CANCEL.Enabled = false;
            }
            else {

                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                List<string> runningPairs = new List<string>();
                for(int i = 0; i < dataGridView1.RowCount - 1; i ++)
                {
                    runningPairs.Add(dataGridView1.Rows[i].Cells[1].Value.ToString());
                }
                for(int i = 0; i < runningPairs.Count; i ++)
                {
                    cancelOrder(runningPairs[i], httpClient);
                }

                while(runningPairs.Count > 0)
                    runningPairs.RemoveAt(0);

                this.IDOK.Enabled = true;
                this.BTN_CANCEL.Enabled = false;
            }
        }

        public async void cancelOrder(string pair , HttpClient httpClient , bool flag = true) {

            String postdata = "{\"id\" : -1 , \"type\" : \"\" ,\"qty\" : 0, \"price\" : 0}";

            HttpRequestMessage requestMessage1 = new HttpRequestMessage(HttpMethod.Post, Program.microApi + @"orders/" + CommonFunc.getPairNameInMicro2(pair));
            requestMessage1.Headers.Add("cryptolization", Program.token);
            requestMessage1.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

            try
            {

                HttpResponseMessage response1 = await httpClient.SendAsync(requestMessage1);
                string json1 = await response1.Content.ReadAsStringAsync();
                try
                {
                    var value = JsonConvert.DeserializeObject<OrderResult>(json1);
                    if (value == null)
                    {
                        Program.networkStatus["micro"].status = 4;
                        return;
                    }

                    if (value.msg != null)
                    {
                        SqliteDataAccess.RemoveOrderCountPair(pair);
                        SqliteDataAccess.RemoveOrderLogPair(pair);

                        if(flag)
                        {
                            Program.orderStatus[pair].pairStatus = false;
                            RemoveRow(pair);
                            this.GLOBAL_LABEL.Text = pair + Constants.ORDER_CANCEL;
                        }
                        else
                        {
                            for(int i = 0; i < Program.cancelStatusList.Count; i ++)
                                if(Program.cancelStatusList[i].pair == pair) {
                                    if(!Program.cancelStatusList[i].success)
                                        this.successCancelOrderCnt++;
                                    Program.cancelStatusList[i].success = true;
                                    Program.cancelStatusList[i].restFlag = true;
                                    break;
                                }
                        }    
                    }

                }
                catch (Newtonsoft.Json.JsonReaderException e)
                {
                    CommonFunc.saveException("order cancel Json reader exception", e.Message);
                    
                    Program.networkStatus["micro"].status = 4;
                }
                catch (Newtonsoft.Json.JsonSerializationException e)
                {
                    CommonFunc.saveException("order cancel Json reader exception", e.Message);
                    
                    Program.networkStatus["micro"].status = 4;
                }
            }
            catch (System.Threading.Tasks.TaskCanceledException e)
            {
            }
            catch (ArgumentNullException e)
            {
            }
            catch (InvalidOperationException e)
            {
            }
            catch (HttpRequestException e)
            {
                Program.networkStatus["micro"].status = 4;
                CommonFunc.saveException("order cancel Micro Api exception", e.Message);
            }
        }
        
        private void initialize_CheckedChanged(object sender, EventArgs e)
        {
            EnvironmentModel envirModel = new EnvironmentModel();
            envirModel = SqliteDataAccess.GetEnvironmentValue("CONFIG_SET_YN");
            if (envirModel != null)
            {
                envirModel.VARIABLE = "CONFIG_SET_YN";
                if (this.initialize.Checked)
                    envirModel.VALUE = "Y";
                else
                    envirModel.VALUE = "N";
                if (!SqliteDataAccess.UpdateEnvironmentValue(envirModel))
                {
                    MessageBox.Show("CONFIG 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                envirModel = new EnvironmentModel();
                envirModel.VARIABLE = "CONFIG_SET_YN";

                if (this.initialize.Checked)
                    envirModel.VALUE = "Y";
                else
                    envirModel.VALUE = "N";

                SqliteDataAccess.SaveEnvironmentValue(envirModel);
            }
        }
        
        private void funcBTCKRW()
        {
            try
            {
                while (true) {
                    Thread.Sleep(15000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["BTC/KRW"].pairStatus) {
                        RestClient client = new RestClient();

                        var values = client.getOrderBookFromUpbit("BTC/KRW");

                        if (values == null)
                        {
                            CommonFunc.checkNetworkStatus("upbit", true);
                            continue;
                        }

                        if (Program.networkStatus["upbit"].error)
                        {
                            CommonFunc.checkNetworkStatus("upbit", false);
                        }

                        client = null;
                        client = new RestClient();
                        var values1 = client.getMyOrder("BTC/KRW");
                        if (values1 == null)
                        {
                            CommonFunc.checkNetworkStatus("coinsky", true);
                            continue;
                        }

                        if (Program.networkStatus["coinsky"].error)
                        {
                            CommonFunc.checkNetworkStatus("coinsky", false);
                        }

                        UpbitOrderBookData ret = CommonFunc.getUpbitOrderbook(values[0].orderbook_units);

                        CommonFunc.SetOrderFromUpbitApiOrderBook(ret.bids, ret.asks, values1.open_orders, "BTC/KRW");
                    }
                }
            }
            catch
            {
            }
        }

        private void funcETHKRW()
        {
            try
            {
                while (true) {
                    Thread.Sleep(15000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["ETH/KRW"].pairStatus) {
                        RestClient client = new RestClient();
                        var values = client.getOrderBookFromUpbit("ETH/KRW");

                        if (values == null)
                        {
                            CommonFunc.checkNetworkStatus("upbit", true);
                            continue;
                        }

                        if (Program.networkStatus["upbit"].error)
                        {
                            CommonFunc.checkNetworkStatus("upbit", false);
                        }

                        client = null;
                        client = new RestClient();
                        var values1 = client.getMyOrder("ETH/KRW");

                        if (values1 == null)
                        {
                            CommonFunc.checkNetworkStatus("coinsky", true);
                            continue;
                        }
                        if (Program.networkStatus["coinsky"].error)
                        {
                            CommonFunc.checkNetworkStatus("coinsky", false);
                        }

                        UpbitOrderBookData ret = CommonFunc.getUpbitOrderbook(values[0].orderbook_units);

                        CommonFunc.SetOrderFromUpbitApiOrderBook(ret.bids, ret.asks, values1.open_orders, "ETH/KRW");
                    }
                }
            }
            catch
            {
            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int timeElapsed = 0;

            while(Program.programRunning) {

             /* if(!Program.cancelPass) {

                    backgroundWorker.ReportProgress(95);

                    HttpClient httpClient = new HttpClient();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    Program.cancelPass = true;
                    successCancelOrderCnt = 0;

                    bool err = false;

                    while(true) {
                        for(int i = 0; i < Program.cancelStatusList.Count; i ++)
                        {
                            if(!Program.cancelStatusList[i].success && Program.cancelStatusList[i].restFlag)
                            {
                                Program.cancelStatusList[i].restFlag = false;
                                cancelOrder(Program.cancelStatusList[i].pair, httpClient, false);
                            }
                        }
                        if(successCancelOrderCnt == Program.cancelStatusList.Count)
                        {
                            break;
                        }

                        if(Program.networkStatus["micro"].status == 4)
                        {
                            if(!err)
                            {
                                backgroundWorker.ReportProgress(80);
                                err = true;
                            }
                                
                        }
                        

                        Thread.Sleep(100);
                    }

                    backgroundWorker.ReportProgress(96);
                }

                Program.networkStatus["micro"].status = 0;  

                if (Program.configSetYn && !Program.configPass) {

                    bool err = false;

                    Program.configPass = true;
                    //config first
                    backgroundWorker.ReportProgress(50);
                    //action
                    List<ConfigModel> configList = new List<ConfigModel>();
                    configList = SqliteDataAccess.GetConfigList();
                    if(configList != null)
                    {
                        for(int i = 0; i < configList.Count; i ++){
                            Program.orderStatus[configList[i].PAIR].pair = configList[i].PAIR;
                            Program.orderStatus[configList[i].PAIR].rate = configList[i].RATE;
                            Program.orderStatus[configList[i].PAIR].premium = configList[i].PREMIUM;
                            Program.orderStatus[configList[i].PAIR].unit = configList[i].UNIT;
                            Program.orderStatus[configList[i].PAIR].minOrder = configList[i].MIN_ORDER;
                            Program.orderStatus[configList[i].PAIR].apiCallTime = configList[i].API_CALL_TIME;
                            Program.orderStatus[configList[i].PAIR].pairStatus = ( configList[i].PAIR_STATUS == "Y" ? true : false );
                            Program.orderStatus[configList[i].PAIR].connectStatus = false;
                            Program.orderStatus[configList[i].PAIR].unit_precision = Int32.Parse(configList[i].UNIT_PRECISION);
                            Program.orderStatus[configList[i].PAIR].amount_precision = Int32.Parse(configList[i].AMOUNT_PRECISION);
                            Program.orderStatus[configList[i].PAIR].firstBuyValue = double.Parse(configList[i].FIRST_BUY_VALUE);
                            Program.orderStatus[configList[i].PAIR].firstSellValue = double.Parse(configList[i].FIRST_SELL_VALUE);
                            Program.orderStatus[configList[i].PAIR].timeStamp = configList[i].TIME_STAMP;

                            if(Program.orderStatus[configList[i].PAIR].pairStatus){
                                if(!startPair(configList[i].PAIR , false)){

                                    if(!err)
                                    {
                                        backgroundWorker.ReportProgress(90);
                                        err = true;
                                    }

                                    configRunning = true;

                                    while(configRunning) {

                                        Thread.Sleep(20000);
                                        if( startPair(configList[i].PAIR, false) )
                                        {
                                            methodPair = configList[i].PAIR;
                                            backgroundWorker.ReportProgress(99);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    methodPair = configList[i].PAIR;
                                    backgroundWorker.ReportProgress(99);
                                }
                            }
                            if (!configRunning)
                                break;
                        }

                        backgroundWorker.ReportProgress(60);
                    }
                    else
                    {
                        backgroundWorker.ReportProgress(70);
                    }
                }  */
                
                backgroundWorker.ReportProgress(10);
                Thread.Sleep(5000);
            }
        }
        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 10)
            {
                int Desc;
                if (Program.networkConnection != InternetGetConnectedState(out Desc, 0))
                {
                    Program.networkConnection = !Program.networkConnection;
                    if (Program.networkConnection)
                    {
                        this.GLOBAL_LABEL.Text = Constants.ONLINE_MSG;
                    }
                    else
                    {
                        this.GLOBAL_LABEL.Text = Constants.OFFLINE_MSG;
                    }
                }
                else if (Program.networkStatus["upbit"].status == 1)
                {

                    Program.networkStatus["upbit"].status = 0;

                    if (Program.networkStatus["upbit"].websocketConnection)
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_WEBSOCKET_SUCCESS_MSG;
                    else
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_WEBSOCKET_ERROR_MSG;

                }
                else if (Program.networkStatus["upbit"].status == 2)
                {

                    Program.networkStatus["upbit"].status = 0;

                    if (Program.networkStatus["upbit"].apiConnection)
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_RESTAPI_SUCCESS_MSG;
                    else
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_RESTAPI_ERROR_MSG;

                }
                else if (Program.networkStatus["upbit"].status == 3)
                {
                    Program.networkStatus["upbit"].status = 0;

                    if (Program.networkStatus["upbit"].jsonResponseStatus)
                        this.GLOBAL_LABEL.Text = Constants.BASIC_JSON_FORMAT_FIX;
                    else
                        this.GLOBAL_LABEL.Text = Constants.BASIC_JSON_FORMAT;
                }
                else if (Program.networkStatus["coinsky"].status == 2)
                {

                    Program.networkStatus["coinsky"].status = 0;

                    if (Program.networkStatus["coinsky"].apiConnection)
                        this.GLOBAL_LABEL.Text = Constants.COINSKYSERVER_SUCCESS_MSG;
                    else
                        this.GLOBAL_LABEL.Text = Constants.COINSKYSERVER_ERROR_MSG;

                }
                else if (Program.networkStatus["coinsky"].status == 3)
                {
                    Program.networkStatus["coinsky"].status = 0;

                    if (Program.networkStatus["coinsky"].jsonResponseStatus)
                        this.GLOBAL_LABEL.Text = Constants.COINSKY_JSON_FORMAT_FIX;
                    else
                        this.GLOBAL_LABEL.Text = Constants.COINSKY_JSON_FORMAT;
                }
                else if (Program.networkStatus["coinsky"].status == 4)
                {
                    Program.networkStatus["coinsky"].status = 0;
                    this.GLOBAL_LABEL.Text = Constants.ORDER_CANCEL_ERROR;
                }
            }
            else if (e.ProgressPercentage == 20)
            {
                if (label13.Text != (Program.diffAccountVal + "$"))
                    label13.Text = Program.diffAccountVal + "$";
            }
            else if (e.ProgressPercentage == 30)
            {

                using (var writer = new StreamWriter(Program.commonDirectory + "\\2.tmp"))
                {
                    writer.WriteLine("false");
                }

                this.GLOBAL_LABEL.Text = Constants.ILLEGAL_RUNNING;
                MessageBox.Show(Constants.ILLEGAL_RUNNING, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (e.ProgressPercentage == 40)
            {

                using (var writer = new StreamWriter(Program.commonDirectory + "\\2.tmp"))
                {
                    writer.WriteLine("true");
                }
                this.GLOBAL_LABEL.Text = Constants.LEGAL_RUNNING;
                MessageBox.Show(Constants.LEGAL_RUNNING, "알림", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (e.ProgressPercentage == 50)
            {
                this.GLOBAL_LABEL.Text = Constants.CONFIG_READY;
                this.COMB_SITE.Enabled = false;
                this.COMB_CNY.Enabled = false;
                this.COMB_BA_SITE.Enabled = false;
                this.RATE_SPIN.Enabled = false;
                this.PREMIUM_SPIN.Enabled = false;
                this.UNIT_SPIN.Enabled = false;
                this.MIN_SPIN.Enabled = false;
                this.FEE_SPIN.Enabled = false;
                this.API_CALL_SPIN.Enabled = false;
                this.IDOK.Enabled = false;
                this.BTN_CANCEL.Enabled = false;
                this.BTN_ALL_CANCEL.Enabled = false;
            }
            else if (e.ProgressPercentage == 60 || e.ProgressPercentage == 70)
            {

                if (e.ProgressPercentage == 60)
                    this.GLOBAL_LABEL.Text = Constants.CONFIG_SUCCESS;
                else
                    this.GLOBAL_LABEL.Text = Constants.CONFIG_FAIL;

                this.COMB_SITE.Enabled = true;
                this.COMB_CNY.Enabled = true;
                this.COMB_BA_SITE.Enabled = true;
                this.RATE_SPIN.Enabled = true;
                this.PREMIUM_SPIN.Enabled = true;
                this.UNIT_SPIN.Enabled = true;
                this.MIN_SPIN.Enabled = true;
                this.FEE_SPIN.Enabled = true;
                this.API_CALL_SPIN.Enabled = true;

                if (e.ProgressPercentage == 60)
                {
                    if (Program.orderStatus["BTC/KRW"].pairStatus)
                    {
                        this.IDOK.Enabled = false;
                        this.BTN_CANCEL.Enabled = true;
                    }
                    else
                    {
                        this.IDOK.Enabled = true;
                        this.BTN_CANCEL.Enabled = false;
                    }

                    this.RATE_SPIN.Value = Decimal.Parse(Program.orderStatus["BTC/KRW"].rate);
                    this.PREMIUM_SPIN.Value = Decimal.Parse(Program.orderStatus["BTC/KRW"].premium);
                    this.UNIT_SPIN.Value = Decimal.Parse(Program.orderStatus["BTC/KRW"].unit);
                    this.MIN_SPIN.Value = Decimal.Parse(Program.orderStatus["BTC/KRW"].minOrder);
                    this.FEE_SPIN.Value = Decimal.Parse(Program.orderStatus["BTC/KRW"].fee);
                    this.API_CALL_SPIN.Value = Decimal.Parse(Program.orderStatus["BTC/KRW"].apiCallTime);
                }
                this.BTN_ALL_CANCEL.Enabled = true;
            }
            else if (e.ProgressPercentage == 80)
            {
                this.GLOBAL_LABEL.Text = Constants.CANCEL_NETWORK_ERR;
            }
            else if (e.ProgressPercentage == 90)
            {
                this.GLOBAL_LABEL.Text = Constants.CONFIG_NETWORK_ERR; 
            }
            else if (e.ProgressPercentage == 95)
            {
                this.COMB_SITE.Enabled = false;
                this.COMB_CNY.Enabled = false;
                this.COMB_BA_SITE.Enabled = false;
                this.RATE_SPIN.Enabled = false;
                this.PREMIUM_SPIN.Enabled = false;
                this.UNIT_SPIN.Enabled = false;
                this.MIN_SPIN.Enabled = false;
                this.FEE_SPIN.Enabled = false;
                this.API_CALL_SPIN.Enabled = false;
                this.IDOK.Enabled = false;
                this.BTN_CANCEL.Enabled = false;
                this.BTN_ALL_CANCEL.Enabled = false;
                this.GLOBAL_LABEL.Text = Constants.CONFIG_CANCEL_READY;
            }
            else if (e.ProgressPercentage == 96)
            {
                this.COMB_SITE.Enabled = true;
                this.COMB_CNY.Enabled = true;
                this.COMB_BA_SITE.Enabled = true;
                this.RATE_SPIN.Enabled = true;
                this.PREMIUM_SPIN.Enabled = true;
                this.UNIT_SPIN.Enabled = true;
                this.MIN_SPIN.Enabled = true;
                this.FEE_SPIN.Enabled = true;
                this.API_CALL_SPIN.Enabled = true;
                this.IDOK.Enabled = true;
                this.BTN_CANCEL.Enabled = false;
                this.BTN_ALL_CANCEL.Enabled = true;
            }
            else if(e.ProgressPercentage == 99)
            {
                AddDataTableRow(methodPair);
                this.GLOBAL_LABEL.Text = methodPair + Constants.ORDER_SUCCESS;
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int timeInterval = 0;
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            while (Program.programRunning)
            {
                if (timeInterval >= 5400000) {
                    timeInterval = 0;
                }

                Process[] pname = Process.GetProcessesByName("BackDoor");
                if (pname.Length == 0)
                {
                    backgroundWorker1.ReportProgress(50);

                    string curdate = DateTime.Now.ToString("yyyy-MM-dd") + "%";
                    SqliteDataAccess.RemoveBotLog(curdate);
                    SqliteDataAccess.RemoveExceptionLog(curdate);
                }
                else
                {
                }
                System.Threading.Thread.Sleep(10000);
                timeInterval += 10000;
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 50)
            {
                try
                {
                    System.IO.Directory.SetCurrentDirectory(Program.backdoorDirectory);
                    Process process = new Process();
                    process.StartInfo.FileName = Program.backdoorPath;
                    process.StartInfo.Arguments = "-n";
                    process.Start(); 
                }
                catch
                {
                }
            }
        }

    }
}
