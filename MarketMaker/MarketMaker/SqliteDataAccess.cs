﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using Dapper;

namespace MarketMaker
{
    public class SqliteDataAccess
    {

        //Remove Exception Log
        public static void RemoveExceptionLog(string date)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("delete from TB_EXCEPTION_LOG where date not like @DATE", new { DATE = date });
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }
            }
        }
        //Remove Bot Log
        public static void RemoveBotLog(string date)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("delete from TB_BOT_LOG where date not like @DATE", new { DATE = date });
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }
            }
        }

        //TB_EXCEPTION_LOG
        /* ------------------------------------------------ */
        public static List<ExceptionLogModel> GetExceptionLogList(string date)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    var output = cnn.Query<ExceptionLogModel>("select * from TB_EXCEPTION_LOG where date like @DATE order by NO desc", new { DATE = date });
                    return output.ToList();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }

            }
        }

        //TB_BOT_LOG
        /* ------------------------------------------------ */
        public static List<BotLogModel> GetBotLogList(string date)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    var output = cnn.Query<BotLogModel>("select * from TB_BOT_LOG where date like @DATE order by NO desc", new { DATE = date });
                    return output.ToList();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }

            }
        }

        //Save Pumping Log 
        public static void SavePumpingLog(PumpingLogModel pLog)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("insert into TB_PUMPING_LOG (DATE , PAIR , DIRECTION , APPLY_TIME , AIM , BASE_COIN , RATE , PREMIUM , ACTION) values (@DATE , @PAIR , @DIRECTION , @APPLY_TIME , @AIM , @BASE_COIN , @RATE , @PREMIUM , @ACTION)", pLog);
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }             
            }
        }
        //Save Bot Log 
        public static void SaveBotLog(BotLogModel bLog)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("insert into TB_BOT_LOG (DATE , PAIR , RATE , PREMIUM , ACTION) values (@DATE , @PAIR , @RATE , @PREMIUM , @ACTION)", bLog);
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }
            }
        }
        //Save Exception Log
        public static void SaveExceptionLog(ExceptionLogModel eLog)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("insert into TB_EXCEPTION_LOG (DATE , POSITION , MSG) values (@DATE , @POSITION , @MSG)", eLog);
                }
                catch(System.Data.SQLite.SQLiteException e)
                {
                }
            }
        }     
        
        //TB_POST_LOG
        /* ------------------------------------------------ */
        public static void SavePostLog(PostLogModel pLog)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("insert into TB_POST_LOG (POSTDATA , TYPE , ORG) values (@POSTDATA , @TYPE , @ORG)", pLog);
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }
            }
        }


        //TB_ENVIRONMENT
        /* ------------------------------------------------ */
        public static List<EnvironmentModel> GetEnvironmentList()  {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    var output = cnn.Query<EnvironmentModel>("select * from TB_ENVIRONMENT", new DynamicParameters());
                    return output.ToList();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }
                
            }
        }

        public static EnvironmentModel GetEnvironmentValue(string variable) {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    return cnn.Query<EnvironmentModel>("select * from TB_ENVIRONMENT where VARIABLE=@VARIABLE", new { VARIABLE = variable }).SingleOrDefault();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }
            }
        }

        public static void SaveEnvironmentValue(EnvironmentModel eValue) {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("insert into TB_ENVIRONMENT (VARIABLE , VALUE) values (@VARIABLE , @VALUE)", eValue);
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }
                
            }
        }

        public static bool UpdateEnvironmentValue(EnvironmentModel updateValue) {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    int rowsAffected = cnn.Execute("UPDATE TB_ENVIRONMENT set VARIABLE = @VARIABLE , VALUE=@VALUE WHERE VARIABLE = @VARIABLE", updateValue);
                    return rowsAffected > 0;
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return false;
                }
            }
        }

        //TB_COIN_ACCOUNT
        /* ------------------------------------------------ */
        public static List<AccountModel> GetAccountList()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    var output = cnn.Query<AccountModel>("select * from TB_COIN_ACCOUNT where DEL_YN = 'N'", new DynamicParameters());
                    return output.ToList();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }
                
            }
        }

        public static bool SaveAccountValue(AccountModel updateValue) {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    int rowsAffected = cnn.Execute("UPDATE TB_COIN_ACCOUNT set ORIGIN_AMOUNT = @ORIGIN_AMOUNT WHERE ACCOUNT_ID = @ACCOUNT_ID", updateValue);
                    return rowsAffected > 0;
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return false;
                }
            }
        }

        //save Config Model
        public static void SaveConfig(ConfigModel config)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("insert into TB_CONFIG (PAIR , RATE , PREMIUM , UNIT , MIN_ORDER , API_CALL_TIME , PAIR_STATUS , CONNECT_STATUS , UNIT_PRECISION , AMOUNT_PRECISION , FIRST_BUY_VALUE , FIRST_SELL_VALUE , TIME_STAMP) values (@PAIR , @RATE , @PREMIUM , @UNIT , @MIN_ORDER , @API_CALL_TIME , @PAIR_STATUS , @CONNECT_STATUS , @UNIT_PRECISION , @AMOUNT_PRECISION , @FIRST_BUY_VALUE , @FIRST_SELL_VALUE , @TIME_STAMP)", config);
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }
            }
        }

        //update Config Model 
        public static bool UpdateConfig(ConfigModel config)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    string query = "UPDATE TB_CONFIG SET RATE = @RATE"
                        + " ,PREMIUM = @PREMIUM"
                        + " ,UNIT = @UNIT"
                        + " ,MIN_ORDER = @MIN_ORDER"
                        + " ,API_CALL_TIME = @API_CALL_TIME"
                        + " ,PAIR_STATUS = @PAIR_STATUS"
                        + " ,CONNECT_STATUS = @CONNECT_STATUS"
                        + " ,UNIT_PRECISION = @UNIT_PRECISION"
                        + " ,AMOUNT_PRECISION = @AMOUNT_PRECISION"
                        + " ,FIRST_BUY_VALUE = @FIRST_BUY_VALUE"
                        + " ,FIRST_SELL_VALUE = @FIRST_SELL_VALUE"
                        + " ,TIME_STAMP = @TIME_STAMP"
                        + " WHERE PAIR = @PAIR ";

                    int rowsAffected = cnn.Execute(query, config);
                    return rowsAffected > 0;
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return false;
                }
            }
        }

        public static ConfigModel GetConfig(string pair)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    return cnn.Query<ConfigModel>("select * from TB_CONFIG where PAIR=@PAIR", new { PAIR = pair }).SingleOrDefault();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }
            }
        }

        public static List<ConfigModel> GetConfigList()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    var output = cnn.Query<ConfigModel>("select * from TB_CONFIG", new DynamicParameters());
                    return output.ToList();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }

            }
        }
        // common
        private static string LoadConnectionString(string id = "Default") {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
