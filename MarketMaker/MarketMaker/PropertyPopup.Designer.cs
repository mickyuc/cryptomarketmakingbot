﻿namespace MarketMaker
{
    partial class PropertyPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.BCH_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.EOS_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.ZEC_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.DASH_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.XRP_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.ETC_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.ETH_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.BTC_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.BCH_ACCOUNT = new System.Windows.Forms.TextBox();
            this.EOS_ACCOUNT = new System.Windows.Forms.TextBox();
            this.ZEC_ACCOUNT = new System.Windows.Forms.TextBox();
            this.DASH_ACCOUNT = new System.Windows.Forms.TextBox();
            this.XRP_ACCOUNT = new System.Windows.Forms.TextBox();
            this.ETC_ACCOUNT = new System.Windows.Forms.TextBox();
            this.ETH_ACCOUNT = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.BTC_ACCOUNT = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ERC_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.KEY_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.ELF_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.MIC_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.TUSD_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.BRL_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.USDT_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.LTC_CUR_ACCOUNT = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.ERC_ACCOUNT = new System.Windows.Forms.TextBox();
            this.KEY_ACCOUNT = new System.Windows.Forms.TextBox();
            this.ELF_ACCOUNT = new System.Windows.Forms.TextBox();
            this.MIC_ACCOUNT = new System.Windows.Forms.TextBox();
            this.TUSD_ACCOUNT = new System.Windows.Forms.TextBox();
            this.BRL_ACCOUNT = new System.Windows.Forms.TextBox();
            this.USDT_ACCOUNT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.LTC_ACCOUNT = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label21 = new System.Windows.Forms.Label();
            this.LOSSLIMIT = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.IDOK = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.1046F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.68619F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.BCH_CUR_ACCOUNT, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.EOS_CUR_ACCOUNT, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.ZEC_CUR_ACCOUNT, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.DASH_CUR_ACCOUNT, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.XRP_CUR_ACCOUNT, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.ETC_CUR_ACCOUNT, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.ETH_CUR_ACCOUNT, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.BTC_CUR_ACCOUNT, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox24, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox21, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.textBox18, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox15, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox12, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox9, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBox6, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox3, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.BCH_ACCOUNT, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.EOS_ACCOUNT, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.ZEC_ACCOUNT, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.DASH_ACCOUNT, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.XRP_ACCOUNT, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.ETC_ACCOUNT, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.ETH_ACCOUNT, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label30, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label26, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label22, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label11, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.BTC_ACCOUNT, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(36, 22);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(478, 416);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // BCH_CUR_ACCOUNT
            // 
            this.BCH_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BCH_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BCH_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BCH_CUR_ACCOUNT.Enabled = false;
            this.BCH_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BCH_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 378);
            this.BCH_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.BCH_CUR_ACCOUNT.Multiline = true;
            this.BCH_CUR_ACCOUNT.Name = "BCH_CUR_ACCOUNT";
            this.BCH_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 28);
            this.BCH_CUR_ACCOUNT.TabIndex = 104;
            this.BCH_CUR_ACCOUNT.Text = "1000";
            this.BCH_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // EOS_CUR_ACCOUNT
            // 
            this.EOS_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.EOS_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EOS_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EOS_CUR_ACCOUNT.Enabled = false;
            this.EOS_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EOS_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 332);
            this.EOS_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.EOS_CUR_ACCOUNT.Multiline = true;
            this.EOS_CUR_ACCOUNT.Name = "EOS_CUR_ACCOUNT";
            this.EOS_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.EOS_CUR_ACCOUNT.TabIndex = 103;
            this.EOS_CUR_ACCOUNT.Text = "1000";
            this.EOS_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ZEC_CUR_ACCOUNT
            // 
            this.ZEC_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ZEC_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ZEC_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ZEC_CUR_ACCOUNT.Enabled = false;
            this.ZEC_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZEC_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 286);
            this.ZEC_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.ZEC_CUR_ACCOUNT.Multiline = true;
            this.ZEC_CUR_ACCOUNT.Name = "ZEC_CUR_ACCOUNT";
            this.ZEC_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.ZEC_CUR_ACCOUNT.TabIndex = 102;
            this.ZEC_CUR_ACCOUNT.Text = "1000";
            this.ZEC_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DASH_CUR_ACCOUNT
            // 
            this.DASH_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.DASH_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DASH_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DASH_CUR_ACCOUNT.Enabled = false;
            this.DASH_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DASH_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 240);
            this.DASH_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.DASH_CUR_ACCOUNT.Multiline = true;
            this.DASH_CUR_ACCOUNT.Name = "DASH_CUR_ACCOUNT";
            this.DASH_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.DASH_CUR_ACCOUNT.TabIndex = 101;
            this.DASH_CUR_ACCOUNT.Text = "1000";
            this.DASH_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // XRP_CUR_ACCOUNT
            // 
            this.XRP_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.XRP_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XRP_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.XRP_CUR_ACCOUNT.Enabled = false;
            this.XRP_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XRP_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 194);
            this.XRP_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.XRP_CUR_ACCOUNT.Multiline = true;
            this.XRP_CUR_ACCOUNT.Name = "XRP_CUR_ACCOUNT";
            this.XRP_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.XRP_CUR_ACCOUNT.TabIndex = 100;
            this.XRP_CUR_ACCOUNT.Text = "1000";
            this.XRP_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ETC_CUR_ACCOUNT
            // 
            this.ETC_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ETC_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ETC_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ETC_CUR_ACCOUNT.Enabled = false;
            this.ETC_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ETC_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 148);
            this.ETC_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.ETC_CUR_ACCOUNT.Multiline = true;
            this.ETC_CUR_ACCOUNT.Name = "ETC_CUR_ACCOUNT";
            this.ETC_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.ETC_CUR_ACCOUNT.TabIndex = 99;
            this.ETC_CUR_ACCOUNT.Text = "1000";
            this.ETC_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ETH_CUR_ACCOUNT
            // 
            this.ETH_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ETH_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ETH_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ETH_CUR_ACCOUNT.Enabled = false;
            this.ETH_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ETH_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 102);
            this.ETH_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.ETH_CUR_ACCOUNT.Multiline = true;
            this.ETH_CUR_ACCOUNT.Name = "ETH_CUR_ACCOUNT";
            this.ETH_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.ETH_CUR_ACCOUNT.TabIndex = 98;
            this.ETH_CUR_ACCOUNT.Text = "1000";
            this.ETH_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BTC_CUR_ACCOUNT
            // 
            this.BTC_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BTC_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BTC_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTC_CUR_ACCOUNT.Enabled = false;
            this.BTC_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTC_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 56);
            this.BTC_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.BTC_CUR_ACCOUNT.Multiline = true;
            this.BTC_CUR_ACCOUNT.Name = "BTC_CUR_ACCOUNT";
            this.BTC_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.BTC_CUR_ACCOUNT.TabIndex = 96;
            this.BTC_CUR_ACCOUNT.Text = "1000";
            this.BTC_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox24.Enabled = false;
            this.textBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox24.Location = new System.Drawing.Point(249, 378);
            this.textBox24.Margin = new System.Windows.Forms.Padding(10);
            this.textBox24.Multiline = true;
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(98, 28);
            this.textBox24.TabIndex = 94;
            this.textBox24.Text = "1000";
            this.textBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox21.Enabled = false;
            this.textBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox21.Location = new System.Drawing.Point(249, 332);
            this.textBox21.Margin = new System.Windows.Forms.Padding(10);
            this.textBox21.Multiline = true;
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(98, 26);
            this.textBox21.TabIndex = 92;
            this.textBox21.Text = "1000";
            this.textBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox18.Enabled = false;
            this.textBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.Location = new System.Drawing.Point(249, 286);
            this.textBox18.Margin = new System.Windows.Forms.Padding(10);
            this.textBox18.Multiline = true;
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(98, 26);
            this.textBox18.TabIndex = 90;
            this.textBox18.Text = "1000";
            this.textBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox15.Enabled = false;
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.Location = new System.Drawing.Point(249, 240);
            this.textBox15.Margin = new System.Windows.Forms.Padding(10);
            this.textBox15.Multiline = true;
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(98, 26);
            this.textBox15.TabIndex = 88;
            this.textBox15.Text = "1000";
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox12.Enabled = false;
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.Location = new System.Drawing.Point(249, 194);
            this.textBox12.Margin = new System.Windows.Forms.Padding(10);
            this.textBox12.Multiline = true;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(98, 26);
            this.textBox12.TabIndex = 86;
            this.textBox12.Text = "1000";
            this.textBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox9.Enabled = false;
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(249, 148);
            this.textBox9.Margin = new System.Windows.Forms.Padding(10);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(98, 26);
            this.textBox9.TabIndex = 84;
            this.textBox9.Text = "1000";
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox6.Enabled = false;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(249, 102);
            this.textBox6.Margin = new System.Windows.Forms.Padding(10);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(98, 26);
            this.textBox6.TabIndex = 82;
            this.textBox6.Text = "1000";
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox3.Enabled = false;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(249, 56);
            this.textBox3.Margin = new System.Windows.Forms.Padding(10);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(98, 26);
            this.textBox3.TabIndex = 79;
            this.textBox3.Text = "1000";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BCH_ACCOUNT
            // 
            this.BCH_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BCH_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BCH_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BCH_ACCOUNT.Location = new System.Drawing.Point(129, 378);
            this.BCH_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.BCH_ACCOUNT.Multiline = true;
            this.BCH_ACCOUNT.Name = "BCH_ACCOUNT";
            this.BCH_ACCOUNT.Size = new System.Drawing.Size(100, 28);
            this.BCH_ACCOUNT.TabIndex = 77;
            this.BCH_ACCOUNT.Text = "1000";
            this.BCH_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // EOS_ACCOUNT
            // 
            this.EOS_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EOS_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EOS_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EOS_ACCOUNT.Location = new System.Drawing.Point(129, 332);
            this.EOS_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.EOS_ACCOUNT.Multiline = true;
            this.EOS_ACCOUNT.Name = "EOS_ACCOUNT";
            this.EOS_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.EOS_ACCOUNT.TabIndex = 74;
            this.EOS_ACCOUNT.Text = "1000";
            this.EOS_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ZEC_ACCOUNT
            // 
            this.ZEC_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ZEC_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ZEC_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZEC_ACCOUNT.Location = new System.Drawing.Point(129, 286);
            this.ZEC_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.ZEC_ACCOUNT.Multiline = true;
            this.ZEC_ACCOUNT.Name = "ZEC_ACCOUNT";
            this.ZEC_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.ZEC_ACCOUNT.TabIndex = 71;
            this.ZEC_ACCOUNT.Text = "1000";
            this.ZEC_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DASH_ACCOUNT
            // 
            this.DASH_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DASH_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DASH_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DASH_ACCOUNT.Location = new System.Drawing.Point(129, 240);
            this.DASH_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.DASH_ACCOUNT.Multiline = true;
            this.DASH_ACCOUNT.Name = "DASH_ACCOUNT";
            this.DASH_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.DASH_ACCOUNT.TabIndex = 68;
            this.DASH_ACCOUNT.Text = "1000";
            this.DASH_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // XRP_ACCOUNT
            // 
            this.XRP_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XRP_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.XRP_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XRP_ACCOUNT.Location = new System.Drawing.Point(129, 194);
            this.XRP_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.XRP_ACCOUNT.Multiline = true;
            this.XRP_ACCOUNT.Name = "XRP_ACCOUNT";
            this.XRP_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.XRP_ACCOUNT.TabIndex = 65;
            this.XRP_ACCOUNT.Text = "1000";
            this.XRP_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ETC_ACCOUNT
            // 
            this.ETC_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ETC_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ETC_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ETC_ACCOUNT.Location = new System.Drawing.Point(129, 148);
            this.ETC_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.ETC_ACCOUNT.Multiline = true;
            this.ETC_ACCOUNT.Name = "ETC_ACCOUNT";
            this.ETC_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.ETC_ACCOUNT.TabIndex = 62;
            this.ETC_ACCOUNT.Text = "1000";
            this.ETC_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ETH_ACCOUNT
            // 
            this.ETH_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ETH_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ETH_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ETH_ACCOUNT.Location = new System.Drawing.Point(129, 102);
            this.ETH_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.ETH_ACCOUNT.Multiline = true;
            this.ETH_ACCOUNT.Name = "ETH_ACCOUNT";
            this.ETH_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.ETH_ACCOUNT.TabIndex = 59;
            this.ETH_ACCOUNT.Text = "1000";
            this.ETH_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(367, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(10);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(101, 26);
            this.label3.TabIndex = 54;
            this.label3.Text = "현 자산";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(249, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(10);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(98, 26);
            this.label2.TabIndex = 53;
            this.label2.Text = "수수료";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(3, 368);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label30.Size = new System.Drawing.Size(113, 48);
            this.label30.TabIndex = 50;
            this.label30.Text = "BCH";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(3, 322);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label26.Size = new System.Drawing.Size(113, 46);
            this.label26.TabIndex = 46;
            this.label26.Text = "EOS";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(3, 276);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label22.Size = new System.Drawing.Size(113, 46);
            this.label22.TabIndex = 42;
            this.label22.Text = "ZEC";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 230);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label18.Size = new System.Drawing.Size(113, 46);
            this.label18.TabIndex = 38;
            this.label18.Text = "DASH";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 184);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label14.Size = new System.Drawing.Size(113, 46);
            this.label14.TabIndex = 34;
            this.label14.Text = "XRP";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 138);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label9.Size = new System.Drawing.Size(113, 46);
            this.label9.TabIndex = 30;
            this.label9.Text = "ETC";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 92);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(113, 46);
            this.label5.TabIndex = 26;
            this.label5.Text = "ETH";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 46);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label10.Size = new System.Drawing.Size(113, 46);
            this.label10.TabIndex = 21;
            this.label10.Text = "BTC";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.Control;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(129, 10);
            this.label11.Margin = new System.Windows.Forms.Padding(10);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(100, 26);
            this.label11.TabIndex = 51;
            this.label11.Text = "초기 자산";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BTC_ACCOUNT
            // 
            this.BTC_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BTC_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTC_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTC_ACCOUNT.Location = new System.Drawing.Point(129, 56);
            this.BTC_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.BTC_ACCOUNT.Multiline = true;
            this.BTC_ACCOUNT.Name = "BTC_ACCOUNT";
            this.BTC_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.BTC_ACCOUNT.TabIndex = 55;
            this.BTC_ACCOUNT.Text = "1000";
            this.BTC_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.1046F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.68619F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.ERC_CUR_ACCOUNT, 3, 8);
            this.tableLayoutPanel2.Controls.Add(this.KEY_CUR_ACCOUNT, 3, 7);
            this.tableLayoutPanel2.Controls.Add(this.ELF_CUR_ACCOUNT, 3, 6);
            this.tableLayoutPanel2.Controls.Add(this.MIC_CUR_ACCOUNT, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.TUSD_CUR_ACCOUNT, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.BRL_CUR_ACCOUNT, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.USDT_CUR_ACCOUNT, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.LTC_CUR_ACCOUNT, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.textBox33, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.textBox34, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.textBox35, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.textBox36, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.textBox37, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.textBox38, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.textBox39, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.textBox40, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.ERC_ACCOUNT, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.KEY_ACCOUNT, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.ELF_ACCOUNT, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.MIC_ACCOUNT, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.TUSD_ACCOUNT, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.BRL_ACCOUNT, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.USDT_ACCOUNT, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label1, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label15, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label16, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label17, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label19, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.LTC_ACCOUNT, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 4);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(496, 22);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 9;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(478, 416);
            this.tableLayoutPanel2.TabIndex = 105;
            // 
            // ERC_CUR_ACCOUNT
            // 
            this.ERC_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ERC_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ERC_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ERC_CUR_ACCOUNT.Enabled = false;
            this.ERC_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ERC_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 378);
            this.ERC_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.ERC_CUR_ACCOUNT.Multiline = true;
            this.ERC_CUR_ACCOUNT.Name = "ERC_CUR_ACCOUNT";
            this.ERC_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 28);
            this.ERC_CUR_ACCOUNT.TabIndex = 104;
            this.ERC_CUR_ACCOUNT.Text = "1000";
            this.ERC_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // KEY_CUR_ACCOUNT
            // 
            this.KEY_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.KEY_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KEY_CUR_ACCOUNT.Enabled = false;
            this.KEY_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KEY_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 332);
            this.KEY_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.KEY_CUR_ACCOUNT.Multiline = true;
            this.KEY_CUR_ACCOUNT.Name = "KEY_CUR_ACCOUNT";
            this.KEY_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.KEY_CUR_ACCOUNT.TabIndex = 103;
            this.KEY_CUR_ACCOUNT.Text = "1000";
            this.KEY_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ELF_CUR_ACCOUNT
            // 
            this.ELF_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ELF_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ELF_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ELF_CUR_ACCOUNT.Enabled = false;
            this.ELF_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ELF_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 286);
            this.ELF_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.ELF_CUR_ACCOUNT.Multiline = true;
            this.ELF_CUR_ACCOUNT.Name = "ELF_CUR_ACCOUNT";
            this.ELF_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.ELF_CUR_ACCOUNT.TabIndex = 102;
            this.ELF_CUR_ACCOUNT.Text = "1000";
            this.ELF_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MIC_CUR_ACCOUNT
            // 
            this.MIC_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.MIC_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MIC_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MIC_CUR_ACCOUNT.Enabled = false;
            this.MIC_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MIC_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 240);
            this.MIC_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.MIC_CUR_ACCOUNT.Multiline = true;
            this.MIC_CUR_ACCOUNT.Name = "MIC_CUR_ACCOUNT";
            this.MIC_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.MIC_CUR_ACCOUNT.TabIndex = 101;
            this.MIC_CUR_ACCOUNT.Text = "1000";
            this.MIC_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TUSD_CUR_ACCOUNT
            // 
            this.TUSD_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.TUSD_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TUSD_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TUSD_CUR_ACCOUNT.Enabled = false;
            this.TUSD_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TUSD_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 194);
            this.TUSD_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.TUSD_CUR_ACCOUNT.Multiline = true;
            this.TUSD_CUR_ACCOUNT.Name = "TUSD_CUR_ACCOUNT";
            this.TUSD_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.TUSD_CUR_ACCOUNT.TabIndex = 100;
            this.TUSD_CUR_ACCOUNT.Text = "1000";
            this.TUSD_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BRL_CUR_ACCOUNT
            // 
            this.BRL_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BRL_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BRL_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BRL_CUR_ACCOUNT.Enabled = false;
            this.BRL_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BRL_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 148);
            this.BRL_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.BRL_CUR_ACCOUNT.Multiline = true;
            this.BRL_CUR_ACCOUNT.Name = "BRL_CUR_ACCOUNT";
            this.BRL_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.BRL_CUR_ACCOUNT.TabIndex = 99;
            this.BRL_CUR_ACCOUNT.Text = "1000";
            this.BRL_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // USDT_CUR_ACCOUNT
            // 
            this.USDT_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.USDT_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.USDT_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.USDT_CUR_ACCOUNT.Enabled = false;
            this.USDT_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.USDT_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 102);
            this.USDT_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.USDT_CUR_ACCOUNT.Multiline = true;
            this.USDT_CUR_ACCOUNT.Name = "USDT_CUR_ACCOUNT";
            this.USDT_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.USDT_CUR_ACCOUNT.TabIndex = 98;
            this.USDT_CUR_ACCOUNT.Text = "1000";
            this.USDT_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // LTC_CUR_ACCOUNT
            // 
            this.LTC_CUR_ACCOUNT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.LTC_CUR_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LTC_CUR_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LTC_CUR_ACCOUNT.Enabled = false;
            this.LTC_CUR_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LTC_CUR_ACCOUNT.Location = new System.Drawing.Point(367, 56);
            this.LTC_CUR_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.LTC_CUR_ACCOUNT.Multiline = true;
            this.LTC_CUR_ACCOUNT.Name = "LTC_CUR_ACCOUNT";
            this.LTC_CUR_ACCOUNT.Size = new System.Drawing.Size(101, 26);
            this.LTC_CUR_ACCOUNT.TabIndex = 96;
            this.LTC_CUR_ACCOUNT.Text = "1000";
            this.LTC_CUR_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox33.Enabled = false;
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox33.Location = new System.Drawing.Point(249, 378);
            this.textBox33.Margin = new System.Windows.Forms.Padding(10);
            this.textBox33.Multiline = true;
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(98, 28);
            this.textBox33.TabIndex = 94;
            this.textBox33.Text = "1000";
            this.textBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox34.Enabled = false;
            this.textBox34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox34.Location = new System.Drawing.Point(249, 332);
            this.textBox34.Margin = new System.Windows.Forms.Padding(10);
            this.textBox34.Multiline = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(98, 26);
            this.textBox34.TabIndex = 92;
            this.textBox34.Text = "1000";
            this.textBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox35.Enabled = false;
            this.textBox35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox35.Location = new System.Drawing.Point(249, 286);
            this.textBox35.Margin = new System.Windows.Forms.Padding(10);
            this.textBox35.Multiline = true;
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(98, 26);
            this.textBox35.TabIndex = 90;
            this.textBox35.Text = "1000";
            this.textBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox36.Enabled = false;
            this.textBox36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox36.Location = new System.Drawing.Point(249, 240);
            this.textBox36.Margin = new System.Windows.Forms.Padding(10);
            this.textBox36.Multiline = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(98, 26);
            this.textBox36.TabIndex = 88;
            this.textBox36.Text = "1000";
            this.textBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox37
            // 
            this.textBox37.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox37.Enabled = false;
            this.textBox37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox37.Location = new System.Drawing.Point(249, 194);
            this.textBox37.Margin = new System.Windows.Forms.Padding(10);
            this.textBox37.Multiline = true;
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(98, 26);
            this.textBox37.TabIndex = 86;
            this.textBox37.Text = "1000";
            this.textBox37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox38.Enabled = false;
            this.textBox38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox38.Location = new System.Drawing.Point(249, 148);
            this.textBox38.Margin = new System.Windows.Forms.Padding(10);
            this.textBox38.Multiline = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(98, 26);
            this.textBox38.TabIndex = 84;
            this.textBox38.Text = "1000";
            this.textBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox39.Enabled = false;
            this.textBox39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox39.Location = new System.Drawing.Point(249, 102);
            this.textBox39.Margin = new System.Windows.Forms.Padding(10);
            this.textBox39.Multiline = true;
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(98, 26);
            this.textBox39.TabIndex = 82;
            this.textBox39.Text = "1000";
            this.textBox39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.textBox40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox40.Enabled = false;
            this.textBox40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox40.Location = new System.Drawing.Point(249, 56);
            this.textBox40.Margin = new System.Windows.Forms.Padding(10);
            this.textBox40.Multiline = true;
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(98, 26);
            this.textBox40.TabIndex = 79;
            this.textBox40.Text = "1000";
            this.textBox40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ERC_ACCOUNT
            // 
            this.ERC_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ERC_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ERC_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ERC_ACCOUNT.Location = new System.Drawing.Point(129, 378);
            this.ERC_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.ERC_ACCOUNT.Multiline = true;
            this.ERC_ACCOUNT.Name = "ERC_ACCOUNT";
            this.ERC_ACCOUNT.Size = new System.Drawing.Size(100, 28);
            this.ERC_ACCOUNT.TabIndex = 77;
            this.ERC_ACCOUNT.Text = "1000";
            this.ERC_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // KEY_ACCOUNT
            // 
            this.KEY_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.KEY_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KEY_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KEY_ACCOUNT.Location = new System.Drawing.Point(129, 332);
            this.KEY_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.KEY_ACCOUNT.Multiline = true;
            this.KEY_ACCOUNT.Name = "KEY_ACCOUNT";
            this.KEY_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.KEY_ACCOUNT.TabIndex = 74;
            this.KEY_ACCOUNT.Text = "1000";
            this.KEY_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ELF_ACCOUNT
            // 
            this.ELF_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ELF_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ELF_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ELF_ACCOUNT.Location = new System.Drawing.Point(129, 286);
            this.ELF_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.ELF_ACCOUNT.Multiline = true;
            this.ELF_ACCOUNT.Name = "ELF_ACCOUNT";
            this.ELF_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.ELF_ACCOUNT.TabIndex = 71;
            this.ELF_ACCOUNT.Text = "1000";
            this.ELF_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MIC_ACCOUNT
            // 
            this.MIC_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MIC_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MIC_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MIC_ACCOUNT.Location = new System.Drawing.Point(129, 240);
            this.MIC_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.MIC_ACCOUNT.Multiline = true;
            this.MIC_ACCOUNT.Name = "MIC_ACCOUNT";
            this.MIC_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.MIC_ACCOUNT.TabIndex = 68;
            this.MIC_ACCOUNT.Text = "1000";
            this.MIC_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TUSD_ACCOUNT
            // 
            this.TUSD_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TUSD_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TUSD_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TUSD_ACCOUNT.Location = new System.Drawing.Point(129, 194);
            this.TUSD_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.TUSD_ACCOUNT.Multiline = true;
            this.TUSD_ACCOUNT.Name = "TUSD_ACCOUNT";
            this.TUSD_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.TUSD_ACCOUNT.TabIndex = 65;
            this.TUSD_ACCOUNT.Text = "1000";
            this.TUSD_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BRL_ACCOUNT
            // 
            this.BRL_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BRL_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BRL_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BRL_ACCOUNT.Location = new System.Drawing.Point(129, 148);
            this.BRL_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.BRL_ACCOUNT.Multiline = true;
            this.BRL_ACCOUNT.Name = "BRL_ACCOUNT";
            this.BRL_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.BRL_ACCOUNT.TabIndex = 62;
            this.BRL_ACCOUNT.Text = "1000";
            this.BRL_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // USDT_ACCOUNT
            // 
            this.USDT_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.USDT_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.USDT_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.USDT_ACCOUNT.Location = new System.Drawing.Point(129, 102);
            this.USDT_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.USDT_ACCOUNT.Multiline = true;
            this.USDT_ACCOUNT.Name = "USDT_ACCOUNT";
            this.USDT_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.USDT_ACCOUNT.TabIndex = 59;
            this.USDT_ACCOUNT.Text = "1000";
            this.USDT_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(367, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(10);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(101, 26);
            this.label1.TabIndex = 54;
            this.label1.Text = "현 자산";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(249, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(10);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(98, 26);
            this.label4.TabIndex = 53;
            this.label4.Text = "수수료";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 368);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(113, 48);
            this.label6.TabIndex = 50;
            this.label6.Text = "ERC";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 322);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label7.Size = new System.Drawing.Size(113, 46);
            this.label7.TabIndex = 46;
            this.label7.Text = "KEY";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 276);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(113, 46);
            this.label8.TabIndex = 42;
            this.label8.Text = "ELF";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 230);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label12.Size = new System.Drawing.Size(113, 46);
            this.label12.TabIndex = 38;
            this.label12.Text = "MIC";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 138);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label15.Size = new System.Drawing.Size(113, 46);
            this.label15.TabIndex = 30;
            this.label15.Text = "BRL";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 92);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label16.Size = new System.Drawing.Size(113, 46);
            this.label16.TabIndex = 26;
            this.label16.Text = "USDT";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(3, 46);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label17.Size = new System.Drawing.Size(113, 46);
            this.label17.TabIndex = 21;
            this.label17.Text = "LTC";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.SystemColors.Control;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label19.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(129, 10);
            this.label19.Margin = new System.Windows.Forms.Padding(10);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(100, 26);
            this.label19.TabIndex = 51;
            this.label19.Text = "초기 자산";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LTC_ACCOUNT
            // 
            this.LTC_ACCOUNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LTC_ACCOUNT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LTC_ACCOUNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LTC_ACCOUNT.Location = new System.Drawing.Point(129, 56);
            this.LTC_ACCOUNT.Margin = new System.Windows.Forms.Padding(10);
            this.LTC_ACCOUNT.Multiline = true;
            this.LTC_ACCOUNT.Name = "LTC_ACCOUNT";
            this.LTC_ACCOUNT.Size = new System.Drawing.Size(100, 26);
            this.LTC_ACCOUNT.TabIndex = 55;
            this.LTC_ACCOUNT.Text = "1000";
            this.LTC_ACCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 184);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label13.Size = new System.Drawing.Size(113, 46);
            this.label13.TabIndex = 34;
            this.label13.Text = "TUSD";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.label21, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.LOSSLIMIT, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(380, 60);
            this.tableLayoutPanel3.TabIndex = 106;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(288, 0);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label21.Size = new System.Drawing.Size(89, 60);
            this.label21.TabIndex = 79;
            this.label21.Text = "$";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LOSSLIMIT
            // 
            this.LOSSLIMIT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LOSSLIMIT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LOSSLIMIT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LOSSLIMIT.Location = new System.Drawing.Point(143, 20);
            this.LOSSLIMIT.Margin = new System.Windows.Forms.Padding(10, 20, 10, 20);
            this.LOSSLIMIT.Multiline = true;
            this.LOSSLIMIT.Name = "LOSSLIMIT";
            this.LOSSLIMIT.Size = new System.Drawing.Size(132, 20);
            this.LOSSLIMIT.TabIndex = 78;
            this.LOSSLIMIT.Text = "1000";
            this.LOSSLIMIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 0);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label20.Size = new System.Drawing.Size(127, 60);
            this.label20.TabIndex = 22;
            this.label20.Text = "허용 손실액";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Location = new System.Drawing.Point(37, 464);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(404, 65);
            this.panel1.TabIndex = 107;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.button2, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.IDOK, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.button7, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(472, 467);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(490, 60);
            this.tableLayoutPanel4.TabIndex = 108;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(377, 15);
            this.button2.Margin = new System.Windows.Forms.Padding(10, 15, 10, 15);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(103, 30);
            this.button2.TabIndex = 111;
            this.button2.Text = "취 소";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // IDOK
            // 
            this.IDOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IDOK.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDOK.Location = new System.Drawing.Point(255, 15);
            this.IDOK.Margin = new System.Windows.Forms.Padding(10, 15, 10, 15);
            this.IDOK.Name = "IDOK";
            this.IDOK.Size = new System.Drawing.Size(102, 30);
            this.IDOK.TabIndex = 110;
            this.IDOK.Text = "저 장";
            this.IDOK.UseVisualStyleBackColor = true;
            this.IDOK.Click += new System.EventHandler(this.button1_Click);
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Font = new System.Drawing.Font("Yu Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(10, 15);
            this.button7.Margin = new System.Windows.Forms.Padding(10, 15, 10, 15);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(225, 30);
            this.button7.TabIndex = 109;
            this.button7.Text = "현 자산을 초기 자산으로 적용";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(458, 464);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(516, 65);
            this.panel2.TabIndex = 108;
            // 
            // PropertyPopup
            // 
            this.AcceptButton = this.IDOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(1018, 556);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "PropertyPopup";
            this.Text = "초기자산 설정";
            this.Load += new System.EventHandler(this.PropertyPopup_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox BTC_ACCOUNT;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox BCH_ACCOUNT;
        private System.Windows.Forms.TextBox EOS_ACCOUNT;
        private System.Windows.Forms.TextBox ZEC_ACCOUNT;
        private System.Windows.Forms.TextBox DASH_ACCOUNT;
        private System.Windows.Forms.TextBox XRP_ACCOUNT;
        private System.Windows.Forms.TextBox ETC_ACCOUNT;
        private System.Windows.Forms.TextBox ETH_ACCOUNT;
        private System.Windows.Forms.TextBox BTC_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox BCH_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox EOS_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox ZEC_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox DASH_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox XRP_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox ETC_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox ETH_CUR_ACCOUNT;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox ERC_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox KEY_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox ELF_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox MIC_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox TUSD_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox BRL_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox USDT_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox LTC_CUR_ACCOUNT;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox ERC_ACCOUNT;
        private System.Windows.Forms.TextBox KEY_ACCOUNT;
        private System.Windows.Forms.TextBox ELF_ACCOUNT;
        private System.Windows.Forms.TextBox MIC_ACCOUNT;
        private System.Windows.Forms.TextBox TUSD_ACCOUNT;
        private System.Windows.Forms.TextBox BRL_ACCOUNT;
        private System.Windows.Forms.TextBox USDT_ACCOUNT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox LTC_ACCOUNT;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox LOSSLIMIT;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button IDOK;
        private System.Windows.Forms.Panel panel2;
    }
}