﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace MarketMaker
{  
    //combobox Item
    public class ComboboxItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public override string ToString()
        {
            return Text;
        }
    }


    // upbit orderbook
    /*public class UpbitOrderbook
    {
        public List<UpbitOrderbookItem> item { get; set; }
    }*/

    public class UpbitOrderbook
    {
        public string market { get; set; }
        public string timestamp { get; set; }
        public double total_ask_size { get; set; }
        public double total_bid_size { get; set; }
        public List<UpbitOrderbookItem> orderbook_units { get; set; }
    }

    public class UpbitOrderbookItem
    {
        public double ask_price { get; set; }
        public double bid_price { get; set; }
        public double ask_size { get; set; }
        public double bid_size { get; set; }
    }


    public class UpbitDataItem
    {
        public double unit_price { get; set; }
        public double amount { get; set; }
    }

    public class UpbitOrderBookData
    {
        public List<UpbitDataItem> bids { get; set; }
        public List<UpbitDataItem> asks { get; set; }
    }

    // orders from Coin Sky
    public class MyOrders
    {
        public bool result { get; set; }
        public List<MyOrderItem> open_orders { get; set; }
    }

    public class MyOrderItem
    {
        public string id { get; set; }
        public string date { get; set; }
        public string type { get; set; }
        public string rate { get; set; }
        public string originalTVolume { get; set; }
        public string tVolume { get; set; }
    }

    public class CoinSkyOrderItem
    {
        public double qty { get; set; }
        public double price { get; set; }
    }

    // Orderbook from Coin Sky
    public class MyOrderbook
    {
        public bool result { get; set; }
        public MyOrderbookDetail order_book { get; set; }
    }

    public class MyOrderbookDetail
    {
        public List<MyOrderbookItem> buy { get; set; }
        public List<MyOrderbookItem> sell { get; set; }
    }

    public class MyOrderbookItem
    {
        public string rate { get; set; }
        public string volume { get; set; }
        public string mine { get; set; }
    }

    public class OrderResult
    {
        public bool result { get; set; }
    }



    public class CommonFunc
    {

        static public UpbitOrderBookData getUpbitOrderbook (List<UpbitOrderbookItem> orderbook_units) {

            UpbitOrderBookData retData = new UpbitOrderBookData();
            retData.asks = new List<UpbitDataItem>();
            retData.bids = new List<UpbitDataItem>();

            UpbitDataItem item = new UpbitDataItem();

            for (int i = 0; i < orderbook_units.Count; i ++)
            {
                item.amount = orderbook_units[i].ask_size;
                item.unit_price = orderbook_units[i].ask_price;
                retData.asks.Add(item);

                item = null;

                item.amount = orderbook_units[i].bid_size;
                item.unit_price = orderbook_units[i].bid_price;
                retData.bids.Add(item);

                item = null;

            }

            return retData;

        }

        //프리미엄 체크 ...
        /* static public List<BittrexApiResultItem> returnFilterData(List<BittrexApiResultItem> param, bool type, string pair)
         {
             // 수량 비율 , 프리미엄 , 최소주문수량
             int i = 0;
             while (true)  {
                 if (i == param.Count)
                     break;
                 double rate = 0;
                 if (type) {
                     if (double.Parse(Program.orderStatus[pair].rate) == 0)
                         rate = 1;
                     else
                         rate = double.Parse(Program.orderStatus[pair].rate) / 100;
                 }
                 else {
                     if (double.Parse(Program.pumpingStatus[pair].baseCoin.rate) == 0)
                         rate = 1;
                     else
                         rate = double.Parse(Program.pumpingStatus[pair].baseCoin.rate);
                 }


                 if (type && param[i].Quantity * rate / Program.balance_number >= double.Parse(Program.orderStatus[pair].minOrder))
                 {
                     param[i].Quantity = param[i].Quantity * rate / Program.balance_number;
                     param[i].Rate = (1 + double.Parse(Program.orderStatus[pair].premium) / 100) * param[i].Rate;
                     i++;
                 }
                 else if (!type && param[i].Quantity * 2 * rate / Program.balance_number >= double.Parse(Program.pumpingStatus[pair].baseCoin.min_order))
                 {
                     param[i].Quantity = param[i].Quantity * 2 * rate / Program.balance_number;
                     param[i].Rate = (1 + double.Parse(Program.pumpingStatus[pair].baseCoin.premium) / 100) * ( param[i].Rate / 2 );
                     i++;
                 }
                 else
                 {
                     param.RemoveAt(i);
                 }
             }

             return param;
         } */

        static public string getPairNameInUpbit(string marketName) {
            string ret = "";
            switch (marketName)
            {
                case "BTC/KRW":
                    ret = "KRW-BTC";
                    break;

                case "ETH/KRW":
                    ret = "KRW-ETH";
                    break;

                default:
                    break;
            }
            return ret;
        }

        static public string getPairNameInCoinSky(string marketName) {
            string ret = "";
            switch (marketName)
            {
                case "BTC/KRW":
                    ret = "BTC-KRW";
                    break;

                case "ETH/KRW":
                    ret = "ETH-KRW";
                    break;

                default:
                    break;
            }
            return ret;
        }
        
        static public async Task<int> SetOrderFromUpbitApiOrderBook(List<UpbitDataItem> buys, List<UpbitDataItem> sells, List<MyOrderItem> item, string pair)
        {

            if (!Program.programLegalRunning)
                return 0;

            //매도10 , 매수10
            if (buys.Count < 1)
                return 0;

            double BuyValue1 = buys[0].unit_price;
            double SellValue1 = sells[0].unit_price;

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string postdata = "";

            if (double.Parse(Program.orderStatus[pair].unit) == 0 || (double.Parse(Program.orderStatus[pair].unit) > (double)Math.Pow(10, Program.orderStatus[pair].unit_precision - 1)  &&  double.Parse(Program.orderStatus[pair].unit) <= (double) Math.Pow(10, Program.orderStatus[pair].unit_precision ) )) {
                List <CoinSkyOrderItem> buyList = new List<CoinSkyOrderItem>();
                List <CoinSkyOrderItem> sellList = new List<CoinSkyOrderItem>();
                
                int stack_length = 0;
                for (int i = 0; i < buys.Count; i++) {

                    if (buyList.Count == 0) {
                        CoinSkyOrderItem oItem = new CoinSkyOrderItem();
                        oItem.price = Math.Round(buys[i].unit_price, Program.orderStatus[pair].unit_precision);
                        oItem.qty = buys[i].amount / 2;
                        buyList.Add(oItem);
                    }
                    else {

                        if (buyList[stack_length].price == Math.Round(buys[i].unit_price, Program.orderStatus[pair].unit_precision)) {
                            buyList[stack_length].qty += (buys[i].amount / 2);
                        }
                        else {

                            CoinSkyOrderItem oItem = new CoinSkyOrderItem();
                            oItem.price = Math.Round(buys[i].unit_price, Program.orderStatus[pair].unit_precision);
                            oItem.qty = buys[i].amount / 2;
                            buyList.Add(oItem);
                            stack_length++;

                        }
                    }
                    if (stack_length > 8)
                        break;
                }
                stack_length = 0;
                for (int i = 0; i < sells.Count; i++) {
                    if (sellList.Count == 0) {
                        CoinSkyOrderItem oItem = new CoinSkyOrderItem();
                        oItem.price = Math.Round(sells[i].unit_price, Program.orderStatus[pair].unit_precision);
                        oItem.qty = sells[i].amount / 2;
                        sellList.Add(oItem);
                    }
                    else {
                        if (sellList[stack_length].price == Math.Round(sells[i].unit_price, Program.orderStatus[pair].unit_precision)) {
                            sellList[stack_length].qty += (sells[i].amount / 2);
                        }
                        else {
                            CoinSkyOrderItem oItem = new CoinSkyOrderItem();
                            oItem.price = Math.Round(sells[i].unit_price, Program.orderStatus[pair].unit_precision);
                            oItem.qty = sells[i].amount / 2;
                            sellList.Add(oItem);
                            stack_length++;
                        }
                    }
                    if (stack_length > 8)
                        break;
                }
                for (int i = 0; i < buyList.Count; i++) {
                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"buy\" ,\"volume\" : " + buyList[i].qty + ", \"rate\" : " + buyList[i].price + " , \"price\" : " + ( buyList[i].price * buyList[i].qty ) + "}";
                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + getPairNameInCoinSky(pair));
                    requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");
                    Thread.Sleep(900);
                    if (Program.orderStatus[pair].pairStatus) {
                        try
                        {
                            HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                            if(Program.networkStatus["coinsky"].error){
                                checkNetworkStatus("coinsky", false);
                            }
                            string json = await response.Content.ReadAsStringAsync();
                        }
                        catch (System.Threading.Tasks.TaskCanceledException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch (ArgumentNullException e)
                        {
                            checkNetworkStatus("coinsky" , true);
                        }
                        catch(InvalidOperationException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch(HttpRequestException e)
                        {
                            CommonFunc.saveException("coinsky Server Api Exception", e.Message);
                            checkNetworkStatus("coinsky", true);
                        }
                    }
                }
                for (int i = 0; i < sellList.Count; i++) {
                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"sell\" ,\"volume\" : " + sellList[i].qty + ", \"rate\" : " + sellList[i].price + " , \"price\" : " + (sellList[i].price * sellList[i].qty) + "}";
                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + getPairNameInCoinSky(pair));
                    requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");
                    Thread.Sleep(900);
                    if (Program.orderStatus[pair].pairStatus) {
                        try
                        {
                            HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                            if (Program.networkStatus["coinsky"].error)
                            {
                                checkNetworkStatus("coinsky", false);
                            }
                            string json = await response.Content.ReadAsStringAsync();
                        }
                        catch (System.Threading.Tasks.TaskCanceledException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch (ArgumentNullException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch (InvalidOperationException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch (HttpRequestException e)
                        {
                            CommonFunc.saveException("coinsky Server Api Exception", e.Message);
                            checkNetworkStatus("coinsky", true);
                        }
                    }
                }
            }
            else
            {
                Program.orderStatus[pair].firstBuyValue = BuyValue1;
                Program.orderStatus[pair].firstSellValue = SellValue1;
                List<CoinSkyOrderItem> buyList = new List<CoinSkyOrderItem>();
                List<CoinSkyOrderItem> sellList = new List<CoinSkyOrderItem>();

                CoinSkyOrderItem oItem = new CoinSkyOrderItem();

                for (int k = 0; k < 10; k++) {

                    oItem = new CoinSkyOrderItem();
                    oItem.qty = 0;
                    oItem.price = Math.Round(BuyValue1 - k * double.Parse(Program.orderStatus[pair].unit), Program.orderStatus[pair].unit_precision);
                    buyList.Add(oItem);
                    oItem = null;

                    oItem = new CoinSkyOrderItem();
                    oItem.qty = 0;
                    oItem.price = Math.Round(SellValue1 + k * double.Parse(Program.orderStatus[pair].unit), Program.orderStatus[pair].unit_precision);
                    sellList.Add(oItem);
                    oItem = null;
                }

                long Buy_firstValue = (long)(BuyValue1 * Math.Pow(10, Program.orderStatus[pair].unit_precision));
                long Sell_firstValue = (long)(SellValue1 * Math.Pow(10, Program.orderStatus[pair].unit_precision));

                long hoga_unit = (long)(double.Parse(Program.orderStatus[pair].unit) * Math.Pow(10, Program.orderStatus[pair].unit_precision));
                long step_value = 0;
                double order_value = 0; int loop = 0;

                for (int k = 0; k < buys.Count; k++) {

                    long buy_val = (long)(Math.Round(buys[k].unit_price, Program.orderStatus[pair].unit_precision) * Math.Pow(10, Program.orderStatus[pair].unit_precision));
                    step_value = Buy_firstValue - (Buy_firstValue - buy_val) / hoga_unit * hoga_unit;
                    order_value = step_value / Math.Pow(10, Program.orderStatus[pair].unit_precision);

                    if (order_value >= buyList[loop].price && loop == 0)
                        buyList[loop].qty += (buys[k].amount / 2);
                    else if (order_value < buyList[loop].price && order_value >= buyList[loop + 1].price && loop <= 8)
                    {
                        if (buyList[loop].price - order_value < order_value - buyList[loop + 1].price)
                            buyList[loop].qty += (buys[k].amount / 2);
                        else
                            buyList[loop + 1].qty += (buys[k].amount / 2);
                    }
                    else if (loop == 9 && order_value < buyList[loop].price)
                        break;

                }

                loop = 0;

                for (int k = 0; k < sells.Count; k++) {

                    long sell_val = (long)(Math.Round(sells[k].unit_price, Program.orderStatus[pair].unit_precision) * Math.Pow(10, Program.orderStatus[pair].unit_precision));
                    step_value = Sell_firstValue + (sell_val - Sell_firstValue) / hoga_unit * hoga_unit;
                    order_value = step_value / Math.Pow(10, Program.orderStatus[pair].unit_precision);

                    if (order_value <= sellList[loop].price && loop == 0)
                        sellList[loop].qty += (sells[k].amount / 2);
                    else if (order_value <= sellList[loop + 1].price && order_value > sellList[loop].price && loop <= 8)
                    {
                        if (sellList[loop + 1].price - order_value < order_value - sellList[loop].price)
                            sellList[loop + 1].qty += (sells[k].amount / 2);
                        else
                            sellList[loop].qty += (sells[k].amount / 2);
                    }
                    else if (loop == 9 && order_value > sellList[loop].price)
                        break;

                }
                //  매도  걸어주기   
                for (int k = 0; k < sellList.Count; k++)
                {
                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"sell\" ,\"volume\" : " + sellList[k].qty + ", \"rate\" : " + sellList[k].price + " , \"price\" : " + (sellList[k].price * sellList[k].qty) + "}";
                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + getPairNameInCoinSky(pair));
                    requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");
                    Thread.Sleep(900);

                    if (Program.orderStatus[pair].pairStatus)
                    {
                        try
                        {
                            HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                            if (Program.networkStatus["coinsky"].error)
                            {
                                checkNetworkStatus("coinsky", false);
                            }
                            string json = await response.Content.ReadAsStringAsync();
                        }
                        catch (System.Threading.Tasks.TaskCanceledException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch (ArgumentNullException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch (InvalidOperationException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch (HttpRequestException e)
                        {
                            CommonFunc.saveException("coinsky Server Api Exception", e.Message);
                            checkNetworkStatus("coinsky", true);
                        }
                    }
                }

                // 매수 걸어주기 
                for (int k = 0; k < buyList.Count; k++)
                {
                    postdata = "{\"api_key\" : \"" + Program.api_key + "\" , \"type\" : \"buy\" ,\"volume\" : " + buyList[k].qty + ", \"rate\" : " + buyList[k].price + " , \"price\" : " + (buyList[k].price * buyList[k].qty) + "}";
                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"create_order?market=" + getPairNameInCoinSky(pair));
                    requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");
                    Thread.Sleep(900);
                    if (Program.orderStatus[pair].pairStatus)
                    {
                        try
                        {
                            HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                            if (Program.networkStatus["coinsky"].error)
                            {
                                checkNetworkStatus("coinsky", false);
                            }
                            string json = await response.Content.ReadAsStringAsync();
                        }
                        catch (System.Threading.Tasks.TaskCanceledException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch (ArgumentNullException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch (InvalidOperationException e)
                        {
                            checkNetworkStatus("coinsky", true);
                        }
                        catch (HttpRequestException e)
                        {
                            CommonFunc.saveException("coinsky API exception", e.Message);
                            checkNetworkStatus("coinsky", true);
                        }
                    }
                }
            }
            for (int i = 0; i < item.Count; i++) {

                postdata = "{\"id\" : " + item[i].id + " , \"api_key\" : \"" + Program.api_key + "\"}";
                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.coinskyApi + @"cancel_order?market=" + getPairNameInCoinSky(pair) );
                requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");
                if (Program.orderStatus[pair].pairStatus) {
                    try
                    {
                        HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                        if (Program.networkStatus["coinsky"].error)
                        {
                            checkNetworkStatus("coinsky", false);
                        }
                        string json = await response.Content.ReadAsStringAsync();
                    }
                    catch (System.Threading.Tasks.TaskCanceledException e)
                    {
                        checkNetworkStatus("coinsky", true);
                    }
                    catch (ArgumentNullException e)
                    {
                        checkNetworkStatus("coinsky", true);
                    }
                    catch (InvalidOperationException e)
                    {
                        checkNetworkStatus("coinsky", true);
                    }
                    catch (HttpRequestException e)
                    {
                        CommonFunc.saveException("coinsky Server Api Exception", e.Message);
                        checkNetworkStatus("coinsky", true);
                    }
                }
            }
            return 1;
        }

        /* check network status */
        /*
         * position - bittrex , micro , bitcointrader
         * error_trigger - true : 오류발생 , false: 정상 
         */
        static public void checkNetworkStatus (string position , bool error_trigger) {

            if (!Program.networkConnection)
                return;

            if(!error_trigger && Program.networkStatus[position].error) {
                Program.networkStatus[position].error = false;
                if(!Program.networkStatus[position].apiConnection)
                    Program.networkStatus[position].status = 2;
                Program.networkStatus[position].apiConnection = true;
            }
            else if( error_trigger && !Program.networkStatus[position].error) {
                Program.networkStatus[position].error = true;
                Program.networkStatus[position].errorTime = DateTime.Now;
                Program.networkStatus[position].connectionTryFailed = 1;
            }
            else if( error_trigger && Program.networkStatus[position].error) {

                DateTime nTime = DateTime.Now;
                TimeSpan span = nTime - Program.networkStatus[position].errorTime;
                double t = span.TotalMilliseconds;
                Program.networkStatus[position].connectionTryFailed ++;

                if(t > Program.networkStatus[position].delay || Program.networkStatus[position].connectionTryFailed > Program.networkStatus[position].errorCnt) {
                    Program.networkStatus[position].status = 2;
                    Program.networkStatus[position].apiConnection = false;
                }
            }
        }


        /* check json data format status */
        /*
         * position - bittrex , micro , bitcointrader
         * error_trigger - true : 오류발생 , false: 정상 
         */
        static public void checkResponseJsonDataFormat(string position, bool error_trigger) {
            if (!Program.networkConnection)
                return;
            if (!error_trigger && Program.networkStatus[position].jsonDataFormat) {
                Program.networkStatus[position].jsonDataFormat = false;

                if(!Program.networkStatus[position].jsonResponseStatus)
                    Program.networkStatus[position].status = 3;

                Program.networkStatus[position].jsonResponseStatus = true;
            }
            else if (error_trigger && !Program.networkStatus[position].jsonDataFormat) {
                Program.networkStatus[position].jsonDataFormat = true;
                Program.networkStatus[position].jsonErrorCnt = 1;
            }
            else if (error_trigger && Program.networkStatus[position].jsonDataFormat) {
                Program.networkStatus[position].jsonErrorCnt++;
                if (Program.networkStatus[position].jsonErrorCnt > Program.networkStatus[position].jsonErrorLimit)
                {
                    Program.networkStatus[position].status = 3;
                    Program.networkStatus[position].jsonResponseStatus = false;
                }
            }
        }

        /* save exception */
        static public void saveException (string POSITION , string MSG) {
            ExceptionLogModel eLog = new ExceptionLogModel();
            eLog.DATE = DateTime.Now.ToString("yyyy-MM-dd h:mm tt");
            eLog.MSG = MSG;
            eLog.POSITION = POSITION;
            SqliteDataAccess.SaveExceptionLog(eLog);
        }

        /* save botlog */
        static public void saveBotLog(string PAIR , string RATE , string PREMIUM , string ACTION)
        {
            BotLogModel bLog = new BotLogModel();
            bLog.DATE = DateTime.Now.ToString("yyyy-MM-dd h:mm tt");
            bLog.PAIR = PAIR;
            bLog.RATE = RATE;
            bLog.PREMIUM = PREMIUM;
            bLog.ACTION = ACTION;
            SqliteDataAccess.SaveBotLog(bLog);
        }

        /* save config */
        static public void saveConfig(string pair) {
            ConfigModel config = new ConfigModel();
            config.PAIR = pair;
            config.RATE = Program.orderStatus[pair].rate;
            config.PREMIUM = Program.orderStatus[pair].premium;
            config.UNIT = Program.orderStatus[pair].unit;
            config.MIN_ORDER = Program.orderStatus[pair].minOrder;
            config.API_CALL_TIME = Program.orderStatus[pair].apiCallTime;
            config.PAIR_STATUS = (Program.orderStatus[pair].pairStatus ? "Y" : "N");
            config.CONNECT_STATUS = (Program.orderStatus[pair].connectStatus ? "Y" : "N");
            config.UNIT_PRECISION = Program.orderStatus[pair].unit_precision.ToString();
            config.AMOUNT_PRECISION = Program.orderStatus[pair].amount_precision.ToString();
            config.FIRST_BUY_VALUE = Program.orderStatus[pair].firstBuyValue.ToString();
            config.FIRST_SELL_VALUE = Program.orderStatus[pair].firstSellValue.ToString();
            config.TIME_STAMP = Program.orderStatus[pair].timeStamp;
            ConfigModel value = new ConfigModel();
            value = SqliteDataAccess.GetConfig(pair);
            if (value == null)
                SqliteDataAccess.SaveConfig(config);
            else
                SqliteDataAccess.UpdateConfig(config);

        }

    }
}


