﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarketMaker
{
    public partial class ApiKey : Form
    {
        public ApiKey()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ApiKey_Load(object sender, EventArgs e)
        {
            this.API_KEY.Height = 40;
            this.API_KEY.Text = Program.api_key;
        }

        private void SAVE_Click(object sender, EventArgs e)
        {
            EnvironmentModel envirModel = new EnvironmentModel();

            if( API_KEY.Text != "") {

                envirModel = SqliteDataAccess.GetEnvironmentValue("API_KEY");
                if (envirModel != null)
                {
                    envirModel.VARIABLE = "API_KEY";
                    envirModel.VALUE = API_KEY.Text;

                    if ( !SqliteDataAccess.UpdateEnvironmentValue(envirModel) ) {
                        MessageBox.Show("API_KEY 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                }
                else {

                    envirModel = new EnvironmentModel();
                    envirModel.VARIABLE = "API_KEY";
                    envirModel.VALUE = API_KEY.Text;

                    SqliteDataAccess.SaveEnvironmentValue(envirModel);

                }
                Program.api_key = API_KEY.Text;
            }
            MessageBox.Show("성과적으로 저장되었습니다." , "성공" , MessageBoxButtons.OK , MessageBoxIcon.Information);
        }
        private void CANCEL_Click(object sender, EventArgs e)
        {
        }
    }
}
