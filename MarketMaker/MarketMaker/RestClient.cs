﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.IO;

namespace MarketMaker
{
    public enum HttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }

    class RestClient
    {
        public string EndPoint { get; set; }
        public HttpVerb Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }

        public RestClient()
        {
            EndPoint = "";
            Method = HttpVerb.GET;
            ContentType = "application/json";
            PostData = "";
        }

        /// 
        public RestClient(string endpoint)
        {
            EndPoint = endpoint;
            Method = HttpVerb.GET;
            ContentType = "application/json";
            PostData = "";
        }

        /// <summary>
        public RestClient(string endpoint, HttpVerb method)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = "application/json";
            PostData = "";
        }

        public RestClient(string endpoint, HttpVerb method, string postData)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = "application/json";
            PostData = postData;
        }

        public string MakeRequest()
        {
            return MakeRequest("");
        }

        public string MakeRequest(string parameters)
        {
            try
            {
                var endPoint = new Uri(string.Concat(EndPoint, parameters));
                var request = (HttpWebRequest)WebRequest.Create(endPoint);

                request.Method = Method.ToString();
                request.ContentLength = 0;
                request.ContentType = ContentType;

                if (!string.IsNullOrEmpty(PostData) && Method == HttpVerb.POST)
                {
                    var encoding = new UTF8Encoding();
                    var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }

                    // grab the response
                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }

                    return responseValue;
                }
            }
            catch (WebException webExcp)
            {
                CommonFunc.saveException("RestClient - MakeRequest", webExcp.Message);
                return "";
            }
            catch (Exception e)
            {
                return "";
            }
        }
      
        public List<UpbitOrderbook> getOrderBookFromUpbit(string pair)
        {

            this.EndPoint = Program.upbitOrderbookApi + "?markets=" + CommonFunc.getPairNameInUpbit(pair);
            this.Method = HttpVerb.GET;
            this.ContentType = "application/json";
            string response = MakeRequest();
            try
            {
                var values = JsonConvert.DeserializeObject<List<UpbitOrderbook>>(response);
                return values;
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                CommonFunc.saveException("RestClient - getOrderBookFromUpbit", e.Message);
                return null;
            }
            catch (Newtonsoft.Json.JsonSerializationException e)
            {
                CommonFunc.saveException("RestClient - getOrderBookFromUpbit", e.Message);
                return null;
            }
        }

        public MyOrders getMyOrder (string pair){
            this.EndPoint = Program.coinskyApi + "open_orders?market=" + CommonFunc.getPairNameInCoinSky(pair);
            this.Method = HttpVerb.POST;
            this.PostData = "{\"api_key\" : \"" + Program.api_key + "\"}";
            string response = MakeRequest();
            try
            {
                var values = JsonConvert.DeserializeObject<MyOrders>(response);
                return values;
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                CommonFunc.saveException("RestClient - getMyOrder From MicroApi", e.Message);
                return null;
            }
            catch (Newtonsoft.Json.JsonSerializationException e)
            {
                CommonFunc.saveException("RestClient - getMyOrder From MicroApi", e.Message);
                return null;
            }
        }

        public MyOrderbook getMyOrderBook (string pair)
        {
            this.EndPoint = Program.coinskyApi + "order_book?market=" + CommonFunc.getPairNameInCoinSky(pair);
            this.Method = HttpVerb.POST;
            this.PostData = "{\"api_key\" : \"" + Program.api_key + "\"}";
            string response = MakeRequest();
            try
            {
                var values = JsonConvert.DeserializeObject<MyOrderbook>(response);
                return values;
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                CommonFunc.saveException("RestClient - getMyOrderBook From CoinskyApi", e.Message);
                return null;
            }
            catch (Newtonsoft.Json.JsonSerializationException e)
            {
                CommonFunc.saveException("RestClient - getMyOrderBook From CoinskyApi", e.Message);
                return null;
            }
        }

    }
}
