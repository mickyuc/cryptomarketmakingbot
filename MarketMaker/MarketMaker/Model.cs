﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketMaker
{
    //환경 변수설정 표
    public class EnvironmentModel
    {
        public int NO { get; set; }
        public string VARIABLE { get; set;  }
        public string VALUE { get; set; }
    }

    //밸런스 표
    public class AccountModel
    {
        public int NO { get; set; }
        public string ACCOUNT_ID { get; set; }
        public string ORIGIN_AMOUNT { get; set; }
        public string DEL_YN { get; set; }
    }

    //포스트정보 이력따지기
    public class PostLogModel
    {

        public int NO { get; set; }
        public string POSTDATA { get; set; }
        public string ORG { get; set; }
        public string TYPE { get; set; }

    }

    //주문정보 1
    public class OrderLogModel
    {

        public int NO { get; set; }
        public string REAL_PRICE { get; set; }
        public string ORDER_ID { get; set; }
        public string HOGA_PRICE { get; set; }
        public string QUANTITY { get; set; }
        public string PAIR { get; set; }
    }

    //주문정보 2
    public class OrderCountModel
    {
        public int NO { get; set; }
        public string PAIR { get; set; }
        public string HOGA_PRICE { get; set; }
        public string ORDER_COUNT { get; set; }
    }

    //펌핑로그
    public class PumpingLogModel
    {
        public int NO { get; set; }
        public string DATE { get; set; }
        public string PAIR { get; set; }
        public string DIRECTION { get; set; }
        public string APPLY_TIME { get; set; }
        public string AIM { get; set; }
        public string BASE_COIN { get; set; }
        public string RATE { get; set; }
        public string PREMIUM { get; set; }
        public string ACTION { get; set; }
    }

    //예외처리 로그
    public class ExceptionLogModel
    {
        public int NO { get; set; }
        public string DATE { get; set; }
        public string POSITION { get; set; }
        public string MSG { get; set; }
    }

    //봇 로그 
    public class BotLogModel
    {
        public int NO { get; set; }
        public string DATE { get; set; }
        public string PAIR { get; set; }
        public string RATE { get; set; }
        public string PREMIUM { get; set; }
        public string ACTION  { get; set; }
    }

    //초기 설정 모델 
    public class ConfigModel
    {
        public int NO { get; set; }
        public string PAIR { get; set; }
        public string RATE { get; set; }
        public string PREMIUM { get; set; }
        public string UNIT { get; set; }
        public string MIN_ORDER { get; set; }
        public string API_CALL_TIME { get; set; }
        public string PAIR_STATUS { get; set; }
        public string CONNECT_STATUS { get; set; }
        public string UNIT_PRECISION { get; set; }
        public string AMOUNT_PRECISION { get; set; }
        public string FIRST_BUY_VALUE { get; set; }
        public string FIRST_SELL_VALUE { get; set; }
        public string TIME_STAMP { get; set; }
    }
}
