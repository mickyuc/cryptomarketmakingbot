﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarketMaker
{
    public partial class BotLog : Form
    {
        public BotLog()
        {
            InitializeComponent();
        }

        private void BotLog_Load(object sender, EventArgs e)
        {
            string curdate = DateTime.Now.ToString("yyyy-MM-dd") + "%";
            List<ExceptionLogModel> exceptionLogList = new List<ExceptionLogModel>();
            exceptionLogList = SqliteDataAccess.GetExceptionLogList(curdate);
            if(exceptionLogList != null)
            {
                for(int i = 0; i < exceptionLogList.Count; i ++) {
                    string[] row = new string[] {( i + 1 ).ToString(), exceptionLogList[i].DATE, exceptionLogList[i].POSITION, exceptionLogList[i].MSG};
                    dataGridView3.Rows.Add(row);
                }
            }
            List<BotLogModel> botLogList = new List<BotLogModel>();
            botLogList = SqliteDataAccess.GetBotLogList(curdate);
            if(botLogList != null)
            {
                for(int i = 0; i < botLogList.Count; i ++) {
                    string[] row = new string[] { (i + 1).ToString(), botLogList[i].DATE, botLogList[i].PAIR , botLogList[i].RATE , botLogList[i].PREMIUM , (botLogList[i].ACTION == "start" ? "시작" : "취소")};
                    dataGridView1.Rows.Add(row);
                }
            }
        }
    }
}
