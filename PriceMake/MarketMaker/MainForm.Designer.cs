﻿namespace MarketMaker
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_ETHBRL = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_BTCBRL = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_ETHUSDT = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_BTCUSDT = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_TUSDUSDT = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_ZECBTC = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_DASHBTC = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_LTCBTC = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_BCHBTC = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_ETCBTC = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_XRPBTC = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button24 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.BTN_BRLETH = new System.Windows.Forms.Button();
            this.BTN_BRLBTC = new System.Windows.Forms.Button();
            this.BTN_USDTETH = new System.Windows.Forms.Button();
            this.BTN_USDTTUDSD = new System.Windows.Forms.Button();
            this.BTN_USDTTUSD = new System.Windows.Forms.Button();
            this.BTN_BTCZEC = new System.Windows.Forms.Button();
            this.BTN_BTCDASH = new System.Windows.Forms.Button();
            this.BTN_BTCLTC = new System.Windows.Forms.Button();
            this.BTN_BTCBCH = new System.Windows.Forms.Button();
            this.BTN_BTCETC = new System.Windows.Forms.Button();
            this.BTN_BTCXRP = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BTN_BTCETH = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.SPIN_ETHBTC = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BTN_TOKEN = new System.Windows.Forms.Button();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.GLOBAL_LABEL = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_ETHBRL)).BeginInit();
            this.tableLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_BTCBRL)).BeginInit();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_ETHUSDT)).BeginInit();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_BTCUSDT)).BeginInit();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_TUSDUSDT)).BeginInit();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_ZECBTC)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_DASHBTC)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_LTCBTC)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_BCHBTC)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_ETCBTC)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_XRPBTC)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_ETHBTC)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.button3, 2, 12);
            this.tableLayoutPanel1.Controls.Add(this.button1, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel13, 3, 11);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel12, 3, 10);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel11, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel10, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel9, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel7, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.button24, 2, 11);
            this.tableLayoutPanel1.Controls.Add(this.button22, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.button20, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.button18, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.button16, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.button14, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.button12, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.button10, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.button8, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.button6, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.button4, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.button2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.BTN_BRLETH, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.BTN_BRLBTC, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.BTN_USDTETH, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.BTN_USDTTUDSD, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.BTN_USDTTUSD, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.BTN_BTCZEC, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.BTN_BTCDASH, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.BTN_BTCLTC, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.BTN_BTCBCH, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.BTN_BTCETC, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.BTN_BTCXRP, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label33, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label30, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label27, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label21, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.BTN_BTCETH, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 3, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(53, 115);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(572, 470);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Enabled = false;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(289, 435);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(137, 32);
            this.button3.TabIndex = 131;
            this.button3.Text = "전체중지";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(146, 435);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 32);
            this.button1.TabIndex = 130;
            this.button1.Text = "전체시작";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel13.Controls.Add(this.SPIN_ETHBRL, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.label22, 1, 0);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(432, 399);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel13.TabIndex = 128;
            // 
            // SPIN_ETHBRL
            // 
            this.SPIN_ETHBRL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_ETHBRL.Location = new System.Drawing.Point(3, 6);
            this.SPIN_ETHBRL.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_ETHBRL.Name = "SPIN_ETHBRL";
            this.SPIN_ETHBRL.Size = new System.Drawing.Size(103, 20);
            this.SPIN_ETHBRL.TabIndex = 0;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(112, 6);
            this.label22.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(22, 24);
            this.label22.TabIndex = 1;
            this.label22.Text = "%";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.Controls.Add(this.SPIN_BTCBRL, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label20, 1, 0);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(432, 363);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel12.TabIndex = 127;
            // 
            // SPIN_BTCBRL
            // 
            this.SPIN_BTCBRL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_BTCBRL.Location = new System.Drawing.Point(3, 6);
            this.SPIN_BTCBRL.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_BTCBRL.Name = "SPIN_BTCBRL";
            this.SPIN_BTCBRL.Size = new System.Drawing.Size(103, 20);
            this.SPIN_BTCBRL.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(112, 6);
            this.label20.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(22, 24);
            this.label20.TabIndex = 1;
            this.label20.Text = "%";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel11.Controls.Add(this.SPIN_ETHUSDT, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.label19, 1, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(432, 327);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel11.TabIndex = 126;
            // 
            // SPIN_ETHUSDT
            // 
            this.SPIN_ETHUSDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_ETHUSDT.Location = new System.Drawing.Point(3, 6);
            this.SPIN_ETHUSDT.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_ETHUSDT.Name = "SPIN_ETHUSDT";
            this.SPIN_ETHUSDT.Size = new System.Drawing.Size(103, 20);
            this.SPIN_ETHUSDT.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(112, 6);
            this.label19.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(22, 24);
            this.label19.TabIndex = 1;
            this.label19.Text = "%";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.Controls.Add(this.SPIN_BTCUSDT, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label17, 1, 0);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(432, 291);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel10.TabIndex = 125;
            // 
            // SPIN_BTCUSDT
            // 
            this.SPIN_BTCUSDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_BTCUSDT.Location = new System.Drawing.Point(3, 6);
            this.SPIN_BTCUSDT.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_BTCUSDT.Name = "SPIN_BTCUSDT";
            this.SPIN_BTCUSDT.Size = new System.Drawing.Size(103, 20);
            this.SPIN_BTCUSDT.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(112, 6);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 24);
            this.label17.TabIndex = 1;
            this.label17.Text = "%";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel9.Controls.Add(this.SPIN_TUSDUSDT, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label16, 1, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(432, 255);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel9.TabIndex = 124;
            // 
            // SPIN_TUSDUSDT
            // 
            this.SPIN_TUSDUSDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_TUSDUSDT.Location = new System.Drawing.Point(3, 6);
            this.SPIN_TUSDUSDT.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_TUSDUSDT.Name = "SPIN_TUSDUSDT";
            this.SPIN_TUSDUSDT.Size = new System.Drawing.Size(103, 20);
            this.SPIN_TUSDUSDT.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(112, 6);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 24);
            this.label16.TabIndex = 1;
            this.label16.Text = "%";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.Controls.Add(this.SPIN_ZECBTC, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label14, 1, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(432, 219);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel8.TabIndex = 123;
            // 
            // SPIN_ZECBTC
            // 
            this.SPIN_ZECBTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_ZECBTC.Location = new System.Drawing.Point(3, 6);
            this.SPIN_ZECBTC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_ZECBTC.Name = "SPIN_ZECBTC";
            this.SPIN_ZECBTC.Size = new System.Drawing.Size(103, 20);
            this.SPIN_ZECBTC.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(112, 6);
            this.label14.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 24);
            this.label14.TabIndex = 1;
            this.label14.Text = "%";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.Controls.Add(this.SPIN_DASHBTC, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label13, 1, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(432, 183);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel7.TabIndex = 122;
            // 
            // SPIN_DASHBTC
            // 
            this.SPIN_DASHBTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_DASHBTC.Location = new System.Drawing.Point(3, 6);
            this.SPIN_DASHBTC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_DASHBTC.Name = "SPIN_DASHBTC";
            this.SPIN_DASHBTC.Size = new System.Drawing.Size(103, 20);
            this.SPIN_DASHBTC.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(112, 6);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 24);
            this.label13.TabIndex = 1;
            this.label13.Text = "%";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel6.Controls.Add(this.SPIN_LTCBTC, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label11, 1, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(432, 147);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel6.TabIndex = 121;
            // 
            // SPIN_LTCBTC
            // 
            this.SPIN_LTCBTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_LTCBTC.Location = new System.Drawing.Point(3, 6);
            this.SPIN_LTCBTC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_LTCBTC.Name = "SPIN_LTCBTC";
            this.SPIN_LTCBTC.Size = new System.Drawing.Size(103, 20);
            this.SPIN_LTCBTC.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(112, 6);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 24);
            this.label11.TabIndex = 1;
            this.label11.Text = "%";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.Controls.Add(this.SPIN_BCHBTC, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label10, 1, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(432, 111);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel5.TabIndex = 120;
            // 
            // SPIN_BCHBTC
            // 
            this.SPIN_BCHBTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_BCHBTC.Location = new System.Drawing.Point(3, 6);
            this.SPIN_BCHBTC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_BCHBTC.Name = "SPIN_BCHBTC";
            this.SPIN_BCHBTC.Size = new System.Drawing.Size(103, 20);
            this.SPIN_BCHBTC.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(112, 6);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 24);
            this.label10.TabIndex = 1;
            this.label10.Text = "%";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.Controls.Add(this.SPIN_ETCBTC, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label8, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(432, 75);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel4.TabIndex = 119;
            // 
            // SPIN_ETCBTC
            // 
            this.SPIN_ETCBTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_ETCBTC.Location = new System.Drawing.Point(3, 6);
            this.SPIN_ETCBTC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_ETCBTC.Name = "SPIN_ETCBTC";
            this.SPIN_ETCBTC.Size = new System.Drawing.Size(103, 20);
            this.SPIN_ETCBTC.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(112, 6);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 24);
            this.label8.TabIndex = 1;
            this.label8.Text = "%";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Controls.Add(this.SPIN_XRPBTC, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(432, 39);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel3.TabIndex = 118;
            // 
            // SPIN_XRPBTC
            // 
            this.SPIN_XRPBTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_XRPBTC.Location = new System.Drawing.Point(3, 6);
            this.SPIN_XRPBTC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_XRPBTC.Name = "SPIN_XRPBTC";
            this.SPIN_XRPBTC.Size = new System.Drawing.Size(103, 20);
            this.SPIN_XRPBTC.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(112, 6);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 24);
            this.label7.TabIndex = 1;
            this.label7.Text = "%";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 432);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 38);
            this.label2.TabIndex = 116;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button24
            // 
            this.button24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button24.Enabled = false;
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button24.Location = new System.Drawing.Point(289, 399);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(137, 30);
            this.button24.TabIndex = 100;
            this.button24.Text = "중지";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button22
            // 
            this.button22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button22.Enabled = false;
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.Location = new System.Drawing.Point(289, 363);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(137, 30);
            this.button22.TabIndex = 99;
            this.button22.Text = "중지";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button20
            // 
            this.button20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button20.Enabled = false;
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.Location = new System.Drawing.Point(289, 327);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(137, 30);
            this.button20.TabIndex = 98;
            this.button20.Text = "중지";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button18
            // 
            this.button18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button18.Enabled = false;
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.Location = new System.Drawing.Point(289, 291);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(137, 30);
            this.button18.TabIndex = 97;
            this.button18.Text = "중지";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button16
            // 
            this.button16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button16.Enabled = false;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Location = new System.Drawing.Point(289, 255);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(137, 30);
            this.button16.TabIndex = 96;
            this.button16.Text = "중지";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button14
            // 
            this.button14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button14.Enabled = false;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(289, 219);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(137, 30);
            this.button14.TabIndex = 95;
            this.button14.Text = "중지";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button12
            // 
            this.button12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button12.Enabled = false;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(289, 183);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(137, 30);
            this.button12.TabIndex = 94;
            this.button12.Text = "중지";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button10
            // 
            this.button10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button10.Enabled = false;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(289, 147);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(137, 30);
            this.button10.TabIndex = 93;
            this.button10.Text = "중지";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Enabled = false;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(289, 111);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(137, 30);
            this.button8.TabIndex = 92;
            this.button8.Text = "중지";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button6
            // 
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(289, 75);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(137, 30);
            this.button6.TabIndex = 91;
            this.button6.Text = "중지";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Enabled = false;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(289, 39);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(137, 30);
            this.button4.TabIndex = 90;
            this.button4.Text = "중지";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(289, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 30);
            this.button2.TabIndex = 89;
            this.button2.Text = "중지";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // BTN_BRLETH
            // 
            this.BTN_BRLETH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_BRLETH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_BRLETH.Location = new System.Drawing.Point(146, 399);
            this.BTN_BRLETH.Name = "BTN_BRLETH";
            this.BTN_BRLETH.Size = new System.Drawing.Size(137, 30);
            this.BTN_BRLETH.TabIndex = 88;
            this.BTN_BRLETH.Text = "시작";
            this.BTN_BRLETH.UseVisualStyleBackColor = true;
            this.BTN_BRLETH.Click += new System.EventHandler(this.BTN_BRLETH_Click);
            // 
            // BTN_BRLBTC
            // 
            this.BTN_BRLBTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_BRLBTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_BRLBTC.Location = new System.Drawing.Point(146, 363);
            this.BTN_BRLBTC.Name = "BTN_BRLBTC";
            this.BTN_BRLBTC.Size = new System.Drawing.Size(137, 30);
            this.BTN_BRLBTC.TabIndex = 86;
            this.BTN_BRLBTC.Text = "시작";
            this.BTN_BRLBTC.UseVisualStyleBackColor = true;
            this.BTN_BRLBTC.Click += new System.EventHandler(this.BTN_BRLBTC_Click);
            // 
            // BTN_USDTETH
            // 
            this.BTN_USDTETH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_USDTETH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_USDTETH.Location = new System.Drawing.Point(146, 327);
            this.BTN_USDTETH.Name = "BTN_USDTETH";
            this.BTN_USDTETH.Size = new System.Drawing.Size(137, 30);
            this.BTN_USDTETH.TabIndex = 84;
            this.BTN_USDTETH.Text = "시작";
            this.BTN_USDTETH.UseVisualStyleBackColor = true;
            this.BTN_USDTETH.Click += new System.EventHandler(this.BTN_USDTETH_Click);
            // 
            // BTN_USDTTUDSD
            // 
            this.BTN_USDTTUDSD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_USDTTUDSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_USDTTUDSD.Location = new System.Drawing.Point(146, 291);
            this.BTN_USDTTUDSD.Name = "BTN_USDTTUDSD";
            this.BTN_USDTTUDSD.Size = new System.Drawing.Size(137, 30);
            this.BTN_USDTTUDSD.TabIndex = 82;
            this.BTN_USDTTUDSD.Text = "시작";
            this.BTN_USDTTUDSD.UseVisualStyleBackColor = true;
            this.BTN_USDTTUDSD.Click += new System.EventHandler(this.BTN_USDTTUDSD_Click);
            // 
            // BTN_USDTTUSD
            // 
            this.BTN_USDTTUSD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_USDTTUSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_USDTTUSD.Location = new System.Drawing.Point(146, 255);
            this.BTN_USDTTUSD.Name = "BTN_USDTTUSD";
            this.BTN_USDTTUSD.Size = new System.Drawing.Size(137, 30);
            this.BTN_USDTTUSD.TabIndex = 80;
            this.BTN_USDTTUSD.Text = "시작";
            this.BTN_USDTTUSD.UseVisualStyleBackColor = true;
            this.BTN_USDTTUSD.Click += new System.EventHandler(this.BTN_USDTTUSD_Click);
            // 
            // BTN_BTCZEC
            // 
            this.BTN_BTCZEC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_BTCZEC.Enabled = false;
            this.BTN_BTCZEC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_BTCZEC.Location = new System.Drawing.Point(146, 219);
            this.BTN_BTCZEC.Name = "BTN_BTCZEC";
            this.BTN_BTCZEC.Size = new System.Drawing.Size(137, 30);
            this.BTN_BTCZEC.TabIndex = 78;
            this.BTN_BTCZEC.Text = "시작";
            this.BTN_BTCZEC.UseVisualStyleBackColor = true;
            this.BTN_BTCZEC.Click += new System.EventHandler(this.BTN_BTCZEC_Click);
            // 
            // BTN_BTCDASH
            // 
            this.BTN_BTCDASH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_BTCDASH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_BTCDASH.Location = new System.Drawing.Point(146, 183);
            this.BTN_BTCDASH.Name = "BTN_BTCDASH";
            this.BTN_BTCDASH.Size = new System.Drawing.Size(137, 30);
            this.BTN_BTCDASH.TabIndex = 76;
            this.BTN_BTCDASH.Text = "시작";
            this.BTN_BTCDASH.UseVisualStyleBackColor = true;
            this.BTN_BTCDASH.Click += new System.EventHandler(this.BTN_BTCDASH_Click);
            // 
            // BTN_BTCLTC
            // 
            this.BTN_BTCLTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_BTCLTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_BTCLTC.Location = new System.Drawing.Point(146, 147);
            this.BTN_BTCLTC.Name = "BTN_BTCLTC";
            this.BTN_BTCLTC.Size = new System.Drawing.Size(137, 30);
            this.BTN_BTCLTC.TabIndex = 74;
            this.BTN_BTCLTC.Text = "시작";
            this.BTN_BTCLTC.UseVisualStyleBackColor = true;
            this.BTN_BTCLTC.Click += new System.EventHandler(this.BTN_BTCLTC_Click);
            // 
            // BTN_BTCBCH
            // 
            this.BTN_BTCBCH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_BTCBCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_BTCBCH.Location = new System.Drawing.Point(146, 111);
            this.BTN_BTCBCH.Name = "BTN_BTCBCH";
            this.BTN_BTCBCH.Size = new System.Drawing.Size(137, 30);
            this.BTN_BTCBCH.TabIndex = 72;
            this.BTN_BTCBCH.Text = "시작";
            this.BTN_BTCBCH.UseVisualStyleBackColor = true;
            this.BTN_BTCBCH.Click += new System.EventHandler(this.BTN_BTCBCH_Click);
            // 
            // BTN_BTCETC
            // 
            this.BTN_BTCETC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_BTCETC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_BTCETC.Location = new System.Drawing.Point(146, 75);
            this.BTN_BTCETC.Name = "BTN_BTCETC";
            this.BTN_BTCETC.Size = new System.Drawing.Size(137, 30);
            this.BTN_BTCETC.TabIndex = 70;
            this.BTN_BTCETC.Text = "시작";
            this.BTN_BTCETC.UseVisualStyleBackColor = true;
            this.BTN_BTCETC.Click += new System.EventHandler(this.BTN_BTCETC_Click);
            // 
            // BTN_BTCXRP
            // 
            this.BTN_BTCXRP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_BTCXRP.Enabled = false;
            this.BTN_BTCXRP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_BTCXRP.Location = new System.Drawing.Point(146, 39);
            this.BTN_BTCXRP.Name = "BTN_BTCXRP";
            this.BTN_BTCXRP.Size = new System.Drawing.Size(137, 30);
            this.BTN_BTCXRP.TabIndex = 68;
            this.BTN_BTCXRP.Text = "시작";
            this.BTN_BTCXRP.UseVisualStyleBackColor = true;
            this.BTN_BTCXRP.Click += new System.EventHandler(this.BTN_BTCXRP_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 396);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(137, 36);
            this.label33.TabIndex = 66;
            this.label33.Text = "BRL-ETH";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(3, 360);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(137, 36);
            this.label30.TabIndex = 63;
            this.label30.Text = "BRL-BTC";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(3, 324);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(137, 36);
            this.label27.TabIndex = 60;
            this.label27.Text = "USDT-ETH";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(3, 288);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(137, 36);
            this.label24.TabIndex = 57;
            this.label24.Text = "USDT-BTC";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(3, 252);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(137, 36);
            this.label21.TabIndex = 54;
            this.label21.Text = "USDT-TUSD";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 216);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(137, 36);
            this.label18.TabIndex = 51;
            this.label18.Text = "BTC-ZEC";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 180);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(137, 36);
            this.label15.TabIndex = 48;
            this.label15.Text = "BTC-DASH";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 144);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 36);
            this.label12.TabIndex = 45;
            this.label12.Text = "BTC-LTC";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(137, 36);
            this.label9.TabIndex = 42;
            this.label9.Text = "BTC-BCH";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(137, 36);
            this.label6.TabIndex = 39;
            this.label6.Text = "BTC-ETC";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 36);
            this.label3.TabIndex = 36;
            this.label3.Text = "BTC-XRP";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "BTC-ETH";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BTN_BTCETH
            // 
            this.BTN_BTCETH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_BTCETH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_BTCETH.Location = new System.Drawing.Point(146, 3);
            this.BTN_BTCETH.Name = "BTN_BTCETH";
            this.BTN_BTCETH.Size = new System.Drawing.Size(137, 30);
            this.BTN_BTCETH.TabIndex = 34;
            this.BTN_BTCETH.Text = "시작";
            this.BTN_BTCETH.UseVisualStyleBackColor = true;
            this.BTN_BTCETH.Click += new System.EventHandler(this.BTN_BTCETH_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.SPIN_ETHBTC, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label5, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(432, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(137, 30);
            this.tableLayoutPanel2.TabIndex = 117;
            // 
            // SPIN_ETHBTC
            // 
            this.SPIN_ETHBTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SPIN_ETHBTC.Location = new System.Drawing.Point(3, 6);
            this.SPIN_ETHBTC.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.SPIN_ETHBTC.Name = "SPIN_ETHBTC";
            this.SPIN_ETHBTC.Size = new System.Drawing.Size(103, 20);
            this.SPIN_ETHBTC.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(112, 6);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 24);
            this.label5.TabIndex = 1;
            this.label5.Text = "%";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Yu Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(503, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 25);
            this.label4.TabIndex = 2;
            this.label4.Text = "프리미엄";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BTN_TOKEN
            // 
            this.BTN_TOKEN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_TOKEN.Location = new System.Drawing.Point(53, 15);
            this.BTN_TOKEN.Name = "BTN_TOKEN";
            this.BTN_TOKEN.Size = new System.Drawing.Size(146, 34);
            this.BTN_TOKEN.TabIndex = 3;
            this.BTN_TOKEN.Text = "토큰설정";
            this.BTN_TOKEN.UseVisualStyleBackColor = true;
            this.BTN_TOKEN.Click += new System.EventHandler(this.BTN_TOKEN_Click);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            // 
            // GLOBAL_LABEL
            // 
            this.GLOBAL_LABEL.AutoSize = true;
            this.GLOBAL_LABEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GLOBAL_LABEL.Location = new System.Drawing.Point(54, 71);
            this.GLOBAL_LABEL.Name = "GLOBAL_LABEL";
            this.GLOBAL_LABEL.Size = new System.Drawing.Size(0, 20);
            this.GLOBAL_LABEL.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 611);
            this.Controls.Add(this.GLOBAL_LABEL);
            this.Controls.Add(this.BTN_TOKEN);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Price Maker";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_ETHBRL)).EndInit();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_BTCBRL)).EndInit();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_ETHUSDT)).EndInit();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_BTCUSDT)).EndInit();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_TUSDUSDT)).EndInit();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_ZECBTC)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_DASHBTC)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_LTCBTC)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_BCHBTC)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_ETCBTC)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_XRPBTC)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SPIN_ETHBTC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button BTN_BRLETH;
        private System.Windows.Forms.Button BTN_BRLBTC;
        private System.Windows.Forms.Button BTN_USDTETH;
        private System.Windows.Forms.Button BTN_USDTTUDSD;
        private System.Windows.Forms.Button BTN_USDTTUSD;
        private System.Windows.Forms.Button BTN_BTCZEC;
        private System.Windows.Forms.Button BTN_BTCDASH;
        private System.Windows.Forms.Button BTN_BTCLTC;
        private System.Windows.Forms.Button BTN_BTCBCH;
        private System.Windows.Forms.Button BTN_BTCETC;
        private System.Windows.Forms.Button BTN_BTCXRP;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BTN_BTCETH;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.NumericUpDown SPIN_ETHBTC;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.NumericUpDown SPIN_ETHBRL;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.NumericUpDown SPIN_BTCBRL;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.NumericUpDown SPIN_ETHUSDT;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.NumericUpDown SPIN_BTCUSDT;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.NumericUpDown SPIN_TUSDUSDT;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.NumericUpDown SPIN_ZECBTC;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.NumericUpDown SPIN_DASHBTC;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.NumericUpDown SPIN_LTCBTC;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.NumericUpDown SPIN_BCHBTC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.NumericUpDown SPIN_ETCBTC;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.NumericUpDown SPIN_XRPBTC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button BTN_TOKEN;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Label GLOBAL_LABEL;
    }
}

