﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace MarketMaker
{
    public class BITTREX_CLASS
    {
        static public BittrexWebsocket btx = new BittrexWebsocket();
        // bit Trex websocket connect
        static public void connectBitTrexWebSocket(string marketName)
        {

            Task task = Task.Run(
                async () =>
                {
                    string apiKey = "YOUR_API_KEY";
                    string apiSecret = "YOUR_API_SECRET";
                    string baseUrl = "https://socket.bittrex.com/signalr";

                    await btx.SubscribeToSummaryDeltasETHBTC();
                });

            task.Wait();

        }
    }

    public class OrderBookItem
    {
        public double price { get; set; }
        public double quantity { get; set; }
    }

    public class PriceResult
    {
        public List<PriceItem> D { get; set; }
    }

    public class PriceItem
    {
        public string M { get; set; }
        public string l { get; set; }
        public string m { get; set; }
    }

    //order result
    public class OrderResult
    {

        public bool success { get; set; }
        public string trace { get; set; }
        public string msg { get; set; }
        public string d { get; set; }

    }
    //Micro order book return result
    public class MicroBookResult
    {
        public bool success;
        public string trace;
        public string msg;
        public string scalar;
        public MicroBookResultDetail d;
    }

    public class MicroBookResultItem
    {
        public string side;
        public double price;
        public double sum;
    }

    public class MicroBookResultDetail
    {
        public List<MicroBookResultItem> item;
        public string total;
    }

    // BitTrex WebSocket Icon

    public class Result
    {
        public string M { get; set; }
        public long N { get; set; }
        public List<Entry> Z { get; set; }
        public List<Entry> S { get; set; }
        public List<Entry> f { get; set; }
    }

    public class Entry
    {
        public int TY { get; set; }
        public double R { get; set; }
        public double Q { get; set; }
    }
    //combobox Item
    public class ComboboxItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public override string ToString() {
            return Text;
        }
    }

    // bitcointrade.com.br
    public class BitcoinTrade
    {
        public string message { get; set; }
        public BitcoinTradeData data { get; set; }
    }

    public class BitcoinTradeData
    {
        public List<BitcoinTradeItem> bids;
        public List<BitcoinTradeItem> asks;
    }

    public class BitcoinTradeItem
    {
        public double unit_price { get; set; }
        public double amount { get; set; }
    }
    // bitfinex orderbook rest api result

    public class BitfinexApiResult
    {
        public List<BitfinexApiResultItem> bids { get; set; }
        public List<BitfinexApiResultItem> asks { get; set; }
    }

    public class BitfinexApiResultItem
    {
        public double price { get; set; }
        public double amount { get; set; }
        public string timestamp { get; set; }
    }

    // bittrex orderbook rest api result
    public class BittrexApiResultItem {

        public double Quantity { get; set; }
        public double Rate { get; set; }

    }

    public class BittrexApiResultItemList { 

        public List<BittrexApiResultItem> buy { get; set; }
        public List<BittrexApiResultItem> sell { get; set; }

    }

    public class BittrexApiResult
    {
        public bool success { get; set; }
        public string message { get; set; }
        public BittrexApiResultItemList result { get; set; }
    }

    // my orders from Micro Coin
    public class MyOrders
    {
        public bool success { get; set; }
        public string trace { get; set; }
        public string msg { get; set; }
        public MyOrdersDetail d { get; set; }

    }
    public class MyOrdersDetail
    {
        public string total { get; set; }
        public List<MyOrderItem> item { get; set; }
    }
    public class MyOrderItem
    {
        public string id { get; set; }
        public string side { get; set; }
        public string market_id { get; set; }
        public string coin_id { get; set; }
        public string qty { get; set; }
        public string price { get; set; }
        public string qty_traded { get; set; }
        public string status { get; set; }
        public string created_at { get; set; }
    }

    public class MicroOrderItem
    {
        public double qty { get; set; }
        public double price { get; set; }
    }

    
    /// /////////////////////////////////////////////////////////////
    public class BittrexTickResult
    {
        public bool success { get; set; }
        public string message { get; set; }
        public BittrexResult result { get; set; }
    }

    public class BittrexResult
    {
        public double Bid { get; set; }
        public double Ask { get; set; }
        public double Last { get; set; }
    }
    /// ////////////////////////////////////////////////////////////////
    public class brlTicker
    {
        public double high { get; set; }
        public double low { get; set; }
        public double volume { get; set; }
        public double quantity { get; set; }
        public double last { get; set; }
        public double buy { get; set; }
        public double sell { get; set; }
        public string date { get; set; }
    }

    public class bitcoinTraderTicker
    {
        public string message { get; set; }
        public brlTicker data { get; set; }
    }
    /////////////////////////////////////////////////////////////////////

    public class CommonFunc
    {
        //프리미엄 체크 ...
        static public List<BittrexApiResultItem> returnFilterData (List<BittrexApiResultItem> param , bool type , string pair)
        {

            // 수량 비율 , 프리미엄 , 최소주문수량
            int i = 0;
            while (true) {

                if(i == param.Count)
                    break;

                double rate = 0;

                if(type)
                {
                    if (double.Parse(Program.orderStatus[pair].rate) == 0)
                        rate = 1;
                    else
                        rate = double.Parse(Program.orderStatus[pair].rate) / 100;
                }
              

                if (type && param[i].Quantity * rate / Program.balance_number >= double.Parse(Program.orderStatus[pair].minOrder))
                {
                    param[i].Quantity = param[i].Quantity * rate / Program.balance_number;
                    param[i].Rate = (1 + double.Parse(Program.orderStatus[pair].premium) / 100) * param[i].Rate;
                    i++;
                }
                else
                {
                    param.RemoveAt(i);
                }
            }

            return param;
        }

        static public string getPairNameInBittrex(string marketName){

            string ret = "";
            switch(marketName){
                case "ETH/BTC":
                    ret = "BTC-ETH";
                    break;
                case "XRP/BTC":
                    ret = "BTC-XRP";
                    break;
                case "ETC/BTC":
                    ret = "BTC-ETC";
                    break;
                case "BCH/BTC":
                    ret = "BTC-BCH";
                    break;
                case "LTC/BTC":
                    ret = "BTC-LTC";
                    break;
                case "DASH/BTC":
                    ret = "BTC-DASH";
                    break;
                case "ZEC/BTC":
                    ret = "BTC-ZEC";
                    break;
                case "TUSD/USDT":
                    ret = "USDT-TUSD";
                    break;
                case "BTC/USDT":
                    ret = "USDT-BTC";
                    break;
                case "ETH/USDT":
                    ret = "USDT-ETH";
                    break;
                case "MIC/BTC":
                    ret = "BTC-XRP";
                    break;
                default:
                    break;
            }

            return ret;
        }

            static public string getPairNameInMicro1(string marketName)
            {

                string ret = "";
                switch (marketName)
                {
                    case "ETH/BTC":
                        ret = "BTC/ETH";
                        break;
                    case "XRP/BTC":
                        ret = "BTC/XRP";
                        break;
                    case "ETC/BTC":
                        ret = "BTC/ETC";
                        break;
                    case "BCH/BTC":
                        ret = "BTC/BCH";
                        break;
                    case "LTC/BTC":
                        ret = "BTC/LTC";
                        break;
                    case "DASH/BTC":
                        ret = "BTC/DASH";
                        break;
                    case "ZEC/BTC":
                        ret = "BTC/ZEC";
                        break;
                    case "TUSD/USDT":
                        ret = "USDT/TUSD";
                        break;
                    case "BTC/USDT":
                        ret = "USDT/BTC";
                        break;
                    case "ETH/USDT":
                        ret = "USDT/ETH";
                        break;
                    case "EOS/BTC":
                        ret = "BTC/EOS";
                        break;
                    case "BTC/BRL":
                        ret = "BRL/BTC";
                        break;
                    case "ETH/BRL":
                        ret = "BRL/ETH";
                        break;
                    case "MIC/BTC":
                        ret = "BTC/MIC";
                        break;
                    case "KEY/ETH":
                        ret = "ETH/KEY";
                        break;
                    case "ERL/ETH":
                        ret = "ETH/ERL";
                        break;

                default:
                        break;
                }

                return ret;
            }

        static public string getPairNameInMicro2(string marketName)
        {

            string ret = "";
            switch (marketName)
            {
                case "ETH/BTC":
                    ret = "btcmarket/eth";
                    break;
                case "XRP/BTC":
                    ret = "btcmarket/xrp";
                    break;
                case "ETC/BTC":
                    ret = "btcmarket/etc";
                    break;
                case "BCH/BTC":
                    ret = "btcmarket/bch";
                    break;
                case "LTC/BTC":
                    ret = "btcmarket/ltc";
                    break;
                case "DASH/BTC":
                    ret = "btcmarket/dash";
                    break;
                case "ZEC/BTC":
                    ret = "btcmarket/zec";
                    break;
                case "TUSD/USDT":
                    ret = "usdtmarket/tusd";
                    break;
                case "BTC/USDT":
                    ret = "usdtmarket/btc";
                    break;
                case "ETH/USDT":
                    ret = "usdtmarket/eth";
                    break;
                case "EOS/BTC":
                    ret = "btcmarket/eos";
                    break;
                case "BTC/BRL":
                    ret = "brlmarket/btc";
                    break;
                case "ETH/BRL":
                    ret = "brlmarket/eth";
                    break;
                case "MIC/BTC":
                    ret = "btcmarket/mic";
                    break;
                case "KEY/ETH":
                    ret = "ethmarket/key";
                    break;
                case "ELF/ETH":
                    ret = "ethmarket/elf";
                    break;

                default:
                    break;
            }

            return ret;
        }
        
        static public string getPairNameInMicro3(string marketName)
        {

            string ret = "";
            switch (marketName)
            {
                case "BTC-ETH":
                    ret = "btcmarket/eth";
                    break;
                case "BTC-XRP":
                    ret = "btcmarket/xrp";
                    break;
                case "BTC-ETC":
                    ret = "btcmarket/etc";
                    break;
                case "BTC-BCH":
                    ret = "btcmarket/bch";
                    break;
                case "BTC-LTC":
                    ret = "btcmarket/ltc";
                    break;
                case "BTC-DASH":
                    ret = "btcmarket/dash";
                    break;
                case "BTC-ZEC":
                    ret = "btcmarket/zec";
                    break;
                case "USDT-TUSD":
                    ret = "usdtmarket/tusd";
                    break;
                case "USDT-BTC":
                    ret = "usdtmarket/btc";
                    break;
                case "USDT-ETH":
                    ret = "usdtmarket/eth";
                    break;
                default:
                    break;
            }

            return ret;
        }

        static public string getPairNameInMicro4(string marketName)
        {

            string ret = "";
            switch (marketName)
            {

                case "BTC-ETH":
                    ret = "ETH/BTC";
                    break;
                case "BTC-XRP":
                    ret = "XRP/BTC";
                    break;
                case "BTC-ETC":
                    ret = "ETC/BTC";
                    break;
                case "BTC-BCH":
                    ret = "BCH/BTC";
                    break;
                case "BTC-LTC":
                    ret = "LTC/BTC";
                    break;
                case "BTC-DASH":
                    ret = "DASH/BTC";
                    break;
                case "BTC-ZEC":
                    ret = "ZEC/BTC";
                    break;
                case "USDT-TUSD":
                    ret = "TUSD/USDT";
                    break;
                case "USDT-BTC":
                    ret = "BTC/USDT";
                    break;
                case "USDT-ETH":
                    ret = "ETH/USDT";
                    break;

                default:
                    break;
            }

            return ret;
        }
        static public string getPairNameInBitcointrader(string marketName)
        {

            string ret = "";
            switch (marketName)
            {
                case "BTC/BRL":
                    ret = "BRLBTC";
                    break;

                case "ETH/BRL":
                    ret = "BRLETH";
                    break;

                default:
                    break;
            }

            return ret;
        }

        /* check network status */
        /*
         * position - bittrex , micro , bitcointrader
         * error_trigger - true : 오류발생 , false: 정상 
         */
        static public void checkNetworkStatus(string position, bool error_trigger)
        {

            if (!Program.networkConnection)
                return;

            if (!error_trigger && Program.networkStatus[position].error)
            {
                Program.networkStatus[position].error = false;
                if (!Program.networkStatus[position].apiConnection)
                    Program.networkStatus[position].status = 2;
                Program.networkStatus[position].apiConnection = true;
            }
            else if (error_trigger && !Program.networkStatus[position].error)
            {
                Program.networkStatus[position].error = true;
                Program.networkStatus[position].errorTime = DateTime.Now;
                Program.networkStatus[position].connectionTryFailed = 1;
            }
            else if (error_trigger && Program.networkStatus[position].error)
            {

                DateTime nTime = DateTime.Now;
                TimeSpan span = nTime - Program.networkStatus[position].errorTime;
                double t = span.TotalMilliseconds;
                Program.networkStatus[position].connectionTryFailed++;

                if (t > Program.networkStatus[position].delay || Program.networkStatus[position].connectionTryFailed > Program.networkStatus[position].errorCnt)
                {
                    Program.networkStatus[position].status = 2;
                    Program.networkStatus[position].apiConnection = false;
                }
            }
        }

        /* check json data format status */
        /*
         * position - bittrex , micro , bitcointrader
         * error_trigger - true : 오류발생 , false: 정상 
         */
        static public void checkResponseJsonDataFormat(string position, bool error_trigger)
        {
            if (!Program.networkConnection)
                return;
            if (!error_trigger && Program.networkStatus[position].jsonDataFormat)
            {
                Program.networkStatus[position].jsonDataFormat = false;

                if (!Program.networkStatus[position].jsonResponseStatus)
                    Program.networkStatus[position].status = 3;

                Program.networkStatus[position].jsonResponseStatus = true;
            }
            else if (error_trigger && !Program.networkStatus[position].jsonDataFormat)
            {
                Program.networkStatus[position].jsonDataFormat = true;
                Program.networkStatus[position].jsonErrorCnt = 1;
            }
            else if (error_trigger && Program.networkStatus[position].jsonDataFormat)
            {
                Program.networkStatus[position].jsonErrorCnt++;
                if (Program.networkStatus[position].jsonErrorCnt > Program.networkStatus[position].jsonErrorLimit)
                {
                    Program.networkStatus[position].status = 3;
                    Program.networkStatus[position].jsonResponseStatus = false;
                }
            }
        }
    }
}


