﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using Microsoft.AspNet.SignalR.Client;

namespace MarketMaker
{
    public sealed class BittrexWebsocket
    {
        public delegate void BittrexCallback(string info);

        private HubConnection _hubConnection { get; set; }
        private IHubProxy _hubProxy { get; set; }
        private BittrexCallback _updateExchangeState { get; set; }
        private BittrexCallback _updateSummaryState { get; set; }
        private BittrexCallback _updateBalanceState { get; set; }
        private string _connectionUrl { get; set; }
        private Action<string> _disConnect { get; set;  }

        public BittrexWebsocket(
            )
        {
            // Set delegates
            // Create connection to c2 SignalR hub
        }
        public void Initialize(string connectionUrl,
            BittrexCallback updateExchangeState,
            BittrexCallback updateSummaryState,
            BittrexCallback updateBalanceState,
            Action<string> disconnectCallback)
        {
            _updateExchangeState = updateExchangeState;
            _updateSummaryState = updateSummaryState;
            _updateBalanceState = updateBalanceState;

            _connectionUrl = connectionUrl;
            _disConnect = disconnectCallback;
        }

        public void setConnection()
        {
            // Create connection to c2 SignalR hub
            _hubConnection = new HubConnection(_connectionUrl);
            _hubProxy = _hubConnection.CreateHubProxy("c2");

            // Register callback for uE (exchange state delta) events
            _hubProxy.On(
                "uE",
                exchangeStateDelta => _updateExchangeState?.Invoke(exchangeStateDelta)
                );

            // Register callback for uO (order status change) events
            _hubProxy.On(
                "uL",
                summaryStateDelta => _updateSummaryState?.Invoke(summaryStateDelta)
                );

            // Register callback for uB (balance status change) events
            _hubProxy.On(
                "uB",
                balanceStateDelta => _updateBalanceState?.Invoke(balanceStateDelta)
                );

            _hubConnection.Closed += () => _disConnect("disconnect");

            _hubConnection.Start().Wait();
        }
        public void Shutdown() => _hubConnection.Stop();

        // marketName example: "BTC-LTC"
        //public async Task<bool> SubscribeToExchangeDeltas(string marketName) => await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", marketName);
        
        public async Task<bool> SubscribeToSummaryDeltasETHBTC() => await _hubProxy.Invoke<bool>("SubscribeToSummaryLiteDeltas");
        /* public async Task<bool> SubscribeToExchangeDeltasXRPBTC() => await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", "BTC-XRP");
        public async Task<bool> SubscribeToExchangeDeltasETCBTC() => await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", "BTC-ETC");
        public async Task<bool> SubscribeToExchangeDeltasBCHBTC() => await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", "BTC-BCH");
        public async Task<bool> SubscribeToExchangeDeltasLTCBTC() => await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", "BTC-LTC");
        public async Task<bool> SubscribeToExchangeDeltasDASHBTC() => await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", "BTC-DASH");
        public async Task<bool> SubscribeToExchangeDeltasZECBTC() => await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", "BTC-ZEC");
        public async Task<bool> SubscribeToExchangeDeltasTUSDUSDT() => await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", "USDT-TUSD");
        public async Task<bool> SubscribeToExchangeDeltasBTCUSDT() => await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", "USDT-BTC");
        public async Task<bool> SubscribeToExchangeDeltasETHUSDT() => await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", "USDT-ETH"); */

        // The return of GetAuthContext is a challenge string. Call CreateSignature(apiSecret, challenge)
        // for the response to the challenge, and pass it to Authenticate().
        public async Task<string> GetAuthContext(string apiKey) => await _hubProxy.Invoke<string>("GetAuthContext", apiKey);

        public async Task<bool> Authenticate(string apiKey, string signedChallenge) => await _hubProxy.Invoke<bool>("Authenticate", apiKey, signedChallenge);

        // Decode converts Bittrex CoreHub2 socket wire protocol data into JSON.
        // Data goes from base64 encoded to gzip (byte[]) to minifed JSON.
        public static string Decode(string wireData)
        {
            // Step 1: Base64 decode the wire data into a gzip blob
            byte[] gzipData = Convert.FromBase64String(wireData);

            // Step 2: Decompress gzip blob into minified JSON
            using (var decompressedStream = new MemoryStream())
            using (var compressedStream = new MemoryStream(gzipData))
            using (var deflateStream = new DeflateStream(compressedStream, CompressionMode.Decompress))
            {
                deflateStream.CopyTo(decompressedStream);
                decompressedStream.Position = 0;

                using (var streamReader = new StreamReader(decompressedStream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        public static string CreateSignature(string apiSecret, string challenge)
        {
            // Get hash by using apiSecret as key, and challenge as data
            var hmacSha512 = new HMACSHA512(Encoding.ASCII.GetBytes(apiSecret));
            var hash = hmacSha512.ComputeHash(Encoding.ASCII.GetBytes(challenge));
            return BitConverter.ToString(hash).Replace("-", string.Empty);
        }
    }
}
