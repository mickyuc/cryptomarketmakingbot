﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketMaker
{
    //환경 변수설정 표
    public class EnvironmentModel
    {
        public int NO { get; set; }
        public string VARIABLE { get; set;  }
        public string VALUE { get; set; }
    }

    //밸런스 표
    public class AccountModel
    {
        public int NO { get; set; }
        public string ACCOUNT_ID { get; set; }
        public string ORIGIN_AMOUNT { get; set; }
        public string DEL_YN { get; set; }
    }

    //포스트정보 이력따지기
    public class PostLogModel
    {

        public int NO { get; set; }
        public string POSTDATA { get; set; }
        public string ORG { get; set; }
        public string TYPE { get; set; }

    }

    //주문정보 
    public class OrderLogModel
    {

        public int NO { get; set; }
        public string REAL_PRICE { get; set; }
        public string ORDER_ID { get; set; }
        public string HOGA_PRICE { get; set; }
        public string QUANTITY { get; set; }
        public string PAIR { get; set; }
    }

    public class OrderCountModel
    {
        public int NO { get; set; }
        public string PAIR { get; set; }
        public string HOGA_PRICE { get; set; }
        public string ORDER_COUNT { get; set; }
    }
}
