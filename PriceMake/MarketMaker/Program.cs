﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarketMaker
{
    public class OrderSetting
    {
        public string pair { get; set; }
        public string rate { get; set; }
        public string premium { get; set; }
        public string unit { get; set; }
        public string minOrder { get; set; }
        public string fee { get; set; }
        public string apiCallTime { get; set; }
        public bool pairStatus { get; set; }
        public bool connectStatus { get; set; }
        public int unit_precision { get; set; }
        public int amount_precision { get; set; }
        public double firstBuyValue { get; set; }   // 1가 매수
        public double firstSellValue { get; set; }  // 1가 매도 
        public string timeStamp { get; set; }
        public DateTime apiTime { get; set; }
        public int transactionBuyCount = 0;
        public int transactionSellCount = 0;
        public double lastPrice { get; set; }
        public string side { get; set; }
        public int timeElapsed { get; set; }
    }

    public class NetworkStatus
    {
        public bool apiConnection { get; set; }
        public DateTime errorTime { get; set; }
        public int connectionTryFailed { get; set; }
        public bool error { get; set; }

        /// ///////////////////////////////
        public bool websocketFirstFlag { get; set; }
        public bool websocketConnection { get; set; }

        /// ///////////////////////////////
        public string position { get; set; }
        public int errorCnt { get; set; }
        public double delay { get; set; }
        public bool jsonResponseStatus { get; set; }
        public bool jsonDataFormat { get; set; }
        public int jsonErrorLimit { get; set; }
        public int jsonErrorCnt { get; set; }

        ///////////////////////
        public int status { get; set; }    // 0: 변하지 않은 상태 , 1: 웹소켓 컨넥션 상태 변화 , 2: RESAT API상태 변화 , 3: json자료형식 상태변화 
    }

    public class Program
    {
        
        public static string token = "eyJ0eXAiOiJKV1QiLCJyZWdEYXRlIjoxNTQ4MTQ2ODU1NDc1LCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJpbmZvcm1hdGlvbiIsIm1lbWJlciI6eyJtZW1iZXJfaWQiOjEwMDAwMTAsImxldmVsX3JvbGUiOjAsImxldmVsX2NlcnQiOjEsImxldmVsX3RyYWRlIjoxLCJleHBpcmVzIjoxNTUwNzM4ODU1fX0.WHh8ClbYs_D7LEfCOyjnZNWK7O8fToRW83fGlHfQ7sI";

        public static Dictionary<string, OrderSetting> orderStatus = new Dictionary<string, OrderSetting>();
        public static Dictionary<string, NetworkStatus> networkStatus = new Dictionary<string, NetworkStatus>();
        
        public static string bittrexOrderbookApi = "https://bittrex.com/api/v1.1/public/getorderbook";
        public static string bittrexGetTickApi = "https://api.bittrex.com/api/v1.1/public/getticker";
        public static string microApi = "http://35.243.121.31:20033/v1/mvc/site/";
        public static string bitfinexOrderbookApi = "https://api.bitfinex.com/v1/book/";
        public static string bitcoinTradeOrderbookApi = "https://api.bitcointrade.com.br/v2/public/";
        public static int delay = 800;   //딜레이 시간간격  ; 초단위;
        public static int balance_number = 15;
        public static int globalOrderCount = 5;
        public static int runningOrderPair = 0;
        public static string commonDirectory = "";
        public static int index = 0;

        public static int webSocketTimeDelay = 60000;
        public static DateTime lastWebSocketReceiveTime = DateTime.Now;
        public static bool programLegalRunning = true;
        public static bool networkConnection = false;
        public static bool programRunning = true;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            EnvironmentModel envirModel = new EnvironmentModel();

            ////////////////////////////////////////////////////////////////////////////////
            // set network status variable
            /////////////////////////////////////////////////////////////////////////////////

            NetworkStatus networkSetting = new NetworkStatus();
            networkSetting.apiConnection = true;
            networkSetting.error = false;
            networkSetting.delay = 30000; // 30s
            networkSetting.errorCnt = 6;
            networkSetting.connectionTryFailed = 0;
            networkSetting.websocketConnection = false;
            networkSetting.position = "bittrex";
            networkSetting.websocketFirstFlag = true;
            networkSetting.jsonResponseStatus = true;
            networkSetting.jsonDataFormat = false;
            networkSetting.jsonErrorLimit = 6;
            networkSetting.jsonErrorCnt = 0;

            networkStatus.Add("bittrex", networkSetting);
            networkSetting = null;

            networkSetting = new NetworkStatus();
            networkSetting.apiConnection = true;
            networkSetting.error = false;
            networkSetting.delay = 30000; // 30s
            networkSetting.errorCnt = 6;
            networkSetting.connectionTryFailed = 0;
            networkSetting.websocketConnection = false;
            networkSetting.position = "bitcointrader";
            networkSetting.websocketFirstFlag = true;
            networkSetting.jsonResponseStatus = true;
            networkSetting.jsonDataFormat = false;
            networkSetting.jsonErrorLimit = 6;
            networkSetting.jsonErrorCnt = 0;

            networkStatus.Add("bitcointrader", networkSetting);
            networkSetting = null;

            networkSetting = new NetworkStatus();
            networkSetting.apiConnection = true;
            networkSetting.error = false;
            networkSetting.delay = 30000; // 30s
            networkSetting.errorCnt = 6;
            networkSetting.connectionTryFailed = 0;
            networkSetting.websocketConnection = false;
            networkSetting.position = "micro";
            networkSetting.websocketFirstFlag = true;
            networkSetting.jsonResponseStatus = true;
            networkSetting.jsonDataFormat = false;
            networkSetting.jsonErrorLimit = 6;
            networkSetting.jsonErrorCnt = 0;

            networkStatus.Add("micro", networkSetting);
            networkSetting = null;
            ////////////////////////////////////////
            /////////////////////////////////////////
            ///
            OrderSetting item = new OrderSetting();
            item.pair = "ETH/BTC";  item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 4;
            item.amount_precision = 3;
            item.timeStamp = "0";
            item.lastPrice = 0.0;
            item.timeElapsed = 0;

            orderStatus.Add("ETH/BTC" , item);
            item = null;

            item = new OrderSetting();
            item.pair = "XRP/BTC"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 7;
            item.amount_precision = 3;
            item.apiTime = DateTime.Now;
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("XRP/BTC", item);
            item = null;

            item = new OrderSetting();
            item.pair = "ETC/BTC"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 5;
            item.amount_precision = 2;
            item.timeStamp = "0";
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("ETC/BTC", item);
            item = null;
            

            item = new OrderSetting();
            item.pair = "BCH/BTC"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 4;
            item.amount_precision = 3;
            item.timeStamp = "0";
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("BCH/BTC", item);
            item = null;

            item = new OrderSetting();
            item.pair = "LTC/BTC"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 5;
            item.amount_precision = 2;
            item.timeStamp = "0";
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("LTC/BTC", item);
            item = null;

            item = new OrderSetting();
            item.pair = "DASH/BTC"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 5;
            item.amount_precision = 3;
            item.timeStamp = "0";
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("DASH/BTC", item);
            item = null;

            item = new OrderSetting();
            item.pair = "ZEC/BTC"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 5;
            item.amount_precision = 3;
            item.timeStamp = "0";
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("ZEC/BTC", item);
            item = null;

            item = new OrderSetting();
            item.pair = "MIC/BTC"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 8;
            item.amount_precision = 3;
            item.timeStamp = "0";
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("MIC/BTC", item);
            item = null;

            item = new OrderSetting();
            item.pair = "KEY/ETH"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 8;
            item.amount_precision = 3;
            item.timeStamp = "0";
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("KEY/ETH", item);
            item = null;

            item = new OrderSetting();
            item.pair = "ETH/KEY"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 8;
            item.amount_precision = 3;
            item.timeStamp = "0";
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("ETH/KEY", item);
            item = null;

            item = new OrderSetting();
            item.pair = "TUSD/USDT"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 3;
            item.amount_precision = 2;
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("TUSD/USDT", item);
            item = null;

            item = new OrderSetting();
            item.pair = "BTC/USDT"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 0;
            item.amount_precision = 6;
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("BTC/USDT", item);
            item = null;

            item = new OrderSetting();
            item.pair = "ETH/USDT"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 1;
            item.amount_precision = 5;
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("ETH/USDT", item);
            item = null;

            item = new OrderSetting();
            item.pair = "BTC/BRL"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 0;
            item.amount_precision = 4;
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("BTC/BRL", item);
            item = null;

            item = new OrderSetting();
            item.pair = "ETH/BRL"; item.premium = item.unit = item.minOrder = item.fee = item.apiCallTime = item.rate = "0.00";
            item.pairStatus = false;
            item.connectStatus = false;
            item.unit_precision = 0;
            item.amount_precision = 4;
            item.lastPrice = 0;
            item.timeElapsed = 0;

            orderStatus.Add("ETH/BRL", item);
            item = null;


            //get configuration file from database 
            envirModel = SqliteDataAccess.GetEnvironmentValue("TOKEN");
            if (envirModel != null)
            {
                token = envirModel.VALUE;
            }

            String path = AppDomain.CurrentDomain.BaseDirectory;
            System.IO.DirectoryInfo path1 = System.IO.Directory.GetParent(path).Parent;
            Program.commonDirectory = path1.FullName + "\\common";

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
