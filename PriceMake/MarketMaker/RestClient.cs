﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.IO;

namespace MarketMaker
{
    public enum HttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }

    class RestClient
    {
        public string EndPoint { get; set; }
        public HttpVerb Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }
        public string cryptolization { get; set; }

        public RestClient()
        {
            EndPoint = "";
            Method = HttpVerb.GET;
            ContentType = "application/json";
            PostData = "";
        }

        /// 
        public RestClient(string endpoint)
        {
            EndPoint = endpoint;
            Method = HttpVerb.GET;
            ContentType = "application/json";
            PostData = "";
        }
        public RestClient(string endpoint, string crypto)
        {
            cryptolization = crypto;
            EndPoint = endpoint;
            Method = HttpVerb.GET;
            ContentType = "application/json";
            PostData = "";
        }

        /// <summary>
        public RestClient(string endpoint, HttpVerb method)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = "application/json";
            PostData = "";
        }

        public RestClient(string endpoint, HttpVerb method, string postData)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = "application/json";
            PostData = postData;
        }

        public RestClient(string endpoint, HttpVerb method, string postData, string crypto)
        {
            cryptolization = crypto;
            EndPoint = endpoint;
            Method = method;
            ContentType = "application/json";
            PostData = postData;
        }

        public string MakeRequest()
        {
            return MakeRequest("");
        }

        public string MakeRequest(string parameters)
        {

            try
            {
                var endPoint = new Uri(string.Concat(EndPoint, parameters));

                var request = (HttpWebRequest)WebRequest.Create(endPoint);

                request.Method = Method.ToString();
                request.ContentLength = 0;
                request.ContentType = ContentType;
                request.Headers["cryptolization"] = cryptolization;

                if (!string.IsNullOrEmpty(PostData) && Method == HttpVerb.POST)
                {
                    var encoding = new UTF8Encoding();
                    var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }

                    // grab the response
                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }

                    return responseValue;
                }
            }
            catch (WebException webExcp)
            {
                return "";
            }
            catch (Exception e)
            {
                return "";
            }
        }

        public BittrexApiResult getOrderBookFromBittrex(string pair) {

            this.EndPoint = Program.bittrexOrderbookApi;
            this.Method = HttpVerb.GET;
            
            string response = MakeRequest("?market=" + CommonFunc.getPairNameInBittrex(pair) + "&type=both");

            try
            {
                var values = JsonConvert.DeserializeObject<BittrexApiResult>(response);
                return values;
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                return null;
            }
            catch (Newtonsoft.Json.JsonSerializationException e)
            {
                return null;
            }

        }

        public BittrexTickResult getCurrentTick(string pair)
        {

            this.EndPoint = Program.bittrexGetTickApi;
            this.Method = HttpVerb.GET;

            string response = MakeRequest("?market=" + CommonFunc.getPairNameInBittrex(pair));

            try
            {
                var values = JsonConvert.DeserializeObject<BittrexTickResult>(response);
                return values;
            }
            catch(Newtonsoft.Json.JsonReaderException e)
            {
                return null;
            }
            catch (Newtonsoft.Json.JsonSerializationException e)
            {
                return null;
            }

        }

        public BitfinexApiResult getOrderBookFromBitfinex(string pair)
        {
            this.EndPoint = Program.bitfinexOrderbookApi + pair;
            this.Method = HttpVerb.GET;
            string response = MakeRequest();
            try
            {
                var values = JsonConvert.DeserializeObject<BitfinexApiResult>(response);
                return values;
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                return null;
            }
            catch (Newtonsoft.Json.JsonSerializationException e)
            {
                return null;
            }
        }

        public BitcoinTrade getOrderBookFromBitcointrade(string pair)
        {

            this.EndPoint = Program.bitcoinTradeOrderbookApi + CommonFunc.getPairNameInBitcointrader(pair) + "/orders";
            this.Method = HttpVerb.GET;
            this.ContentType = "application/json";
            string response = MakeRequest();

            try
            {
                var values = JsonConvert.DeserializeObject<BitcoinTrade>(response);
                return values;
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                return null;
            }
            catch (Newtonsoft.Json.JsonSerializationException e)
            {
                return null;
            }

        }


        public bitcoinTraderTicker getCurrentBrlTick(string pair)
        {

            this.EndPoint = Program.bitcoinTradeOrderbookApi + CommonFunc.getPairNameInBitcointrader(pair) + "/ticker";
            this.Method = HttpVerb.GET;
            this.ContentType = "application/json";
            string response = MakeRequest();

            try
            {
                var values = JsonConvert.DeserializeObject<bitcoinTraderTicker>(response);
                return values;
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                return null;
            }
            catch (Newtonsoft.Json.JsonSerializationException e)
            {
                return null;
            }

        }


        public MyOrders getMyOrder (string pair){

            this.EndPoint = Program.microApi + "bots/orders/" + CommonFunc.getPairNameInMicro1(pair);
            this.Method = HttpVerb.GET;
            this.cryptolization = Program.token;
            string response = MakeRequest();

            try
            {
                var values = JsonConvert.DeserializeObject<MyOrders>(response);
                return values;
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                return null;
            }
            catch (Newtonsoft.Json.JsonSerializationException e)
            {
                return null;
            }

        }
        public MicroBookResult getMyOrderBook (string pair)
        {

            this.EndPoint = Program.microApi + "bots/books/" + CommonFunc.getPairNameInMicro1(pair);
            this.Method = HttpVerb.GET;
            this.cryptolization = Program.token;
            string response = MakeRequest();

            try
            {
                var values = JsonConvert.DeserializeObject<MicroBookResult>(response);
                return values;
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                return null;
            }
            catch (Newtonsoft.Json.JsonSerializationException e)
            {
                return null;
            }

        }
    }
}
