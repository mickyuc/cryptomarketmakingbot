﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketMaker
{
    class Constants
    {
        /// <summary>
        /// //////////////////////////////////////////////////////////
        /// </summary>
        public const string INTERNET_ERROR_MSG = "인터넷 연결 오류. 다시 시도해 주세요.";
        public const string NETWORK_ERROR_MSG = "인터넷 연결 오류입니다.";
        public const string ERROR_TITLE = "오류";

        public const string BITTREX_RESTAPI_ERROR_MSG = "기준거래사이트 API오류 발생하였습니다.";
        public const string BITTREX_RESTAPI_SUCCESS_MSG = "기준거래사이트 API 정상 동작합니다.";

        public const string BITTREX_WEBSOCKET_ERROR_MSG = "기준거래사이트(Upbit) WEBSOCKET 오류 발생하였습니다.";
        public const string BITTREX_WEBSOCKET_SUCCESS_MSG = "기준거래사이트 API 정상 동작합니다.";


        public const string BITCOINTRADE_RESTAPI_ERROR_MSG = "기준거래사이트(BitcoinTrade) API오류 발생하였습니다.";
        public const string BITCOINTRADE_RESTAPI_SUCCESS_MSG = "기준거래사이트 API 정상 동작합니다.";

        public const string MICROSERVER_ERROR_MSG = "마이크로 API서버 오류입니다.";
        public const string MICROSERVER_SUCCESS_MSG = "마이크로 API서버 정상 동작합니다.";

        public const string OFFLINE_MSG = "오프라인 상태입니다.";
        public const string ONLINE_MSG = "Online";

        public const string MICRO_JSON_FORMAT = "마이크로 API 응답 JSON 오류입니다. 봇을 중지하고 확인하세요.";
        public const string MICRO_JSON_FORMAT_FIX = "마이크로 API(JSON) 정상 동작합니다.";

        public const string BASIC_JSON_FORMAT = "기준거래사이트 JSON 오류입니다. 봇을 중지하고 확인하세요.";
        public const string BASIC_JSON_FORMAT_FIX = "기준거래사이트 API(JSON) 정상 동작합니다.";
        /// <summary>
        /// //////////////////////////////////////////////////////////////
        /// </summary>
        public const string ORDER_READY = "기준거래사이트 접속 중입니다...";
        public const string ORDER_SUCCESS = " 에 대한 주문 진행중...";
        public const string ORDER_CANCEL = " 에 대한 주문취소 성공";
        public const string ORDER_CANCEL_ALL = "모든 페어에 주문취소 성공";
        public const string ORDER_CANCEL_ERROR = "주문취소 오류";
        /// <summary>
        /// //////////////////////////////////////////////////
        /// </summary>
        /// 
        public const string CONNECT_ERROR_MICRO = "마이크로 거래소 연결 오류입니다.다시 시도해주세요.";
        public const string CONNECT_ERROR_BASIC = "거래소 연결 오류입니다.다시 시도해주세요.";

        public const string PROGRAM_RUNNING = "거래중";
        public const string PROGRAM_STOP = "중지됨";

        ////////////////////////////////////////
        public const string SAVE_SUCCESS = "저장되었습니다.";
        ///////////////////////////////////////
        public const string ILLEGAL_RUNNING = "손실액이 허용손실액을 초과하여 봇을 임시 중단합니다.";
        public const string ILLEGAL_STARTING = "손실액이 허용손실액을 초과하여 조작을 진행할 수 없습니다.";
        public const string LEGAL_RUNNING = "봇을 재개합니다.(손실액)";
        public const string LOCAL_ACCOUNT_ERROR = "초기자산을 불러올수 없습니다.";

        ////////////////////////////////////////
        public const string CONFIG_READY = "봇 초기 설정중입니다.";
        public const string CONFIG_SUCCESS = "봇 초기 설정 완료 되었습니다.";
        public const string CONFIG_FAIL = "봇 초기 설정 실패하였습니다.";
        public const string CONFIG_CANCEL_READY = "초기 주문취소 중입니다...";

        public const string CONFIG_NETWORK_ERR = "봇 초기설정 시 오류 발생하였습니다. 인터넷 상태를 확인해주세요.";
        public const string CANCEL_NETWORK_ERR = "초기 주문취소에 오류 발생하였습니다. 인터넷 상태를 확인해주세요.";
    }
}
