﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using WebSocketSharp;
using System.Runtime.InteropServices;
using System.IO;

namespace MarketMaker
{

    public partial class MainForm : Form
    {

        public string selectedPair = "ETH/BTC";
        public string baseUrl = "https://socket.bittrex.com/signalr";


        Thread threadETHBTC, threadXRPBTC, threadETCBTC, threadBCHBTC, threadLTCBTC, threadDASHBTC, threadZECBTC, threadTUSDUSDT, threadBTCUSDT, threadETHUSDT, threadBTCBRL, threadETHBRL;


        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);

        public MainForm()
        {
            InitializeComponent();
        }

        //public BittrexWebsocket btx = new BittrexWebsocket();
        private void MainForm_Load(object sender, EventArgs e)
        {

            BITTREX_CLASS.btx.Initialize("https://socket.bittrex.com/signalr",
                        CreateCallback("exchange"),
                        CreateCallback("order"),
                        CreateCallback("balance"),
                        bittrexDisconnectCallback);


            this.button2.Enabled = false;
            this.button6.Enabled = false;
            this.button3.Enabled = false;

            // start background worker
            backgroundWorker.RunWorkerAsync();

        }

        /* bittrex disconnect funtion  */
        static public void bittrexDisconnectCallback(string parameter) {
            Program.networkStatus["bittrex"].websocketConnection = false;
        }

        private bool webSocketStart(string pair , string premium , bool first = true)
        {
            if (!Program.programLegalRunning)
            {
                MessageBox.Show(Constants.ILLEGAL_STARTING, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!Program.networkStatus["bittrex"].websocketConnection)
            {

                try
                {
                    BITTREX_CLASS.btx.setConnection();
                    BITTREX_CLASS.connectBitTrexWebSocket(pair);
                    Program.networkStatus["bittrex"].websocketFirstFlag = false;
                }
                catch (System.AggregateException)
                {
                    
                    if (first)
                    {
                        this.GLOBAL_LABEL.Text = "망 연결 오유";
                        MessageBox.Show("망 연결 오유", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    return false;

                }

                Program.networkStatus["bittrex"].websocketConnection = true;

            }

            Program.orderStatus[pair].pairStatus = true;
            Program.orderStatus[pair].premium = premium;
            
            if (pair == "ETH/BTC" && threadETHBTC == null)
            {
                threadETHBTC = new Thread(new ThreadStart(funcETHBTC));
                threadETHBTC.Start();
            }
            else if (pair == "ETC/BTC" && threadETCBTC == null)
            {
                threadETCBTC = new Thread(new ThreadStart(funcETCBTC));
                threadETCBTC.Start();
            }
            else if (pair == "DASH/BTC" && threadDASHBTC == null)
            {
                threadDASHBTC = new Thread(new ThreadStart(funcDASHBTC));
                threadDASHBTC.Start();
            }
            else if (pair == "LTC/BTC" && threadLTCBTC == null)
            {
                threadLTCBTC = new Thread(new ThreadStart(funcLTCBTC));
                threadLTCBTC.Start();
            }
            else if (pair == "BCH/BTC" && threadBCHBTC == null)
            {
                threadBCHBTC = new Thread(new ThreadStart(funcBCHBTC));
                threadBCHBTC.Start();
            }
            else if (pair == "TUSD/USDT" && threadTUSDUSDT == null)
            {
                threadTUSDUSDT = new Thread(new ThreadStart(funcTUSDUSDT));
                threadTUSDUSDT.Start();
            }
            else if (pair == "ETH/USDT" && threadETHUSDT == null)
            {
                threadETHUSDT = new Thread(new ThreadStart(funcETHUSDT));
                threadETHUSDT.Start();
            }
            else if (pair == "BTC/USDT" && threadBTCUSDT == null)
            {
                threadBTCUSDT = new Thread(new ThreadStart(funcBTCUSDT));
                threadBTCUSDT.Start();
            }
            else if (pair == "BTC/BRL" && threadBTCBRL == null)
            {
                threadBTCBRL = new Thread(new ThreadStart(funcBTCBRL));
                threadBTCBRL.Start();
            }
            else if (pair == "ETH/BRL" && threadETHBRL == null)
            {
                threadETHBRL = new Thread(new ThreadStart(funcETHBRL));
                threadETHBRL.Start();
            }

            if (pair != "BTC/BRL" && pair != "ETH/BRL")
            {
                RestClient client = new RestClient();
                var lastTick = client.getCurrentTick(pair);

                if (lastTick == null)
                {
                    Program.orderStatus[pair].pairStatus = false;

                    if (first)
                    {
                        this.GLOBAL_LABEL.Text = Constants.CONNECT_ERROR_MICRO;
                        MessageBox.Show(Constants.CONNECT_ERROR_BASIC, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    return false;
                }

                if (lastTick.success)
                {
                    Program.orderStatus[pair].lastPrice = lastTick.result.Last;

                    if (lastTick.result.Ask == lastTick.result.Last)
                        Program.orderStatus[pair].side = "S";
                    else
                        Program.orderStatus[pair].side = "B";

                }
                    
            }
            else {

                RestClient client = new RestClient();
                var lastTick = client.getCurrentBrlTick(pair);

                if(lastTick == null)
                {
                    Program.orderStatus[pair].pairStatus = false;

                    if (first)
                    {
                        this.GLOBAL_LABEL.Text = Constants.CONNECT_ERROR_MICRO;
                        MessageBox.Show(Constants.CONNECT_ERROR_BASIC, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    return false;
                }

                if(lastTick.message == null)
                {
                    Program.orderStatus[pair].lastPrice = lastTick.data.last;

                    if(lastTick.data.last == lastTick.data.sell)
                        Program.orderStatus[pair].side = "S";
                    else
                        Program.orderStatus[pair].side = "B";
                }
                    
            }
            return true;
        }


        private void BTN_TOKEN_Click(object sender, EventArgs e)
        {
            ApiKey apiKeyForm = new ApiKey();
            if (apiKeyForm.ShowDialog(this) == DialogResult.OK)
            {
            }
            apiKeyForm.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("체결을 시작하시겠습니까?", "체결시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (!result.Equals(DialogResult.OK))
                return;

            if( webSocketStart("ETH/BTC" , this.SPIN_ETHBTC.Value.ToString()))
            {
                this.BTN_BTCETH.Enabled = false;
                this.button2.Enabled = true;
            }


            if( webSocketStart("ETC/BTC" , this.SPIN_ETCBTC.Value.ToString()) )
            {
                this.BTN_BTCETC.Enabled = false;
                this.button6.Enabled = true;
            }
            if( webSocketStart("DASH/BTC", this.SPIN_DASHBTC.Value.ToString()) )
            {
                this.BTN_BTCDASH.Enabled = false;
                this.button8.Enabled = true;
            }
            if( webSocketStart("BCH/BTC", this.SPIN_BCHBTC.Value.ToString()) )
            {
                this.BTN_BTCBCH.Enabled = false;
                this.button10.Enabled = true;
            }
            if( webSocketStart("LTC/BTC", this.SPIN_LTCBTC.Value.ToString()) )
            {

                this.BTN_BTCLTC.Enabled = false;
                this.button12.Enabled = true;
            }
            if( webSocketStart("BTC/USDT", this.SPIN_BTCUSDT.Value.ToString()) )
            {
                this.BTN_USDTTUDSD.Enabled = false;
                this.button16.Enabled = true;
            }
            if ( webSocketStart("ETH/USDT", this.SPIN_ETHUSDT.Value.ToString()) )
            {
                this.BTN_USDTETH.Enabled = false;
                this.button18.Enabled = true;
            }
            if ( webSocketStart("TUSD/USDT", this.SPIN_TUSDUSDT.Value.ToString()) )
            {
                this.BTN_USDTTUSD.Enabled = false;
                this.button20.Enabled = true;
            }
            if ( webSocketStart("ETH/BRL", this.SPIN_ETHBRL.Value.ToString()) )
            {
                this.BTN_BRLETH.Enabled = false;
                this.button22.Enabled = true;
            }
            if ( webSocketStart("BTC/BRL", this.SPIN_BTCBRL.Value.ToString()) )
            {
                this.BTN_BRLBTC.Enabled = false;
                this.button24.Enabled = true;
            }
            
            this.button1.Enabled = false;
            this.button3.Enabled = true;
        }

        
        private void button3_Click(object sender, EventArgs e)
        {

            Program.orderStatus["ETH/BTC"].pairStatus = false;
            Program.orderStatus["ETC/BTC"].pairStatus = false;
            Program.orderStatus["DASH/BTC"].pairStatus = false;
            Program.orderStatus["BCH/BTC"].pairStatus = false;
            Program.orderStatus["LTC/BTC"].pairStatus = false;
            Program.orderStatus["BTC/USDT"].pairStatus = false;
            Program.orderStatus["ETH/USDT"].pairStatus = false;
            Program.orderStatus["TUSD/USDT"].pairStatus = false;
            Program.orderStatus["ETH/BRL"].pairStatus = false;
            Program.orderStatus["BTC/BRL"].pairStatus = false;


            this.BTN_BTCETH.Enabled = true;
            this.BTN_BTCETC.Enabled = true;
            this.BTN_BTCDASH.Enabled = true;
            this.BTN_BTCBCH.Enabled = true;
            this.BTN_BTCLTC.Enabled = true;
            this.BTN_USDTTUDSD.Enabled = true;
            this.BTN_USDTETH.Enabled = true;
            this.BTN_USDTTUSD.Enabled = true;
            this.BTN_BRLETH.Enabled = true;
            this.BTN_BRLBTC.Enabled = true;


            this.button2.Enabled = false;
            this.button6.Enabled = false;
            this.button8.Enabled = false;
            this.button10.Enabled = false;
            this.button12.Enabled = false;
            this.button16.Enabled = false;
            this.button18.Enabled = false;
            this.button20.Enabled = false;
            this.button22.Enabled = false;
            this.button24.Enabled = false;

            this.button1.Enabled = true;
            this.button3.Enabled = false;
        }

        private void BTN_BTCETH_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("ETH/BTC 체결을 시작하시겠습니까?", "체결시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (!result.Equals(DialogResult.OK))
                return;
            if (!webSocketStart("ETH/BTC", this.SPIN_ETHBTC.Value.ToString()))
                return;

            this.BTN_BTCETH.Enabled = false;
            this.button2.Enabled = true;
            
            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }
        }

        private void BTN_BTCXRP_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(selectedPair + "주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;

            if (!webSocketStart("XRP/BTC", this.SPIN_XRPBTC.Value.ToString()))
                return;

            this.BTN_BTCXRP.Enabled = false;
            this.button4.Enabled = true;

            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }
        }

        private void BTN_BTCETC_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("ETC/BTC 체결을 시작하시겠습니까?", "체결시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;


            if ( !webSocketStart("ETC/BTC", this.SPIN_ETCBTC.Value.ToString()))
                return;

            this.BTN_BTCETC.Enabled = false;
            this.button6.Enabled = true;

            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }

        }

        private void BTN_BTCBCH_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("BCH/BTC 주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;

            if (!webSocketStart("BCH/BTC", this.SPIN_BCHBTC.Value.ToString()))
                return;
            this.BTN_BTCBCH.Enabled = false;
            this.button8.Enabled = true;

            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }
        }

        private void BTN_BTCLTC_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("LTC/BTC 주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;

            if (!webSocketStart("LTC/BTC", this.SPIN_LTCBTC.Value.ToString()))
                return;

            this.BTN_BTCLTC.Enabled = false;
            this.button10.Enabled = true;

            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }
        }

        private void BTN_BTCDASH_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("DASH/BTC 주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;

            if (!webSocketStart("DASH/BTC", this.SPIN_DASHBTC.Value.ToString()))
                return;

            this.BTN_BTCDASH.Enabled = false;
            this.button12.Enabled = true;

            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }
        }

        private void BTN_BTCZEC_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("ZEC/BTC 주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;

            if (!webSocketStart("ZEC/BTC", this.SPIN_ZECBTC.Value.ToString()))
                return;

            this.BTN_BTCZEC.Enabled = false;
            this.button14.Enabled = true;
        }

        private void BTN_USDTTUSD_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("TUSD/USDT 주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;

            if (!webSocketStart("TUSD/USDT", this.SPIN_TUSDUSDT.Value.ToString()))
                return;

            this.BTN_USDTTUSD.Enabled = false;
            this.button16.Enabled = true;

            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }

        }

        private void BTN_USDTTUDSD_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("BTC/USDT 주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;

            if (!webSocketStart("BTC/USDT", this.SPIN_BTCUSDT.Value.ToString()))
                return;

            this.BTN_USDTTUDSD.Enabled = false;
            this.button18.Enabled = true;

            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }

        }

        private void BTN_USDTETH_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("ETH/USDT 주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;

            if (!webSocketStart("ETH/USDT", this.SPIN_ETHUSDT.Value.ToString()))
                return;

            this.BTN_USDTETH.Enabled = false;
            this.button20.Enabled = true;

            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }
        }

        private void BTN_BRLBTC_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("BTC/BRL 주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;

            if (!webSocketStart("BTC/BRL", this.SPIN_BTCBRL.Value.ToString()))
                return;

            this.BTN_BRLBTC.Enabled = false;
            this.button22.Enabled = true;

            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }
        }

        private void BTN_BRLETH_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("ETH/BRL 주문을 시작하시겠습니까?", "주문시작", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (!result.Equals(DialogResult.OK))
                return;

            if (!webSocketStart("ETH/BRL", this.SPIN_ETHBRL.Value.ToString()))
                return;
            this.BTN_BRLETH.Enabled = false;
            this.button24.Enabled = true;

            if (Program.orderStatus["ETH/BTC"].pairStatus && Program.orderStatus["ETC/BTC"].pairStatus && Program.orderStatus["LTC/BTC"].pairStatus && Program.orderStatus["BCH/BTC"].pairStatus && Program.orderStatus["DASH/BTC"].pairStatus && Program.orderStatus["ETH/USDT"].pairStatus && Program.orderStatus["BTC/USDT"].pairStatus && Program.orderStatus["TUSD/USDT"].pairStatus && Program.orderStatus["ETH/BRL"].pairStatus && Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = false;
                this.button3.Enabled = true;
            }
        }

        /// <summary>
        /// stop button  stop button 
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            this.button2.Enabled = false;
            this.BTN_BTCETH.Enabled = true;
            Program.orderStatus["ETH/BTC"].pairStatus = false;

            if (!Program.orderStatus["ETH/BTC"].pairStatus && !Program.orderStatus["ETC/BTC"].pairStatus && !Program.orderStatus["LTC/BTC"].pairStatus && !Program.orderStatus["BCH/BTC"].pairStatus && !Program.orderStatus["DASH/BTC"].pairStatus && !Program.orderStatus["ETH/USDT"].pairStatus && !Program.orderStatus["BTC/USDT"].pairStatus && !Program.orderStatus["TUSD/USDT"].pairStatus && !Program.orderStatus["ETH/BRL"].pairStatus && !Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = true;
                this.button3.Enabled = false;
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.button4.Enabled = false;
            this.BTN_BTCXRP.Enabled = true;
            Program.orderStatus["XRP/BTC"].pairStatus = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.button6.Enabled = false;
            this.BTN_BTCETC.Enabled = true;
            Program.orderStatus["ETC/BTC"].pairStatus = false;

            if (!Program.orderStatus["ETH/BTC"].pairStatus && !Program.orderStatus["ETC/BTC"].pairStatus && !Program.orderStatus["LTC/BTC"].pairStatus && !Program.orderStatus["BCH/BTC"].pairStatus && !Program.orderStatus["DASH/BTC"].pairStatus && !Program.orderStatus["ETH/USDT"].pairStatus && !Program.orderStatus["BTC/USDT"].pairStatus && !Program.orderStatus["TUSD/USDT"].pairStatus && !Program.orderStatus["ETH/BRL"].pairStatus && !Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = true;
                this.button3.Enabled = false;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.button8.Enabled = false;
            this.BTN_BTCBCH.Enabled = true;
            Program.orderStatus["BCH/BTC"].pairStatus = false;

            if (!Program.orderStatus["ETH/BTC"].pairStatus && !Program.orderStatus["ETC/BTC"].pairStatus && !Program.orderStatus["LTC/BTC"].pairStatus && !Program.orderStatus["BCH/BTC"].pairStatus && !Program.orderStatus["DASH/BTC"].pairStatus && !Program.orderStatus["ETH/USDT"].pairStatus && !Program.orderStatus["BTC/USDT"].pairStatus && !Program.orderStatus["TUSD/USDT"].pairStatus && !Program.orderStatus["ETH/BRL"].pairStatus && !Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = true;
                this.button3.Enabled = false;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.button10.Enabled = false;
            this.BTN_BTCLTC.Enabled = true;
            Program.orderStatus["LTC/BTC"].pairStatus = false;

            if (!Program.orderStatus["ETH/BTC"].pairStatus && !Program.orderStatus["ETC/BTC"].pairStatus && !Program.orderStatus["LTC/BTC"].pairStatus && !Program.orderStatus["BCH/BTC"].pairStatus && !Program.orderStatus["DASH/BTC"].pairStatus && !Program.orderStatus["ETH/USDT"].pairStatus && !Program.orderStatus["BTC/USDT"].pairStatus && !Program.orderStatus["TUSD/USDT"].pairStatus && !Program.orderStatus["ETH/BRL"].pairStatus && !Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = true;
                this.button3.Enabled = false;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.button12.Enabled = false;
            this.BTN_BTCDASH.Enabled = true;
            Program.orderStatus["DASH/BTC"].pairStatus = false;

            if (!Program.orderStatus["ETH/BTC"].pairStatus && !Program.orderStatus["ETC/BTC"].pairStatus && !Program.orderStatus["LTC/BTC"].pairStatus && !Program.orderStatus["BCH/BTC"].pairStatus && !Program.orderStatus["DASH/BTC"].pairStatus && !Program.orderStatus["ETH/USDT"].pairStatus && !Program.orderStatus["BTC/USDT"].pairStatus && !Program.orderStatus["TUSD/USDT"].pairStatus && !Program.orderStatus["ETH/BRL"].pairStatus && !Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = true;
                this.button3.Enabled = false;
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            this.button14.Enabled = false;
            this.BTN_BTCZEC.Enabled = true;
            Program.orderStatus["ZEC/BTC"].pairStatus = false;
        }

        private void button16_Click(object sender, EventArgs e)
        {
            this.button16.Enabled = false;
            this.BTN_USDTTUSD.Enabled = true;
            Program.orderStatus["TUSD/USDT"].pairStatus = false;

            if (!Program.orderStatus["ETH/BTC"].pairStatus && !Program.orderStatus["ETC/BTC"].pairStatus && !Program.orderStatus["LTC/BTC"].pairStatus && !Program.orderStatus["BCH/BTC"].pairStatus && !Program.orderStatus["DASH/BTC"].pairStatus && !Program.orderStatus["ETH/USDT"].pairStatus && !Program.orderStatus["BTC/USDT"].pairStatus && !Program.orderStatus["TUSD/USDT"].pairStatus && !Program.orderStatus["ETH/BRL"].pairStatus && !Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = true;
                this.button3.Enabled = false;
            }

        }

        private void button18_Click(object sender, EventArgs e)
        {
            this.button18.Enabled = false;
            this.BTN_USDTTUDSD.Enabled = true;
            Program.orderStatus["BTC/USDT"].pairStatus = false;

            if (!Program.orderStatus["ETH/BTC"].pairStatus && !Program.orderStatus["ETC/BTC"].pairStatus && !Program.orderStatus["LTC/BTC"].pairStatus && !Program.orderStatus["BCH/BTC"].pairStatus && !Program.orderStatus["DASH/BTC"].pairStatus && !Program.orderStatus["ETH/USDT"].pairStatus && !Program.orderStatus["BTC/USDT"].pairStatus && !Program.orderStatus["TUSD/USDT"].pairStatus && !Program.orderStatus["ETH/BRL"].pairStatus && !Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = true;
                this.button3.Enabled = false;
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            this.button20.Enabled = false;
            this.BTN_USDTETH.Enabled = true;
            Program.orderStatus["ETH/USDT"].pairStatus = false;

            if (!Program.orderStatus["ETH/BTC"].pairStatus && !Program.orderStatus["ETC/BTC"].pairStatus && !Program.orderStatus["LTC/BTC"].pairStatus && !Program.orderStatus["BCH/BTC"].pairStatus && !Program.orderStatus["DASH/BTC"].pairStatus && !Program.orderStatus["ETH/USDT"].pairStatus && !Program.orderStatus["BTC/USDT"].pairStatus && !Program.orderStatus["TUSD/USDT"].pairStatus && !Program.orderStatus["ETH/BRL"].pairStatus && !Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = true;
                this.button3.Enabled = false;
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            this.button22.Enabled = false;
            this.BTN_BRLBTC.Enabled = true;
            Program.orderStatus["BTC/BRL"].pairStatus = false;
            if (!Program.orderStatus["ETH/BTC"].pairStatus && !Program.orderStatus["ETC/BTC"].pairStatus && !Program.orderStatus["LTC/BTC"].pairStatus && !Program.orderStatus["BCH/BTC"].pairStatus && !Program.orderStatus["DASH/BTC"].pairStatus && !Program.orderStatus["ETH/USDT"].pairStatus && !Program.orderStatus["BTC/USDT"].pairStatus && !Program.orderStatus["TUSD/USDT"].pairStatus && !Program.orderStatus["ETH/BRL"].pairStatus && !Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = true;
                this.button3.Enabled = false;
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {
            this.button24.Enabled = false;
            this.BTN_BRLETH.Enabled = true;
            Program.orderStatus["ETH/BRL"].pairStatus = false;
            if (!Program.orderStatus["ETH/BTC"].pairStatus && !Program.orderStatus["ETC/BTC"].pairStatus && !Program.orderStatus["LTC/BTC"].pairStatus && !Program.orderStatus["BCH/BTC"].pairStatus && !Program.orderStatus["DASH/BTC"].pairStatus && !Program.orderStatus["ETH/USDT"].pairStatus && !Program.orderStatus["BTC/USDT"].pairStatus && !Program.orderStatus["TUSD/USDT"].pairStatus && !Program.orderStatus["ETH/BRL"].pairStatus && !Program.orderStatus["BTC/BRL"].pairStatus)
            {
                this.button1.Enabled = true;
                this.button3.Enabled = false;
            }
        }

        static public async void setPriceFollow(string side , string pair) {

            int i = 0;

            if (Program.orderStatus[pair].lastPrice == 0)
                return;

            RestClient client = new RestClient();

            var resultValues = client.getMyOrderBook(pair);

            if (resultValues == null)
            {
                CommonFunc.checkNetworkStatus("micro", true);
                return;
            }
                
            if(Program.networkStatus["micro"].error)
            {
                CommonFunc.checkNetworkStatus("micro", false);
            }

            if (resultValues.success && resultValues.d.item.Count > 0)
            {
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                int k = 0;

                List<OrderBookItem> buyOrderBook = new List<OrderBookItem>();
                List<OrderBookItem> sellOrderBook = new List<OrderBookItem>();

                for (i = 0; i < resultValues.d.item.Count; i++)
                {
                    if (resultValues.d.item[i].side == "B")
                        break;

                    OrderBookItem orderBookItem = new OrderBookItem();
                    orderBookItem.price = resultValues.d.item[i].price;
                    orderBookItem.quantity = (resultValues.d.item[i].sum / resultValues.d.item[i].price);
                    sellOrderBook.Add(orderBookItem);
                    orderBookItem = null;
                }

                for (; i < resultValues.d.item.Count; i++)
                {
                    OrderBookItem orderBookItem = new OrderBookItem();
                    orderBookItem.price = resultValues.d.item[i].price;
                    orderBookItem.quantity = (resultValues.d.item[i].sum / resultValues.d.item[i].price);
                    buyOrderBook.Add(orderBookItem);
                    orderBookItem = null;
                }

                double orderQuantity = 0;
                bool intercept = false;

                if (side == "B")
                { // BUY
                    intercept = false;
                    for (i = 0; i < buyOrderBook.Count; i++)
                    {
                        if (Program.orderStatus[pair].lastPrice > buyOrderBook[i].price)
                        {
                            intercept = true;
                            break;
                        }
                        if (Program.orderStatus[pair].lastPrice == buyOrderBook[i].price)
                        {
                            intercept = false;
                            break;
                        }
                        orderQuantity = orderQuantity + buyOrderBook[i].quantity;
                    }

                    if (i < buyOrderBook.Count)
                    {
                        if (intercept)
                        {
                            Random r = new Random();
                            double priceSum = (double)r.Next(1, 10) / 10;
                            orderQuantity = orderQuantity + priceSum / Program.orderStatus[pair].lastPrice;
                            Program.orderStatus[pair].side = "S";
                        }
                        else
                        {
                            Random r = new Random();
                            int fractionX = r.Next(2, 15);
                            int fractionY = r.Next(1, fractionX - 1);
                            orderQuantity = orderQuantity + (double)buyOrderBook[i].quantity * fractionY / fractionX;
                        }
                    }

                    String postdata = "{\"side\" : \"S\" , \"type\" : \"L\" ,\"qty\" : " + orderQuantity + ", \"price\" : " + Program.orderStatus[pair].lastPrice + "}";
                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.microApi + @"orders/" + CommonFunc.getPairNameInMicro2(pair));
                    requestMessage.Headers.Add("cryptolization", Program.token);
                    requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                    try
                    {
                        HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                        string json = await response.Content.ReadAsStringAsync();

                        if (Program.networkStatus["micro"].error)
                        {
                            CommonFunc.checkNetworkStatus("micro", false);
                        }

                    }
                    catch (System.Threading.Tasks.TaskCanceledException e)
                    {
                        CommonFunc.checkNetworkStatus("micro", true);
                    }
                    catch (ArgumentNullException e)
                    {
                        CommonFunc.checkNetworkStatus("micro", true);
                    }
                    catch (InvalidOperationException e)
                    {
                        CommonFunc.checkNetworkStatus("micro", true);
                    }
                    catch (HttpRequestException e)
                    {
                        CommonFunc.checkNetworkStatus("micro", true);
                    }
                }
                else
                {

                    intercept = false;
                    for (i = 0; i < sellOrderBook.Count; i++)
                    {
                        if (Program.orderStatus[pair].lastPrice < sellOrderBook[i].price)
                        {
                            intercept = true;
                            break;
                        }
                        if (Program.orderStatus[pair].lastPrice == sellOrderBook[i].price)
                        {
                            intercept = false;
                            break;
                        }
                        orderQuantity = orderQuantity + sellOrderBook[i].quantity;
                    }

                    if( i < sellOrderBook.Count) {
                        if (intercept)
                        {
                            Random r = new Random();
                            double priceSum = (double)r.Next(1, 10) / 10;
                            orderQuantity = orderQuantity + priceSum / Program.orderStatus[pair].lastPrice;
                            Program.orderStatus[pair].side = "B";
                        }
                        else
                        {
                            Random r = new Random();
                            int fractionX = r.Next(2, 15);
                            int fractionY = r.Next(1, fractionX - 1);
                            orderQuantity = orderQuantity + (double)sellOrderBook[i].quantity * fractionY / fractionX;
                        }
                    }

                    String postdata = "{\"side\" : \"B\" , \"type\" : \"L\" ,\"qty\" : " + orderQuantity + ", \"price\" : " + Program.orderStatus[pair].lastPrice + "}";
                    HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, Program.microApi + @"orders/" + CommonFunc.getPairNameInMicro2(pair));
                    requestMessage.Headers.Add("cryptolization", Program.token);
                    requestMessage.Content = new StringContent(postdata, Encoding.UTF8, "application/json");

                    try
                    {
                        HttpResponseMessage response = await httpClient.SendAsync(requestMessage);
                        string json = await response.Content.ReadAsStringAsync();
                        if (Program.networkStatus["micro"].error)
                        {
                            CommonFunc.checkNetworkStatus("micro", false);
                        }
                    }
                    catch (System.Threading.Tasks.TaskCanceledException e)
                    {
                        CommonFunc.checkNetworkStatus("micro", true);
                    }
                    catch (ArgumentNullException e)
                    {
                        CommonFunc.checkNetworkStatus("micro", true);
                    }
                    catch (InvalidOperationException e)
                    {
                        CommonFunc.checkNetworkStatus("micro", true);
                    }
                    catch (HttpRequestException e)
                    {
                        CommonFunc.checkNetworkStatus("micro", true);
                    }

                }
            }
        }

        //bittrex callback function
        static public BittrexWebsocket.BittrexCallback CreateCallback(string name)
        {
            return async (info) =>
            {
                Program.lastWebSocketReceiveTime = DateTime.Now;

                if (!Program.programLegalRunning)
                    return;

                if (!Program.networkStatus["bittrex"].websocketFirstFlag && !Program.networkStatus["bittrex"].websocketConnection)
                    Program.networkStatus["bittrex"].websocketConnection = true;

                string json_response = BittrexWebsocket.Decode(info);

                try
                {
                    var values = JsonConvert.DeserializeObject<PriceResult>(json_response);

                    if (values == null)
                    {
                        CommonFunc.checkResponseJsonDataFormat("bittrex", true);
                        return;
                    }

                    if (Program.networkStatus["bittrex"].jsonResponseStatus)
                    {
                        CommonFunc.checkResponseJsonDataFormat("bittrex", false);
                    }

                    for (int i = 0; i < values.D.Count; i++)
                    {

                        foreach (KeyValuePair<string, OrderSetting> item in Program.orderStatus)
                        {

                            string pair1 = CommonFunc.getPairNameInBittrex(item.Value.pair);

                            if (pair1 == values.D[i].M && item.Value.pairStatus)
                            {

                                //Console.WriteLine(values.D[i].M + "--" + values.D[i].l + "--" + values.D[i].m);
                                double price = double.Parse(values.D[i].l) * (1 + double.Parse(Program.orderStatus[item.Value.pair].premium) / 100);
                                price = Math.Round(price, Program.orderStatus[item.Value.pair].unit_precision);

                                if (price != Program.orderStatus[item.Value.pair].lastPrice)
                                {

                                    if (price < Program.orderStatus[item.Value.pair].lastPrice)
                                    {
                                        Program.orderStatus[item.Value.pair].side = "B";
                                    }
                                    else if (price > Program.orderStatus[item.Value.pair].lastPrice)
                                    {
                                        Program.orderStatus[item.Value.pair].side = "S";
                                    }
                                    Program.orderStatus[item.Value.pair].lastPrice = price;
                                    setPriceFollow(Program.orderStatus[item.Value.pair].side, item.Value.pair);
                                }
                                break;
                            }
                        }
                    }
                }
                catch (Newtonsoft.Json.JsonReaderException e)
                {
                }
                catch (Newtonsoft.Json.JsonSerializationException e)
                {
                }

            };
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.orderStatus["ETH/BTC"].pairStatus = false;
            if( !(threadETHBTC == null) )
            {
                threadETHBTC.Abort();
            }

            Program.orderStatus["XRP/BTC"].pairStatus = false;
            if (!(threadXRPBTC == null))
            {
                threadXRPBTC.Abort();
            }

            Program.orderStatus["ETC/BTC"].pairStatus = false;
            if (!(threadETCBTC == null))
            {
                threadETCBTC.Abort();
            }

            Program.orderStatus["BCH/BTC"].pairStatus = false;
            if (!(threadBCHBTC == null))
            {
                threadBCHBTC.Abort();
            }

            Program.orderStatus["LTC/BTC"].pairStatus = false;
            if (!(threadLTCBTC == null))
            {
                threadLTCBTC.Abort();
            }

            Program.orderStatus["DASH/BTC"].pairStatus = false;
            if (!(threadDASHBTC == null))
            {
                threadDASHBTC.Abort();
            }

            Program.orderStatus["ZEC/BTC"].pairStatus = false;
            if (!(threadZECBTC == null))
            {
                threadZECBTC.Abort();
            }

            Program.orderStatus["TUSD/USDT"].pairStatus = false;
            if (!(threadTUSDUSDT == null))
            {
                threadTUSDUSDT.Abort();
            }

            Program.orderStatus["BTC/USDT"].pairStatus = false;
            if (!(threadBTCUSDT == null))
            {
                threadBTCUSDT.Abort();
            }

            Program.orderStatus["ETH/USDT"].pairStatus = false;
            if (!(threadETHUSDT == null))
            {
                threadETHUSDT.Abort();
            }

            Program.orderStatus["BTC/BRL"].pairStatus = false;
            if (!(threadBTCBRL == null))
            {
                threadBTCBRL.Abort();
            }

            Program.orderStatus["ETH/BRL"].pairStatus = false;
            if (!(threadETHBRL == null))
            {
                threadETHBRL.Abort();
            }

            Program.programRunning = false;
            backgroundWorker.CancelAsync();

        }
       
        private void funcETHBTC()
        {
            try
            {
                while(true)
                {
                    Thread.Sleep(40000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["ETH/BTC"].pairStatus)
                        setPriceFollow(Program.orderStatus["ETH/BTC"].side, "ETH/BTC");
                }
            }
            catch
            {
            }
        }

        private void funcXRPBTC()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(40000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["XRP/BTC"].pairStatus)
                        setPriceFollow(Program.orderStatus["XRP/BTC"].side, "XRP/BTC");
                }
            }
            catch
            {
            }
        }

        private void funcETCBTC()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(40000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["ETC/BTC"].pairStatus)
                        setPriceFollow(Program.orderStatus["ETC/BTC"].side, "ETC/BTC");
                }
            }
            catch
            {
            }
        }
        private void funcBCHBTC()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(40000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["BCH/BTC"].pairStatus)
                        setPriceFollow(Program.orderStatus["BCH/BTC"].side, "BCH/BTC");
                }
            }
            catch
            {
            }
        }
        private void funcLTCBTC()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(40000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["LTC/BTC"].pairStatus)
                        setPriceFollow(Program.orderStatus["LTC/BTC"].side, "LTC/BTC");
                }
            }
            catch
            {
            }
        }
        private void funcDASHBTC()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(40000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["DASH/BTC"].pairStatus)
                        setPriceFollow(Program.orderStatus["DASH/BTC"].side, "DASH/BTC");
                }
            }
            catch
            {
            }
        }
        private void funcZECBTC()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(40000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["ZEC/BTC"].pairStatus)
                        setPriceFollow(Program.orderStatus["ZEC/BTC"].side, "ZEC/BTC");
                }
            }
            catch
            {
            }
        }
        private void funcTUSDUSDT()
        {

            try
            {
                while (true)
                {
                    Thread.Sleep(40000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["TUSD/USDT"].pairStatus)
                        setPriceFollow(Program.orderStatus["TUSD/USDT"].side, "TUSD/USDT");
                }
            }
            catch
            {

            }
        }
        private void funcBTCUSDT()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(40000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["BTC/USDT"].pairStatus)
                        setPriceFollow(Program.orderStatus["BTC/USDT"].side, "BTC/USDT");
                }
            }
            catch
            {
            }
        }
        private void funcETHUSDT()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(40000);

                    if (!Program.programLegalRunning)
                        continue;

                    if (Program.orderStatus["ETH/USDT"].pairStatus)
                        setPriceFollow(Program.orderStatus["ETH/USDT"].side, "ETH/USDT");
                }
            }
            catch
            {
            }
        }
        private void funcBTCBRL()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(3000);

                    if (!Program.programLegalRunning)
                        continue;

                    Program.orderStatus["BTC/BRL"].timeElapsed += 3000;
                    if (Program.orderStatus["BTC/BRL"].pairStatus) {

                        RestClient client = new RestClient();
                        var lastTick = client.getCurrentBrlTick("BTC/BRL");

                        if(lastTick == null)
                        {
                            CommonFunc.checkNetworkStatus("bitcointrader", true);
                            continue;
                        }

                        if(Program.networkStatus["bitcointrader"].error)
                        {
                            CommonFunc.checkNetworkStatus("bitcointrader", false);
                        }

                        if (lastTick.message == null)
                        {

                            double price = lastTick.data.last;
                            price = price * (1 + double.Parse(Program.orderStatus["BTC/BRL"].premium) / 100);

                            if (Program.orderStatus["BTC/BRL"].lastPrice != price) {
                                if (price < Program.orderStatus["BTC/BRL"].lastPrice)
                                {
                                    Program.orderStatus["BTC/BRL"].side = "B";
                                }
                                else
                                {
                                    Program.orderStatus["BTC/BRL"].side = "S";
                                }

                                Program.orderStatus["BTC/BRL"].lastPrice = price;

                                setPriceFollow(Program.orderStatus["BTC/BRL"].side, "BTC/BRL");
                            }
                            if (Program.orderStatus["BTC/BRL"].timeElapsed > 40000)
                            {
                                Program.orderStatus["BTC/BRL"].timeElapsed = 0;
                                setPriceFollow(Program.orderStatus["BTC/BRL"].side, "BTC/BRL");
                            }
                        }
                    }
                }

            }
            catch
            {
            }
        }
        private void funcETHBRL()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(3000);

                    if (!Program.programLegalRunning)
                        continue;

                    Program.orderStatus["ETH/BRL"].timeElapsed += 3000;
                    if (Program.orderStatus["ETH/BRL"].pairStatus)
                    {

                        RestClient client = new RestClient();
                        var lastTick = client.getCurrentBrlTick("ETH/BRL");

                        if (lastTick == null)
                        {
                            CommonFunc.checkNetworkStatus("bitcointrader", true);
                            continue;
                        }
                        if (Program.networkStatus["bitcointrader"].error)
                        {
                            CommonFunc.checkNetworkStatus("bitcointrader", false);
                        }

                        if (lastTick.message == null)
                        {

                            double price = lastTick.data.last;
                            price = price * (1 + double.Parse(Program.orderStatus["ETH/BRL"].premium) / 100);
                            price = Math.Round(price, Program.orderStatus["ETH/BRL"].unit_precision);

                            if (Program.orderStatus["ETH/BRL"].lastPrice != price)
                            {
                                if (price < Program.orderStatus["ETH/BRL"].lastPrice)
                                {
                                    Program.orderStatus["ETH/BRL"].side = "B";
                                }
                                else
                                {
                                    Program.orderStatus["ETH/BRL"].side = "S";
                                }

                                Program.orderStatus["ETH/BRL"].lastPrice = price;

                                setPriceFollow(Program.orderStatus["ETH/BRL"].side, "ETH/BRL");
                            }
                            if (Program.orderStatus["ETH/BRL"].timeElapsed > 40000)
                            {
                                Program.orderStatus["ETH/BRL"].timeElapsed = 0;
                                setPriceFollow(Program.orderStatus["ETH/BRL"].side, "ETH/BRL");
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void reloadWebSocketConnect()
        {
            foreach (KeyValuePair<string, OrderSetting> item in Program.orderStatus)
            {
                string pair = item.Value.pair;
                if (item.Value.pairStatus)
                    BITTREX_CLASS.connectBitTrexWebSocket(pair);

            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int timeElapsed = 0;

            while (Program.programRunning)
            {   
                Program.networkStatus["micro"].status = 0;
                bool flag = false;
                if (Program.networkStatus["bittrex"].websocketConnection)
                {
                    DateTime nTime = DateTime.Now;
                    TimeSpan span = nTime - Program.lastWebSocketReceiveTime;

                    if (span.TotalMilliseconds > Program.webSocketTimeDelay)
                    {
                        BITTREX_CLASS.btx.Shutdown();
                        BITTREX_CLASS.btx = null;
                        BITTREX_CLASS.btx = new BittrexWebsocket();
                        flag = true;
                        Program.lastWebSocketReceiveTime = DateTime.Now;

                        BITTREX_CLASS.btx.Initialize("https://socket.bittrex.com/signalr",
                            CreateCallback("exchange"),
                            CreateCallback("order"),
                            CreateCallback("balance"),
                            bittrexDisconnectCallback);
                        try
                        {
                            BITTREX_CLASS.btx.setConnection();
                            reloadWebSocketConnect();
                        }
                        catch (System.AggregateException e1)
                        {
                        }
                    }
                }
                backgroundWorker.ReportProgress(10);
                //if websocket disconnect --- try connect
                if (!Program.networkStatus["bittrex"].websocketConnection && !Program.networkStatus["bittrex"].websocketFirstFlag && !flag)
                {

                    Program.lastWebSocketReceiveTime = DateTime.Now;
                    BITTREX_CLASS.btx.Shutdown();
                    BITTREX_CLASS.btx = null;
                    BITTREX_CLASS.btx = new BittrexWebsocket();

                    BITTREX_CLASS.btx.Initialize("https://socket.bittrex.com/signalr",
                        CreateCallback("exchange"),
                        CreateCallback("order"),
                        CreateCallback("balance"),
                        bittrexDisconnectCallback);
                    try
                    {
                        BITTREX_CLASS.btx.setConnection();
                        reloadWebSocketConnect();
                    }
                    catch (System.AggregateException e1)
                    {
                    }
                }

                Thread.Sleep(5000);
                timeElapsed += 5000;
            }
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 10)
            {
                int Desc;
                if (Program.networkConnection != InternetGetConnectedState(out Desc, 0))
                {
                    Program.networkConnection = !Program.networkConnection;
                    if (Program.networkConnection)
                    {
                        this.GLOBAL_LABEL.Text = Constants.ONLINE_MSG;
                    }
                    else
                    {
                        this.GLOBAL_LABEL.Text = Constants.OFFLINE_MSG;
                    }
                }
                else if (Program.networkStatus["bittrex"].status == 1)
                {
                    Program.networkStatus["bittrex"].status = 0;

                    if (Program.networkStatus["bittrex"].websocketConnection)
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_WEBSOCKET_SUCCESS_MSG;
                    else
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_WEBSOCKET_ERROR_MSG;
                }
                else if (Program.networkStatus["bittrex"].status == 2)
                {

                    Program.networkStatus["bittrex"].status = 0;

                    if (Program.networkStatus["bittrex"].apiConnection)
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_RESTAPI_SUCCESS_MSG;
                    else
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_RESTAPI_ERROR_MSG;
                }
                else if (Program.networkStatus["bittrex"].status == 3)
                {
                    Program.networkStatus["bittrex"].status = 0;

                    if (Program.networkStatus["bittrex"].jsonResponseStatus)
                        this.GLOBAL_LABEL.Text = Constants.BASIC_JSON_FORMAT_FIX;
                    else
                        this.GLOBAL_LABEL.Text = Constants.BASIC_JSON_FORMAT;
                }
                else if (Program.networkStatus["bitcointrader"].status == 1)
                {

                    Program.networkStatus["bitcointrader"].status = 0;

                    if (Program.networkStatus["bitcointrader"].websocketConnection)
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_WEBSOCKET_SUCCESS_MSG;
                    else
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_WEBSOCKET_ERROR_MSG;

                }
                else if (Program.networkStatus["bitcointrader"].status == 2)
                {

                    Program.networkStatus["bitcointrader"].status = 0;

                    if (Program.networkStatus["bitcointrader"].apiConnection)
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_RESTAPI_SUCCESS_MSG;
                    else
                        this.GLOBAL_LABEL.Text = Constants.BITTREX_RESTAPI_ERROR_MSG;

                }
                else if (Program.networkStatus["bitcointrader"].status == 3)
                {
                    Program.networkStatus["bitcointrader"].status = 0;

                    if (Program.networkStatus["bitcointrader"].jsonResponseStatus)
                        this.GLOBAL_LABEL.Text = Constants.BASIC_JSON_FORMAT_FIX;
                    else
                        this.GLOBAL_LABEL.Text = Constants.BASIC_JSON_FORMAT;
                }
                else if (Program.networkStatus["micro"].status == 2)
                {

                    Program.networkStatus["micro"].status = 0;

                    if (Program.networkStatus["micro"].apiConnection)
                        this.GLOBAL_LABEL.Text = Constants.MICROSERVER_SUCCESS_MSG;
                    else
                        this.GLOBAL_LABEL.Text = Constants.MICROSERVER_ERROR_MSG;

                }
                else if (Program.networkStatus["micro"].status == 3)
                {
                    Program.networkStatus["micro"].status = 0;

                    if (Program.networkStatus["micro"].jsonResponseStatus)
                        this.GLOBAL_LABEL.Text = Constants.MICRO_JSON_FORMAT_FIX;
                    else
                        this.GLOBAL_LABEL.Text = Constants.MICRO_JSON_FORMAT;
                }
                else if (Program.networkStatus["micro"].status == 4)
                {
                    Program.networkStatus["micro"].status = 0;
                    this.GLOBAL_LABEL.Text = Constants.ORDER_CANCEL_ERROR;
                }


                bool flag = true;
                try
                {
                    using (StreamReader reader = new StreamReader(Program.commonDirectory + "\\2.tmp"))
                    {
                        string line;
                        line = reader.ReadLine();
                        if (line == null)
                            flag = true;
                        else if (line == "true")
                            flag = true;
                        else if (line == "false")
                        {
                            flag = false;
                        }
                    }
                }
                catch (System.IO.FileNotFoundException e1)
                {
                    flag = true;
                }
                catch (System.Reflection.TargetInvocationException e1)
                {
                    flag = true;
                }
                catch (System.IO.DirectoryNotFoundException e1)
                {
                    flag = true;
                }

                if(flag && !Program.programLegalRunning)
                {
                    Program.programLegalRunning = true;
                    this.GLOBAL_LABEL.Text = Constants.LEGAL_RUNNING;
                    MessageBox.Show(Constants.LEGAL_RUNNING, "알림", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if(!flag && Program.programLegalRunning)
                {
                    Program.programLegalRunning = false;
                    this.GLOBAL_LABEL.Text = Constants.ILLEGAL_RUNNING;
                    MessageBox.Show(Constants.ILLEGAL_RUNNING, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }

    }
}
