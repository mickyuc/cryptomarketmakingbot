﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using Dapper;

namespace MarketMaker
{
    public class SqliteDataAccess
    {

        //TB_ORDER_COUNT
        public static List<String> GetOrderCountList(String pair)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    var output = cnn.Query<String>("select HOGA_PRICE from TB_ORDER_COUNT where PAIR=@PAIR", new { PAIR = pair });
                    return output.ToList();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }
            }
        }

        //SAVE COUNT
        public static void SaveOrderCount(OrderCountModel oLog)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute("insert into TB_ORDER_COUNT (PAIR , HOGA_PRICE , ORDER_COUNT) values (@PAIR , @HOGA_PRICE , @ORDER_COUNT)", oLog);
            }
        }


        //UPDATE COUNT
        public static bool UpdateOrderCount(OrderCountModel oLog)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    int rowsAffected = cnn.Execute("UPDATE TB_ORDER_COUNT set ORDER_COUNT = @ORDER_COUNT WHERE HOGA_PRICE = @HOGA_PRICE AND PAIR = @PAIR", oLog);
                    return rowsAffected > 0;
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return false;
                }
            }
        }


        public static OrderCountModel getOrderCountInfo(String pair, String hogaPrice)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    return cnn.Query<OrderCountModel>("select * from TB_ORDER_COUNT where PAIR=@PAIR AND HOGA_PRICE = @HOGA_PRICE LIMIT 1", new { PAIR = pair, HOGA_PRICE = hogaPrice }).SingleOrDefault();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }

            }
        }

        //TB_ORDER_LOG
        /* ------------------------------------------------ */

        //제일 최소의 orderid 얻기 
        public static OrderLogModel getMinOrderId(String pair)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    return cnn.Query<OrderLogModel>("select * from TB_ORDER_LOG where PAIR=@PAIR order by ORDER_ID LIMIT 1", new { PAIR = pair }).SingleOrDefault();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }

            }
        }

        public static List<String> GetOrderByPairList(String pair)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    var output = cnn.Query<String>("select ORDER_ID from TB_ORDER_LOG where PAIR=@PAIR", new { PAIR = pair });
                    return output.ToList();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }

            }
        }

        public static void SaveOrderLog(OrderLogModel oLog)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("insert into TB_ORDER_LOG (REAL_PRICE , ORDER_ID , HOGA_PRICE , QUANTITY , PAIR) values (@REAL_PRICE , @ORDER_ID , @HOGA_PRICE , @QUANTITY , @PAIR)", oLog);
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }
            }
        }

        //수량 얻기
        public static int getOrderCount(String orderHogaPrice, String pair)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    return cnn.Query<int>("select count(*) from TB_ORDER_LOG where PAIR=@PAIR AND HOGA_PRICE=@PRICE", new { PAIR = pair, PRICE = orderHogaPrice }).SingleOrDefault();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return 0;
                }
            }
        }

        //제일 마지막 오더 얻기
        public static OrderLogModel getFirstOrder(String orderHogaPrice, String pair)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    return cnn.Query<OrderLogModel>("select * from TB_ORDER_LOG where PAIR=@PAIR AND HOGA_PRICE=@PRICE order by ORDER_ID LIMIT 1", new { PAIR = pair, PRICE = orderHogaPrice }).SingleOrDefault();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }
            }
        }

        //orderLog 삭제
        public static void RemoveOrderLog(String pair, String orderId)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("delete from TB_ORDER_LOG where PAIR=@PAIR AND ORDER_ID=@ORDER_ID", new { PAIR = pair, ORDER_ID = orderId });
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }
            }
        }
        //TB_POST_LOG
        /* ------------------------------------------------ */
        public static void SavePostLog(PostLogModel pLog)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("insert into TB_POST_LOG (POSTDATA , TYPE , ORG) values (@POSTDATA , @TYPE , @ORG)", pLog);
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }
            }
        }


        //TB_ENVIRONMENT
        /* ------------------------------------------------ */
        public static List<EnvironmentModel> GetEnvironmentList()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    var output = cnn.Query<EnvironmentModel>("select * from TB_ENVIRONMENT", new DynamicParameters());
                    return output.ToList();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }

            }
        }

        public static EnvironmentModel GetEnvironmentValue(string variable)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    return cnn.Query<EnvironmentModel>("select * from TB_ENVIRONMENT where VARIABLE=@VARIABLE", new { VARIABLE = variable }).SingleOrDefault();
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return null;
                }
            }
        }

        public static void SaveEnvironmentValue(EnvironmentModel eValue)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    cnn.Execute("insert into TB_ENVIRONMENT (VARIABLE , VALUE) values (@VARIABLE , @VALUE)", eValue);
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                }

            }
        }

        public static bool UpdateEnvironmentValue(EnvironmentModel updateValue)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                try
                {
                    int rowsAffected = cnn.Execute("UPDATE TB_ENVIRONMENT set VARIABLE = @VARIABLE , VALUE=@VALUE WHERE VARIABLE = @VARIABLE", updateValue);
                    return rowsAffected > 0;
                }
                catch (System.Data.SQLite.SQLiteException e)
                {
                    return false;
                }
            }
        }

        // common
        private static string LoadConnectionString(string id = "Default") {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
