﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarketMaker
{
    public partial class ApiKey : Form
    {
        public ApiKey()
        {
            InitializeComponent();
        }

        private void ApiKey_Load(object sender, EventArgs e)
        {
            this.TOKEN.Height = 40;
            this.TOKEN.Text = Program.token;
        }

        private void SAVE_Click(object sender, EventArgs e)
        {
            EnvironmentModel envirModel = new EnvironmentModel();

            if (TOKEN.Text != "")
            {

                envirModel = SqliteDataAccess.GetEnvironmentValue("TOKEN");
                if (envirModel != null)
                {
                    envirModel.VARIABLE = "TOKEN";
                    envirModel.VALUE = TOKEN.Text;

                    if (!SqliteDataAccess.UpdateEnvironmentValue(envirModel))
                    {
                        MessageBox.Show("TOKEN 보관에 문제가 발생하였습니다.", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                }
                else
                {

                    envirModel = new EnvironmentModel();
                    envirModel.VARIABLE = "TOKEN";
                    envirModel.VALUE = TOKEN.Text;

                    SqliteDataAccess.SaveEnvironmentValue(envirModel);

                }
                Program.token = TOKEN.Text;
            }

            MessageBox.Show("저장되었습니다.", "성공", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
