﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackDoor
{
    static class Program
    {

        public static bool running = true;
        public static string marketMakerPath = "";
        public static string marketMakerDirectory = "";
        public static string commonDirectory = "";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            String path = AppDomain.CurrentDomain.BaseDirectory;
            System.IO.DirectoryInfo path1 = System.IO.Directory.GetParent(path).Parent;
            Program.marketMakerPath = path1.FullName + "\\MarketMaker\\MarketMaker.exe";
            Program.marketMakerDirectory = path1.FullName + "\\MarketMaker";
            Program.commonDirectory = path1.FullName + "\\common";
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
