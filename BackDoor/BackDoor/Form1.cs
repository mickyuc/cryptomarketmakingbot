﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackDoor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;

            backgroundWorker.RunWorkerAsync();
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while(Program.running)
            {
                Process[] pname = Process.GetProcessesByName("MarketMaker");
                if(pname.Length == 0)
                {
                    backgroundWorker.ReportProgress(50);
                }
                else
                {
                }
                System.Threading.Thread.Sleep(10000);
            }
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 50)
            {
                bool flag = false;

                try
                {
                    using (StreamReader reader = new StreamReader(Program.commonDirectory + "\\1.tmp"))
                    {
                        string line;
                        line = reader.ReadLine();
                        if (line == null)
                            flag = true;
                        else if (line != "close")
                        {
                            flag = true;
                        }
                    }
                }
                catch (System.IO.FileNotFoundException e1)
                {
                    flag = true;
                }
                

                if(flag)
                {
                    System.IO.Directory.SetCurrentDirectory(Program.marketMakerDirectory);
                    Process process = new Process();
                    process.StartInfo.FileName = Program.marketMakerPath;
                    process.StartInfo.Arguments = "-n";
                    process.Start();
                }
                
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.running = false;
            backgroundWorker.CancelAsync();
        }
    }
}
